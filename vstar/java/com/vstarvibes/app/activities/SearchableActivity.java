package com.vstarvibes.app.activities;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vstarvibes.app.R;
import com.vstarvibes.app.others.PostClass;
import com.vstarvibes.app.others.PostListClass;
import com.vstarvibes.app.others.SearchListAdapter;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by VICTOR on 14-Dec-16.
 */

public class SearchableActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;
    Toolbar toolbar;
    private boolean shouldLoadHomeFragOnBackPress = true;
    private int page = 1;
    String query;
    private ArrayList<PostListClass> allPosts;
    private SearchListAdapter searchPostAdapter;
    ListView listView;
    TextView noResults;
    private int postID;
    private String postTitle;
    String postDate;
    String author;
    String postImage;
    String category;
    String songUrl;
    String content;
    private String postLink;
    private String postBase;
    String output = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.search_main);
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        toolbar.setTitle("Search results");
        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_close_white);
        actionBar.setTitle("Search results");
        toolbar.setTitleTextColor(Color.WHITE);

        progressDialog = new ProgressDialog(this);
        listView = (ListView) findViewById(R.id.searchListView);
        noResults = (TextView) findViewById(R.id.noResults);
        noResults.setVisibility(View.INVISIBLE);
        progressDialog.setMessage("Searching");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        allPosts = new ArrayList<>();
        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) ;
        {
            query = intent.getStringExtra(SearchManager.QUERY);

            if (query != "") {
                page = 1;
                postBase = "https://vstarvibes.com/wp-json/dec_29/posts?search=" + query + "&fields=id,my_title,categoryOne,song_url,content,author_name,link,featured_media,date,comment_count&per_page=18img_quality=thumbnail&page=";
                new PostsFetcher().execute();
            } else {
                listView.setVisibility(View.INVISIBLE);
                noResults.setVisibility(View.VISIBLE);
                progressDialog.dismiss();
            }
        }


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               /* Activity activity=getActivity();
                if(activity instanceof MainActivity)
                {
                    mainActivity=(MainActivity) activity;
                    connected= mainActivity.checkConnection();

                }*/
                com.vstarvibes.app.activities.MainActivity mainActivity = new com.vstarvibes.app.activities.MainActivity();
                PostListClass post = allPosts.get(position);

                postID = post.id;
                postTitle = post.title;
                postTitle = Html.fromHtml(postTitle).toString();
                postDate = post.date;
                postDate = mainActivity.formatDate(postDate);
                author = post.authorName;
                postImage = post.featuredImage;
                postLink = post.link;
                category=post.categoryOne;
                content=post.content;
                songUrl=post.song_url;
                ArrayList<PostListClass> Posts = allPosts;
                Posts.remove(position);
                ArrayList<PostListClass> relatedPosts = new ArrayList<>();
                for (int i = 0; i < 7; i++) {
                    if (Posts.size() > i) {
                        PostListClass eachPost = Posts.get(i);
                        relatedPosts.add(0, eachPost);
                    }

                }
                PostClass newPost = new PostClass(postID, postTitle, postDate, author, content, postImage, postLink,category,songUrl);
                Intent intent = new Intent(SearchableActivity.this, SinglePostActivity.class);
                intent.putExtra("post", newPost);
                intent.putExtra("returnState", 0);
                intent.putParcelableArrayListExtra("relatedPosts", relatedPosts);
                startActivity(intent);
            }
        });


        listView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view,
                                             int scrollState) { // TODO Auto-generated method stub
                int threshold = 1;
                int count = listView.getCount();

                if (scrollState == SCROLL_STATE_IDLE) {
                    if (listView.getLastVisiblePosition() >= count
                            - threshold) {
                        // Execute LoadMoreDataTask AsyncTask
                        page += 1;
                        progressDialog.setTitle("Loading");
                        progressDialog.show();

                        new PostsFetcher().execute();

                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                // TODO Auto-generated method stub

            }

        });
    }

    public void getPost(String data) {


        if (!TextUtils.isEmpty(data)) {
            Gson Converter = new Gson();
            Type type = new TypeToken<List<PostListClass>>() {
            }.getType();

            try {

                if (page == 1) {
                    allPosts = Converter.fromJson(data, type);

                    searchPostAdapter = new SearchListAdapter(this, allPosts);
                    listView.setAdapter(searchPostAdapter);

                } else {
                    ArrayList<PostListClass> holder = Converter.fromJson(data, type);
                    allPosts.addAll(holder);
                    searchPostAdapter = new SearchListAdapter(this, allPosts);
                    listView.setAdapter(searchPostAdapter);
                    listView.smoothScrollToPosition(listView.getCount() - 18);

                }

            } catch (Exception e) {

            }
            progressDialog.dismiss();
        } else {
            listView.setVisibility(View.INVISIBLE);
            noResults.setVisibility(View.VISIBLE);
        }

    }


    private class PostsFetcher extends AsyncTask<Void, Void, String> {


        @Override
        protected void onPreExecute() {


        }

        @Override
        protected String doInBackground(Void... params) {

            HttpHandler httpHandler = new HttpHandler();

            // Making a request to url and getting response
            String output = httpHandler.makeServiceCall(postBase + page);
            return output;


        }

        @Override
        protected void onPostExecute(String result) {
            if (result != "") {
                getPost(result);

            } else {
                listView.setVisibility(View.INVISIBLE);
                noResults.setVisibility(View.VISIBLE);
                progressDialog.dismiss();

            }
        }

        private class HttpHandler {

            String response;


            public String makeServiceCall(String Url) {

                try {
                    URL url = new URL(Url);
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");
                    // read the response
                    InputStream in = new BufferedInputStream(conn.getInputStream());

                    response = convertStreamToString(in);
                } catch (MalformedURLException e) {

                } catch (ProtocolException e) {

                } catch (IOException e) {

                } catch (Exception e) {

                }
                return response;
            }

            private String convertStreamToString(InputStream is) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();

                String line;
                try {
                    while ((line = reader.readLine()) != null) {
                        sb.append(line).append('\n');
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        is.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                return sb.toString();
            }
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
