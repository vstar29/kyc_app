package com.vstarvibes.app.activities;


import android.Manifest;
import android.app.ActivityOptions;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ToxicBakery.viewpager.transforms.TabletTransformer;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.appinvite.AppInviteInvitation;
import com.vstarvibes.app.R;
import com.vstarvibes.app.fragments.HomeFragment;
import com.vstarvibes.app.others.ConnectivityReceiver;
import com.vstarvibes.app.others.DrawerAdapter;
import com.vstarvibes.app.others.HeaderGridView;
import com.vstarvibes.app.others.HelperClass;
import com.vstarvibes.app.others.MyApplication;
import com.vstarvibes.app.others.PairItemClass;
import com.vstarvibes.app.others.PostClass;
import com.vstarvibes.app.others.PostListClass;
import com.vstarvibes.app.others.TabsPageAdapter;
import com.vstarvibes.app.others.UserClass;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;


public class MainActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {


    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private DrawerLayout drawer;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private View navHeader;
    private ImageView  imgProfile;
    private TextView userEmail;
    private TextView  txtUserName;
    Toolbar toolbar;
    FragmentManager mFragmentManager;
    private boolean shouldLoadHomeFragOnBackPress = true;
    private ActionBarDrawerToggle mDrawerToggle;
    private int returnState=-1;
    Snackbar snackbar;
    HomeFragment fragment = new HomeFragment();
    UserClass user;
    HelperClass helperClass;
    Boolean logIn=false;
    private  String posts;
    private static  final  int REQUEST_INVITE=0;
    HeaderGridView gridView;
    static Bitmap avatar;
    SharedPreferences preferences;
    private static Boolean fresh=true;
    private ImageView auth;
     SharedPreferences.Editor editor;
    private static ArrayList<PairItemClass> favouriteList=new ArrayList<PairItemClass>();
    private ArrayList<PairItemClass> favouriteTabs=new ArrayList<PairItemClass>();
    String fb_url;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MobileAds.initialize(this, getString(R.string.admob_app_id));
        AppEventsLogger.activateApp(this);
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        preferences =this.getSharedPreferences("My_Preference", 0);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.main_drawer_layout);
        LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        navHeader = layoutInflater.inflate(R.layout.main_nav_drawer_header, null);
        gridView = (HeaderGridView) findViewById(R.id.GridView);
        gridView.addHeaderView(navHeader);
        DrawerAdapter drawerAdapter = new DrawerAdapter(this);
        gridView.setAdapter(drawerAdapter);
        mTabLayout = (TabLayout) findViewById(R.id.tabs);
        mFragmentManager = getSupportFragmentManager();
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        editor= preferences.edit();
        txtUserName = (TextView) navHeader.findViewById(R.id.username);
        imgProfile = (ImageView) navHeader.findViewById(R.id.img_profile);
        userEmail = (TextView) navHeader.findViewById(R.id.user_email);
        auth = (ImageView) navHeader.findViewById(R.id.ic_login);
        helperClass = new HelperClass();
        user = helperClass.getUser(this);
        FacebookSdk.sdkInitialize(getApplicationContext());


        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                if (position == 0) {


                } else {
                    position -= gridView.getHeaderViewCount() + 1;

                    if (position == 11) {
                        Intent intent = new AppInviteInvitation.IntentBuilder(getString(R.string.invitation_title))
                                .setMessage(getString(R.string.invitation_message))
                                .setEmailSubject("App Invitation")
                                .setDeepLink(Uri.parse("https://vstarvibes.com"))
                                .setEmailHtmlContent("<html><body><p><a href=\"%%APPINVITE_LINK_PLACEHOLDER%%\"> Install</a> Vstarvibes mobile app and stay updated with latest news, music, jobs opportunities, gist,movies,sports,fashion tips, technology and many more. </p> </body></html>")
                                .build();
                        startActivityForResult(intent, REQUEST_INVITE);
                    } else {
                        mViewPager.setCurrentItem(position);
                    }
                }


                mDrawerLayout.closeDrawers();
            }
        });

        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (logIn == false) {

                    Intent intent = new Intent(MainActivity.this, AuthActivity.class);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        startActivity(intent,
                                ActivityOptions
                                        .makeSceneTransitionAnimation(MainActivity.this).toBundle());
                    } else {
                        startActivity(intent);
                    }


                }
            }
        });

        /*AppLinkData.fetchDeferredAppLinkData(this,
                new AppLinkData.CompletionHandler() {
                    @Override
                    public void onDeferredAppLinkDataFetched(AppLinkData appLinkData) {
                        Uri targetUrl = appLinkData.getTargetUri();
                        Toast.makeText(MainActivity.this,targetUrl.toString(),Toast.LENGTH_LONG).show();
                        if (targetUrl != null) {
                            fb_url=targetUrl.toString();
                            Intent intent=new Intent();
                            intent.putExtra("fb_url",fb_url);
                            startActivity(intent);

                        }
                    }
                }
        );*/

        auth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (logIn == false) {

                    Intent intent = new Intent(MainActivity.this, AuthActivity.class);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        startActivity(intent,
                                ActivityOptions
                                        .makeSceneTransitionAnimation(MainActivity.this).toBundle());
                    } else {
                        startActivity(intent);
                    }


                } else {
                    LoginManager.getInstance().logOut();
                    helperClass.logoutUser(MainActivity.this);
                    Toast.makeText(MainActivity.this, "Logged out", Toast.LENGTH_LONG).show();
                    txtUserName.setText("");
                    userEmail.setText("");
                    imgProfile.setImageResource(R.drawable.unknown);
                    mDrawerLayout.closeDrawers();
                    logIn = false;
                }
            }
        });


        if (getIntent().hasExtra("returnState")) {
            returnState = getIntent().getExtras().getInt("returnState");
        }

        if (getIntent().hasExtra("notifyCallBack")) {
            PostClass notifyPost = getIntent().getParcelableExtra("notifyCallBack");
            Intent intent = new Intent(this, SinglePostActivity.class);
            intent.putExtra("post", notifyPost);
            intent.putExtra("returnState", 0);
            ArrayList<PostListClass> relatedPosts = new ArrayList<>();
            intent.putParcelableArrayListExtra("relatedPosts", relatedPosts);
            startActivity(intent);

        }


        if (getIntent().hasExtra("posts")) {
            posts = getIntent().getExtras().getString("posts");
        }


        TabsPageAdapter adapter = new TabsPageAdapter(mFragmentManager, posts, returnState);
        mViewPager.setAdapter(adapter);
        mViewPager.setOffscreenPageLimit(3);
        mTabLayout.setupWithViewPager(mViewPager);
        mViewPager.setPageTransformer(true, new TabletTransformer());

        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        try {
            if (user != null) {
                logIn = true;
                txtUserName.setText(user.name);
                userEmail.setText(user.email);
                if(avatar!=null)
                {
                    imgProfile.setImageBitmap(avatar);
                }
                else
                {

                    Glide.with(this).load(user.profilePics).asBitmap().diskCacheStrategy(DiskCacheStrategy.ALL).into(new SimpleTarget<Bitmap>(100, 100) {

                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                            avatar=resource;

                            imgProfile.setImageBitmap(avatar);

                        }
                    });


                }





            }
        }catch(Exception e)
        {

        }



        if(preferences.getBoolean("new_install",true))
        {
            mDrawerLayout.openDrawer(Gravity.LEFT);
            String state= Environment.getExternalStorageState();
            if(Environment.MEDIA_MOUNTED.equals(state))
            {
                if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M)
                {
                    if(checkSelfPermission (Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
                    {
                        requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},1);
                        requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},1);
                    }
                }
            }
            editor.putBoolean("new_install",false);
            editor.putString("imageQuality","Low");
            editor.commit();

        }
       /* if(fresh)
        {
            String v=getVersion();
            if(v!="2")
            {
                updateNotification();
            }
        }*/

        mTabLayout.addTab(mTabLayout.newTab().setIcon(R.drawable.hometabicon), 0);
        mTabLayout.addTab(mTabLayout.newTab().setIcon(R.drawable.newstabicon), 1);
        mTabLayout.addTab(mTabLayout.newTab().setIcon(R.drawable.musictabicon), 2);
        mTabLayout.addTab(mTabLayout.newTab().setIcon(R.drawable.gisttabicon), 3);
        mTabLayout.addTab(mTabLayout.newTab().setIcon(R.drawable.videotabicon), 4);
        mTabLayout.addTab(mTabLayout.newTab().setIcon(R.drawable.sporttabicon), 5);
        mTabLayout.addTab(mTabLayout.newTab().setIcon(R.drawable.romancetabicon), 6);
        mTabLayout.addTab(mTabLayout.newTab().setIcon(R.drawable.movietabicon), 7);
        mTabLayout.addTab(mTabLayout.newTab().setIcon(R.drawable.techtabicon), 8);
        mTabLayout.addTab(mTabLayout.newTab().setIcon(R.drawable.jobtabicon), 9);
        mTabLayout.addTab(mTabLayout.newTab().setIcon(R.drawable.fashiontabicon), 10);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                mViewPager.setCurrentItem(position);

            }


            @Override
            public void onPageScrolled(int position, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int position) {

            }

        });


        // Navigation view header

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                toolbar, R.string.app_name, R.string.app_name) {
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.menu_icon);


        actionBar.setTitle("Vstarvibes");
        toolbar.setTitleTextColor(Color.WHITE);
        mViewPager.setCurrentItem(returnState);
        boolean connected = checkConnection();

    }



    /*---get App Version---*/

    private  String getVersion()
    {
        String versionName="";
        try {

            PackageInfo packageInfo=this.getPackageManager().getPackageInfo(this.getPackageName(),0);
            versionName=packageInfo.versionName;
        }catch (PackageManager.NameNotFoundException e)
        {

        }
        return versionName;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == REQUEST_INVITE) {

            if (resultCode == RESULT_OK) {

                // Get the invitation IDs of all sent messages
                String[] ids = AppInviteInvitation.getInvitationIds(resultCode, data);

            } else {


            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener
        MyApplication.getInstance().setConnectivityListener(this);

    }






    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }



    public boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
        return isConnected;
    }



    private void showSnack(boolean isConnected) {


        if (!isConnected) {
            final CoordinatorLayout coordinatorLayout=(CoordinatorLayout) findViewById(R.id.home);
             snackbar = Snackbar.make(coordinatorLayout, "No internet connection",Snackbar.LENGTH_LONG).setAction("DISMISS", new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
            snackbar.setActionTextColor(Color.WHITE);
            TextView tv = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.RED);
            snackbar.show();

        }

    }


    public String formatDate(String date) {

        Date today = new Date();
        String current = today.toString();
        Date postdate = null;

        Long mSeconds;
        long day, hours, minutes, seconds;
        Boolean sameYear=false;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat yearFormat=new SimpleDateFormat("yyyy");
        SimpleDateFormat defaultFormat=new SimpleDateFormat("MMM dd");
        SimpleDateFormat format=new SimpleDateFormat("MMM dd, yyyy");

        try {
            postdate = dateFormat.parse(date);
            today= dateFormat.parse(current);

        } catch (Exception e) {
        }

        mSeconds = today.getTime() - postdate.getTime();
        //seconds= TimeUnit.MILLISECONDS.toSeconds(mSeconds);
        minutes=TimeUnit.MILLISECONDS.toMinutes(mSeconds);
        hours=TimeUnit.MILLISECONDS.toHours(mSeconds);
        day=TimeUnit.MILLISECONDS.toDays(mSeconds);
        String originalYear=yearFormat.format(postdate);
        String yearNow=yearFormat.format(today);
        if(originalYear.equals(yearNow))
        {
            sameYear=true;
        }

        if(day>7 &&sameYear )
        {
            return defaultFormat.format(postdate);
        }

        else if(day==1 &&sameYear )
        {
            return Long.toString(day) +  " day ago";
        }

        else if(day>1 &&day<=7 &&sameYear )
        {
            return Long.toString(day) +  " days ago";
        }
        else if(hours>1 &&hours <=23 && sameYear)
        {
            return Long.toString(hours) +  " hours ago";
        }
        else if(minutes>1 && minutes<60)
        {
            return Long.toString(minutes) +" minutes ago";
        }
        else if(hours==1)
        {
            return  Long.toString(hours) + " hour ago";
        }

        else if(minutes==1)
        {

            return Long.toString(minutes) +" minute ago";
        }

        else if(minutes<1 && sameYear)
        {

            return " just now";
        }

        else
        {
            return format.format(postdate);
        }



    }


    /*--update app notification--*/
    public void updateNotification()
    {
        final CoordinatorLayout coordinatorLayout=(CoordinatorLayout) findViewById(R.id.home);
        snackbar = Snackbar.make(coordinatorLayout, "New Version Available",Snackbar.LENGTH_LONG).setAction("UPDATE", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String appPackageName = getPackageName();
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                }
                catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
                }

            }
        });
        snackbar.setActionTextColor(Color.WHITE);
        TextView tv = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.GREEN);
        snackbar.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mainoption, menu);
        SearchManager searchManager=(SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView=(SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.colorVstarLite,null));

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        //searchView.setIconifiedByDefault(true);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }


        if (id == R.id.action_settings) {

            Intent intent=new Intent(this,SettingActivity.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                startActivity(intent,
                        ActivityOptions
                                .makeSceneTransitionAnimation(MainActivity.this).toBundle());
            } else {
                startActivity(intent);
            }
            return true;
        }



        return super.onOptionsItemSelected(item);
    }


    /*---GSON request function using volley--*/



    
}
