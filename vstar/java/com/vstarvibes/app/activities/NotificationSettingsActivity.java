package com.vstarvibes.app.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;

import com.vstarvibes.app.R;
import com.vstarvibes.app.others.FireBaseRegService;

import java.util.Arrays;
import java.util.List;

/**
 * Created by VICTOR on 14-Dec-16.
 */

public class NotificationSettingsActivity extends AppCompatActivity {

    Toolbar toolbar;
    private boolean shouldLoadHomeFragOnBackPress = true;
    private SwitchCompat notification;
    private SwitchCompat all;
    private CheckBox gist;
    private CheckBox music;
    private CheckBox news;
    private CheckBox video;
    private CheckBox movie;
    private CheckBox sport;
    private CheckBox tech;
    private  CheckBox love;
    private  CheckBox fashion;
    private  CheckBox job;
    private List<String> notifyList;
    RelativeLayout relativeLayout;
    FireBaseRegService fireBaseRegService=new FireBaseRegService();
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.notification_settings);
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        toolbar.setTitle("Notification");
        toolbar.setTitleTextColor(Color.WHITE);
        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_action_nav_left);
        notification=(SwitchCompat) findViewById(R.id.notificationSwitch);
        all=(SwitchCompat) findViewById(R.id.all);
        gist=(CheckBox) findViewById(R.id.gist);
        music=(CheckBox) findViewById(R.id.music);
        sport=(CheckBox) findViewById(R.id.sport);
        video=(CheckBox) findViewById(R.id.video);
        movie=(CheckBox) findViewById(R.id.movie);
        tech=(CheckBox) findViewById(R.id.tech);
        news=(CheckBox) findViewById(R.id.news);
        love=(CheckBox) findViewById(R.id.love);
        job=(CheckBox) findViewById(R.id.job);
        relativeLayout=(RelativeLayout) findViewById(R.id.swap);
        fashion=(CheckBox) findViewById(R.id.fashion);
        sharedPreferences=this.getSharedPreferences("My_preference",0);
        editor= sharedPreferences.edit();
        notifyList= Arrays.asList("news","gist","video","movie","love","job","fashion","sport","tech") ;
        if(sharedPreferences.getBoolean("pushNotification",true))
        {
            notification.setChecked(true);

            if(sharedPreferences.getBoolean("music_notify",true))
            {
                music.setChecked(true);
            }
            else{
                music.setChecked(false);
            }

            if(sharedPreferences.getBoolean("video_notify",false))
            {
                video.setChecked(true);
            }
            else{
                video.setChecked(false);
            }
            if(sharedPreferences.getBoolean("sport_notify",false))
            {
                sport.setChecked(true);
            }
            else{
                sport.setChecked(false);
            }
            if(sharedPreferences.getBoolean("news_notify",false))
            {
                news.setChecked(true);
            }
            else{
                news.setChecked(false);
            }

            if(sharedPreferences.getBoolean("gist_notify",false))
            {
                gist.setChecked(true);
            }
            else{
                gist.setChecked(false);
            }

            if(sharedPreferences.getBoolean("movie_notify",false))
            {
                movie.setChecked(true);
            }
            else{
                movie.setChecked(false);
            }

            if(sharedPreferences.getBoolean("tech_notify",true))
            {
                tech.setChecked(true);
            }
            else{
                tech.setChecked(false);
            }

            if(sharedPreferences.getBoolean("job_notify",false))
            {
                job.setChecked(true);
            }
            else{
                job.setChecked(false);
            }

            if(sharedPreferences.getBoolean("fashion_notify",false))
            {
                fashion.setChecked(true);
            }
            else{
                fashion.setChecked(false);
            }

            if(sharedPreferences.getBoolean("love_notify",false))
            {
                love.setChecked(true);
            }
            else{
                love.setChecked(false);
            }
        }
        else
        {
            notification.setChecked(false);
            relativeLayout.setVisibility(View.INVISIBLE);
        }




        notification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    relativeLayout.setVisibility(View.VISIBLE);
                    all.setChecked(true);
                    fireBaseRegService.subscribedAll();
                    editor.putBoolean("pushNotification",true);
                    for (int i=0;i<notifyList.size();i++)
                    {
                        String item=notifyList.get(i);
                        editor.putBoolean(item+"_notify",true);
                        editor.commit();
                    }
                }else
                {

                    relativeLayout.setVisibility(View.INVISIBLE);
                    editor.putBoolean("pushNotification",false);
                    fireBaseRegService.unSubscribedAll();
                    for (int i=0;i< notifyList.size();i++)
                    {
                        String item=notifyList.get(i);
                        editor.putBoolean(item+"_notify",false);
                        editor.commit();
                    }
                    all.setChecked(false);
                }
            }
        });

        all.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    fireBaseRegService.subscribedAll();
                    music.setChecked(true);
                    video.setChecked(true);
                    movie.setChecked(true);
                    tech.setChecked(true);
                    sport.setChecked(true);
                    news.setChecked(true);
                    gist.setChecked(true);
                    love.setChecked(true);
                    job.setChecked(true);
                    fashion.setChecked(true);

                }else
                {
                    fireBaseRegService.unSubscribedAll();
                    music.setChecked(false);
                    video.setChecked(false);
                    movie.setChecked(false);
                    tech.setChecked(false);
                    sport.setChecked(false);
                    news.setChecked(false);
                    gist.setChecked(false);
                    love.setChecked(false);
                    fashion.setChecked(false);
                    job.setChecked(false);
                }
            }
        });


        setSupportActionBar(toolbar);
    }

   public void subscribeNotification(View v)
    {

        boolean checked=((CheckBox) v).isChecked();
        switch(v.getId())
        {

            case R.id.music:
                if(checked)
                {
                    fireBaseRegService.subscribedUser("Music");
                    editor.putBoolean("music_notify",true);
                    editor.commit();
                }
                else
                {
                    fireBaseRegService.unSubscribedUser("Music");
                    editor.putBoolean("music_notify",false);
                    editor.commit();
                }
                break;
            case R.id.video:
                if(checked)
                {
                    fireBaseRegService.subscribedUser("Videos");
                    editor.putBoolean("video_notify",true);
                    editor.commit();
                }
                else
                {
                    fireBaseRegService.unSubscribedUser("Videos");
                    editor.putBoolean("video_notify",false);
                    editor.commit();
                }
                break;

            case R.id.movie:
                if(checked)
                {
                    fireBaseRegService.subscribedUser("Movie");
                    editor.putBoolean("movie_notify",true);
                    editor.commit();
                }
                else
                {
                    fireBaseRegService.unSubscribedUser("Movie");
                    editor.putBoolean("movie_notify",false);
                    editor.commit();
                }
                break;

            case R.id.news:
                if(checked)
                {
                    fireBaseRegService.subscribedUser("News");
                    editor.putBoolean("news_notify",true);
                    editor.commit();
                }
                else
                {
                    fireBaseRegService.unSubscribedUser("News");
                    editor.putBoolean("news_notify",false);
                    editor.commit();
                }
                break;

            case R.id.tech:
            if(checked)
            {
                fireBaseRegService.subscribedUser("Tech");
                editor.putBoolean("tech_notify",true);
                editor.commit();
            }
            else
            {
                fireBaseRegService.unSubscribedUser("Tech");
                editor.putBoolean("tech_notify",false);
                editor.commit();
            }
            break;

            case R.id.sport:
                if(checked)
                {
                    fireBaseRegService.subscribedUser("Sports");
                    editor.putBoolean("sport_notify",true);
                    editor.commit();
                }
                else
                {
                    fireBaseRegService.unSubscribedUser("Sports");
                    editor.putBoolean("sport_notify",false);
                    editor.commit();
                }
                break;
            case R.id.gist:
                if(checked)
                {
                    fireBaseRegService.subscribedUser("Gist");
                    editor.putBoolean("gist_notify",true);
                    editor.commit();
                }
                else
                {
                    fireBaseRegService.unSubscribedUser("Gist");
                    editor.putBoolean("gist_notify",false);
                    editor.commit();
                }
                break;

            case R.id.love:
                if(checked)
                {
                    fireBaseRegService.subscribedUser("Love");
                    editor.putBoolean("love_notify",true);
                    editor.commit();
                }
                else
                {
                    fireBaseRegService.unSubscribedUser("Love");
                    editor.putBoolean("love_notify",false);
                    editor.commit();
                }
                break;
            case R.id.job:
                if(checked)
                {
                    fireBaseRegService.subscribedUser("Jobs");
                    editor.putBoolean("job_notify",true);
                    editor.commit();
                }
                else
                {
                    fireBaseRegService.unSubscribedUser("Jobs");
                    editor.putBoolean("job_notify",false);
                    editor.commit();

                }
                break;

            case R.id.fashion:
                if(checked)
                {
                    fireBaseRegService.subscribedUser("Fashion");
                    editor.putBoolean("fashion_notify",true);
                    editor.commit();
                }
                else
                {
                    fireBaseRegService.unSubscribedUser("Fashion");
                    editor.putBoolean("fashion_notify",false);
                    editor.commit();
                }
                break;

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(this,SettingActivity.class);
        startActivity(intent);
    }
}
