package com.vstarvibes.app.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vstarvibes.app.R;
import com.vstarvibes.app.others.PostClass;
import com.vstarvibes.app.others.PostListClass;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;


public class DeepLinkActivity extends AppCompatActivity {

   String postBase;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deep_link);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading");
        progressDialog.show();
        onNewIntent(getIntent());

    }


    protected void onNewIntent(Intent intent) {

        String action = intent.getAction();
        String data = intent.getDataString();

        if ((Intent.ACTION_VIEW.equals(action) && data != null)) {
            String link = data;


            try
            {
                if(link.equalsIgnoreCase("https://vstarvibes.com")||link.equalsIgnoreCase("http://vstarvibes.com"))
                {

                    Intent intentm=new Intent(this,MainActivity.class);
                    startActivity(intentm);

                }
                else
                {
                    String[] splits = link.split("/");
                    String slug = splits[splits.length - 1];
                    if(slug.contains("?"))
                    {
                        slug = splits[splits.length - 2];
                    }
                    if(slug.contains("vstarvibes.com"))
                    {

                        Intent intentm=new Intent(this,MainActivity.class);
                        startActivity(intentm);
                    }
                    else
                    {
                        postBase = "https://vstarvibes.com/wp-json/dec_29/posts?slug="+slug + "&fields=id,my_title,categoryOne,song_url,content,author_name,link,featured_media,date,comment_count&per_page=1&page=1";
                        new PostsFetcher().execute();
                    }

                }


            }catch (Exception e)
            {

            }



        }
    }

    public void getPost(String data) {

        com.vstarvibes.app.activities.MainActivity mainActivity = new com.vstarvibes.app.activities.MainActivity();
        if (!TextUtils.isEmpty(data)) {
            Gson Converter = new Gson();
            Type type = new TypeToken<List<PostListClass>>() {
            }.getType();

            try {
                ArrayList<PostListClass> holder = Converter.fromJson(data, type);

                PostListClass post =(PostListClass) holder.get(0);
                post.title=Html.fromHtml(post.title).toString();
                post.date= mainActivity.formatDate(post.date);
                post.content=post.content.trim();
                PostClass postClass=new PostClass(post.id,post.title,post.date,post.authorName,post.content,post.getFeaturedImage(),post.link,post.categoryOne,post.song_url);
                Intent intent = new Intent(DeepLinkActivity.this, SinglePostActivity.class);
                intent.putExtra("post", postClass);
                intent.putExtra("returnState", 0);
                ArrayList<PostListClass> relatedPosts = new ArrayList<>();
                intent.putParcelableArrayListExtra("relatedPosts", relatedPosts);
                startActivity(intent);


            } catch (Exception e) {

            }

        } else {

        }

        progressDialog.dismiss();

    }



    private class PostsFetcher extends AsyncTask<Void, Void, String> {


        @Override
        protected void onPreExecute() {


        }

        @Override
        protected String doInBackground(Void... params) {

            PostsFetcher.HttpHandler httpHandler = new PostsFetcher.HttpHandler();

            // Making a request to url and getting response
            String output = httpHandler.makeServiceCall(postBase);
            return output;


        }

        @Override
        protected void onPostExecute(String result) {
            if (result != "") {
                getPost(result);

            } else {

                progressDialog.dismiss();

            }
        }

        private class HttpHandler {

            String response;


            public String makeServiceCall(String Url) {

                try {
                    URL url = new URL(Url);
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");
                    // read the response
                    InputStream in = new BufferedInputStream(conn.getInputStream());

                    response = convertStreamToString(in);
                } catch (MalformedURLException e) {

                } catch (ProtocolException e) {

                } catch (IOException e) {

                } catch (Exception e) {

                }
                return response;
            }

            private String convertStreamToString(InputStream is) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();

                String line;
                try {
                    while ((line = reader.readLine()) != null) {
                        sb.append(line).append('\n');
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        is.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                return sb.toString();
            }
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(this, MainActivity.class);;
        startActivity(intent);
    }
}
