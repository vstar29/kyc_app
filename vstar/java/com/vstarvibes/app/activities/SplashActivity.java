package com.vstarvibes.app.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.vstarvibes.app.R;

/**
 * Created by VICTOR on 07-Dec-16.
 */

public class SplashActivity extends AppCompatActivity {

    SplashActivity splashActivity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.splash);
        Thread thread=new Thread()
        {
            public void run()
            {
                try
                {
                    sleep(900);
                }catch (Exception e)
                {

                }
                finally {
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                }
            }
        };
        thread.start();


    }


}



