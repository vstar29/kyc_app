package com.vstarvibes.app.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.vstarvibes.app.R;
import com.vstarvibes.app.others.PostClass;
import com.vstarvibes.app.others.PostListClass;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;


public class NotifyCallBackActivity extends AppCompatActivity {

    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notify_call_back);
        progressBar=(ProgressBar) findViewById(R.id.loader);
        progressBar.setVisibility(View.VISIBLE);
        try
        {
            String title = getIntent().getStringExtra("message");String link=getIntent().getStringExtra("link");
            String featured_image = getIntent().getStringExtra("featured_img");
            String date = getIntent().getStringExtra("date");date=formatDate(date);
            String author = getIntent().getStringExtra("author");
            Integer  ID= Integer.valueOf(getIntent().getStringExtra("ID"));
            PostClass post =new PostClass(ID,title,date,author,"",featured_image,link,"","");
            Intent intent = new Intent(this, SinglePostActivity.class);
            intent.putExtra("post",post);
            intent.putExtra("returnState",0);
            ArrayList<PostListClass> relatedPosts=new ArrayList<>();
            intent.putParcelableArrayListExtra("relatedPosts",relatedPosts);
            startActivity(intent);
        }catch (Exception e)
        {
            final CoordinatorLayout coordinatorLayout=(CoordinatorLayout) findViewById(R.id.cord);
             Snackbar snackbar = Snackbar.make(coordinatorLayout, "An error occured",Snackbar.LENGTH_LONG).setAction("DISMISS", new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
            snackbar.setActionTextColor(Color.WHITE);
            TextView tv = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.RED);
            snackbar.show();
        }




    }

    private String formatDate(String date) {

        Date today = new Date();
        String current = today.toString();
        Date postdate = null;

        Long mSeconds;
        long day, hours, minutes, seconds;
        Boolean sameYear=false;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat yearFormat=new SimpleDateFormat("yyyy");
        SimpleDateFormat defaultFormat=new SimpleDateFormat("MMM dd");
        SimpleDateFormat format=new SimpleDateFormat("MMM dd, yyyy");

        try {
            postdate = dateFormat.parse(date);
            today= dateFormat.parse(current);

        } catch (Exception e) {
        }

        mSeconds = today.getTime() - postdate.getTime();
        //seconds= TimeUnit.MILLISECONDS.toSeconds(mSeconds);
        minutes= TimeUnit.MILLISECONDS.toMinutes(mSeconds);
        hours=TimeUnit.MILLISECONDS.toHours(mSeconds);
        day=TimeUnit.MILLISECONDS.toDays(mSeconds);
        String originalYear=yearFormat.format(postdate);
        String yearNow=yearFormat.format(today);
        if(originalYear.equals(yearNow))
        {
            sameYear=true;
        }

        if(day>7 &&sameYear )
        {
            return defaultFormat.format(postdate);
        }

        else if(day==1 &&sameYear )
        {
            return Long.toString(day) +  " day ago";
        }

        else if(day>1 &&day<=7 &&sameYear )
        {
            return Long.toString(day) +  " days ago";
        }
        else if(hours>1 &&hours <=23 && sameYear)
        {
            return Long.toString(hours) +  " hours ago";
        }
        else if(hours==1)
        {
            return  Long.toString(hours) + " hour ago";
        }

        else if(hours< 1 && minutes>1)
        {
            return Long.toString(minutes) +" minutes ago";
        }

        else if(minutes==1)
        {

            return Long.toString(minutes) +" minute ago";
        }

        else if(minutes<1 && sameYear)
        {

            return " just now";
        }

        else
        {
            return format.format(postdate);
        }



    }

}
