package com.vstarvibes.app.activities;


import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.vstarvibes.app.R;
import com.vstarvibes.app.others.PostClass;
import com.vstarvibes.app.others.PostListClass;

import java.util.ArrayList;
import java.util.regex.Pattern;


public class ActivityBrowser extends AppCompatActivity {



    private WebView browser;
    Toolbar toolbar;
    String url;
    ProgressDialog progressDialog;
    PostClass returnPost;
    ArrayList<PostListClass> relatedPosts;
    int height;
    int width;
    String style;
    String start;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.browser);
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        browser=(WebView) findViewById(R.id.browser);
        WebSettings webSettings=browser.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);

        DisplayMetrics displayMetrics=new DisplayMetrics();
        WindowManager windowManager=(WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        width=displayMetrics.widthPixels;
        height=displayMetrics.heightPixels;
        start="<html><head><meta name=\"viewport\"\"content=\"width="+width+", initial-scale=1 \" /></head>";
        style="<style>img,figure,p img,iframe{ margin:auto  !important; display:block; height: auto !important;max-width:100%  !important;}</style>";

        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Loading");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        relatedPosts=new ArrayList<>();
        if(getIntent().hasExtra("forwardReturnPost"))
        {
            returnPost=getIntent().getParcelableExtra("forwardReturnPost");
            relatedPosts=getIntent().getParcelableArrayListExtra("relatedPosts");
        }
        if(getIntent().hasExtra("url"))
        {
            url=getIntent().getStringExtra("url");


        }

        browser.setWebViewClient(new WebClient());
        browser.loadUrl(url);
        final android.support.v7.app.ActionBar actionBar=getSupportActionBar();

        actionBar.setDisplayHomeAsUpEnabled(true);

        if(url.contains("vstarvibes.com"))
        {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrowleft);
        }
        else
        {
            toolbar.setBackgroundColor(Color.WHITE);
            toolbar.setTitleTextColor(Color.argb(212, 140, 8, 33));
            toolbar.setSubtitle(url);
            toolbar.setSubtitleTextColor(Color.BLACK);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_action_close);

        }

        //actionBar.setIcon(R.drawable.logo);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.browser_option, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id==android.R.id.home) {

            Intent intent=new Intent(this,SinglePostActivity.class);
            intent.putExtra("returnPost", returnPost);
            intent.putParcelableArrayListExtra("relatedPosts", relatedPosts);
            browser.clearHistory();
            //browser.clearCache(true);
            startActivity(intent);
            return true;
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.openExternal) {
            Intent external=new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(external);
            return true;
        }


        return super.onOptionsItemSelected(item);
    }


    private class WebClient extends WebViewClient {


        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
            progressDialog.show();
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error)
        {

            super.onReceivedError(view,request,error);
            {
                try
                {
                    Intent intent=new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(url));
                    startActivity(intent);

                }catch (Exception e)
                {
                    Toast.makeText(ActivityBrowser.this, "Cant load url",Toast.LENGTH_LONG).show();
                }
            }

        }


        @Override
        public boolean shouldOverrideUrlLoading(WebView webView, String url)
        {

            if(url.endsWith(".mp3") || url.endsWith(".mp4") || url.endsWith(".3gp")|| url.endsWith(".pdf") || url.endsWith(".apk") || url.endsWith(".avi")|| url.endsWith(".flv")|| url.endsWith(".zip")||url.endsWith(".docx") ||url.endsWith(".MP4"))
            {

                    String[] splits = url.split(Pattern.quote("."));
                    Uri source = Uri.parse(url);
                    DownloadManager.Request request = new DownloadManager.Request(source);
                    request.setDescription("Donwloading..");
                    String extension = splits[splits.length - 1];



                    try {

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                String title = webView.getTitle() + "." + extension;
                                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, title);
                                request.allowScanningByMediaScanner();
                                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                                DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                                manager.enqueue(request);

                            }





                    } catch (Exception e) {

                    }

            }
            webView.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            progressDialog.dismiss();
            super.onPageFinished(view, url);
            ActivityBrowser.this.setTitle(view.getTitle());

        }

       /* @Override
        public boolean onKeyDown(int keyCode, KeyEvent event)
        { //if back key is pressed
            if((keyCode == KeyEvent.KEYCODE_BACK)&& browser.canGoBack())
            {
                browser.goBack();
                return true;

            }
             if((keyCode == KeyEvent.KEYCODE_BACK)&& !browser.canGoBack))
            {
                Intent intent=new Intent(this,SinglePostActivity.class);
                intent.putParcelableArrayListExtra("returnPost", returnPost);
                intent.putParcelableArrayListExtra("relatedPosts",relatedPosts);
                startActivity(intent);
                return false

            }

            return super.onKeyDown(keyCode, event);

        }*/

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(this,SinglePostActivity.class);
        intent.putExtra("returnPost", returnPost);
        intent.putParcelableArrayListExtra("relatedPosts", relatedPosts);
        browser.clearHistory();
        browser.clearCache(true);
        startActivity(intent);
    }

}


