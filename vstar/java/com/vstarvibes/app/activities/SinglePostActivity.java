package com.vstarvibes.app.activities;


import android.Manifest;
import android.app.ActivityOptions;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.vstarvibes.app.R;
import com.vstarvibes.app.others.PostClass;
import com.vstarvibes.app.others.PostListClass;
import com.vstarvibes.app.others.SinglePostAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;


public class SinglePostActivity extends AppCompatActivity {



    Toolbar toolbar;
    private boolean shouldLoadHomeFragOnBackPress = true;
    private int id;
    ViewPager viewPager;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    private int returnState;
    PostClass returnPost;
    PostClass post;
    Boolean favourite=false;
    InterstitialAd mInterstitialAd;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.single_post_main);
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        viewPager=(ViewPager)findViewById(R.id.viewpager);
        mFragmentManager = getSupportFragmentManager();
        toolbar.setTitleTextColor(Color.WHITE);
        MobileAds.initialize(this,getString(R.string.admob_app_id));
         mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.add_full));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener()
        {
            @Override
            public void onAdLoaded()
            {
                showInterstitial();
            }
            @Override
            public void onAdFailedToLoad(int errorCode)
            {
                
            }
        });


       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setExitTransition(new Slide());
        }*/
        if(getIntent().hasExtra("post")) {
            post = getIntent().getParcelableExtra("post");
        }



        ArrayList<PostListClass> relatedPosts=getIntent().getParcelableArrayListExtra("relatedPosts");
        if(getIntent().hasExtra("returnPost")) {
            returnPost=getIntent().getParcelableExtra("returnPost");
        }


        id= getIntent().getExtras().getInt("id");
        returnState=getIntent().getExtras().getInt("returnState");
        SinglePostAdapter singlePostAdapter;

        if(returnPost!=null)
        {

             singlePostAdapter=new SinglePostAdapter(this,mFragmentManager,post,relatedPosts,returnPost,returnPost.category);
        }
        else
        {
             singlePostAdapter=new SinglePostAdapter(this,mFragmentManager,post,relatedPosts,returnPost,post.category);
        }


        viewPager.setAdapter(singlePostAdapter);
        final android.support.v7.app.ActionBar actionBar=getSupportActionBar();

        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_action_nav_left);
        //actionBar.setIcon(R.drawable.logo);

    }

    public  void favouriteAdded()
    {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.single_post_option, menu);
        return true;
    }

    public String formatDate(String date) {

        Date today = new Date();
        String current = today.toString();
        Date postdate = null;

        Long mSeconds;
        long day, hours, minutes, seconds;
        Boolean sameYear=false;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat yearFormat=new SimpleDateFormat("yyyy");
        SimpleDateFormat defaultFormat=new SimpleDateFormat("MMM dd");
        SimpleDateFormat format=new SimpleDateFormat("MMM dd, yyyy");

        try {
            postdate = dateFormat.parse(date);
            today= dateFormat.parse(current);

        } catch (Exception e) {
        }

        mSeconds = today.getTime() - postdate.getTime();
        seconds= TimeUnit.MILLISECONDS.toSeconds(mSeconds);
        minutes=TimeUnit.MILLISECONDS.toMinutes(mSeconds);
        hours=TimeUnit.MILLISECONDS.toHours(mSeconds);
        day=TimeUnit.MILLISECONDS.toDays(mSeconds);
        String originalYear=yearFormat.format(postdate);
        String yearNow=yearFormat.format(today);
        if(originalYear.equals(yearNow))
        {
            sameYear=true;
        }

        if(day>7 &&sameYear )
        {
            return defaultFormat.format(postdate);
        }

        else if(day==1 &&sameYear )
        {
            return Long.toString(day) +  " day ago";
        }

        else if(day>1 &&day<=7 &&sameYear )
        {
            return Long.toString(day) +  " days ago";
        }
        else if(hours>1 &&hours <=23 && sameYear)
        {
            return Long.toString(hours) +  " hours ago";
        }
        else if(hours==1)
        {
            return  Long.toString(hours) + " hour ago";
        }

        else if(minutes>1 && minutes<60)
        {
            return Long.toString(minutes) +" minutes ago";
        }

        else if(minutes==1)
        {

            return Long.toString(minutes) +" minute ago";
        }

        else if(minutes< 1 && sameYear)
        {

            return " just now";
        }

        else
        {
            return format.format(postdate);
        }



    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id==android.R.id.home) {
            Intent intent=new Intent(this, com.vstarvibes.app.activities.MainActivity.class);
            intent.putExtra("returnState",returnState);
            startActivity(intent);
            return true;
        }
        /*if (id == R.id.action_favourite) {

            return super.onOptionsItemSelected(item);
        }*/

        if (id == R.id.action_comments) {

            return super.onOptionsItemSelected(item);
        }

        if (id == R.id.action_share) {

            return super.onOptionsItemSelected(item);
        }



        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(this, MainActivity.class);
        intent.putExtra("returnState",returnState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            startActivity(intent,
                    ActivityOptions
                            .makeSceneTransitionAnimation(this).toBundle());
        } else {
            startActivity(intent);
        }
    }

    /*--Check if external files is writable() */
    public  Boolean isExternalStorageWritable()
    {
        String state= Environment.getExternalStorageState();
        if(Environment.MEDIA_MOUNTED.equals(state))
        {
            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M)
            {
                if(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED)
                {
                    return true;
                }
                else
                {
                    requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},1);
                    requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},1);
                }

            }
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,int[] grantResults){
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (isExternalStorageWritable()) {
                continueAction();
            }
        }
    }
    @Override
    public void onResume()
    {
        super.onResume();
        try {
            showInterstitial();
        }catch (Exception e)

        {

        }
    }


    private void showInterstitial()
    {
        try {
            if(mInterstitialAd.isLoaded())
            {
                mInterstitialAd.show();
            }
        }catch (Exception e)

        {

        }

    }

    public  Boolean continueAction()
    {
        return  true;
    }





}
