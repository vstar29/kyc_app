package com.vstarvibes.app.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.vstarvibes.app.R;
import com.vstarvibes.app.others.HelperClass;
import com.vstarvibes.app.others.UserClass;

import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by VICTOR on 08-Nov-16.
 */
public class AuthActivity extends AppCompatActivity {

    private LoginButton loginButton;
    private CallbackManager callbackManager;
    View loginPage;
    private UserClass user;
    HelperClass helperClass;
    Toolbar toolbar;
    private String imageUrl;
    Bitmap avatar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
       setContentView(R.layout.login_page);
       AppEventsLogger.activateApp(this);
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_close_white);
        toolbar.setTitleTextColor(Color.WHITE);
        actionBar.setTitle("");

        loginButton=(LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList(
                "public_profile", "email", "user_birthday", "user_friends"));
        callbackManager = CallbackManager.Factory.create();
        helperClass=new HelperClass();



        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Toast.makeText(AuthActivity.this,"Logged in",Toast.LENGTH_LONG).show();

                GraphRequest graphRequest= GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {

                            String id=object.getString("id").toString();
                            String name=object.getString("name");
                            String email="";
                            if(object.has("email"))
                            {
                                email=object.getString("email").toLowerCase();
                            }

                            JSONObject data=response.getJSONObject();
                            String picsUrl=data.getJSONObject("picture").getJSONObject("data").getString("url");
                            user=new UserClass(id,name,email,picsUrl);
                            helperClass.saveUser(AuthActivity.this,user);
                            Intent intent=new Intent(AuthActivity.this,MainActivity.class);
                            startActivity(intent);




                        }catch (Exception e)
                        {
                            Toast.makeText(AuthActivity.this,e.getMessage(),Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }

                    }
                });
                Bundle parameters =new Bundle();
                parameters.putString("fields", "id,name,email,picture.type(large)");
                graphRequest.setParameters(parameters);
                graphRequest.executeAsync();



            }

            @Override
            public void onCancel() {
                Intent intent =new Intent(AuthActivity.this,MainActivity.class);
                startActivity(intent);

            }

            @Override
            public void onError(FacebookException e) {
                Toast.makeText(AuthActivity.this,"Problem signing in",Toast.LENGTH_LONG).show();
                Intent intent =new Intent(AuthActivity.this,AuthActivity.class);
                startActivity(intent);

            }
        });
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        int id = item.getItemId();

        if (id==android.R.id.home) {
            Intent intent=new Intent(this,MainActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
     Intent intent=new Intent(this,MainActivity.class);
        startActivity(intent);
    }

}
