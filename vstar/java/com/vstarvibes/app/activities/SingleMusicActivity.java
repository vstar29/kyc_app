package com.vstarvibes.app.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vstarvibes.app.R;
import com.vstarvibes.app.fragments.SinglePostFragment;
import com.vstarvibes.app.others.CommentAdapter;
import com.vstarvibes.app.others.CommentClass;
import com.vstarvibes.app.others.DataFetcher;
import com.vstarvibes.app.others.HelperClass;
import com.vstarvibes.app.others.ItemClickListener;
import com.vstarvibes.app.others.PostClass;
import com.vstarvibes.app.others.PostListClass;
import com.vstarvibes.app.others.RelatedPostAdapter;
import com.vstarvibes.app.others.UserClass;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class SingleMusicActivity extends AppCompatActivity implements ItemClickListener
{
  ArrayList<CommentClass> CommentsList;
  String authorName;
  RelativeLayout blurBg;
  String category;
  ImageView closeDialogue;
  CommentAdapter commentAdapter;
  private String commentBase;
  ProgressBar commentLoader;
  int commentPage = 1;
  View commentsView;
  WebView content;
  CoordinatorLayout coordinatorLayout;
  Boolean favourite = Boolean.valueOf(false);
  ImageView featured_image;
  PostClass forwardReturnPost;
  Gson gson;
  AudioManager audioManager;
  int height;
  private int id;
  private LayoutInflater layoutinflater;
  FragmentManager mFragmentManager;
  MediaPlayer mediaPlayer;
  EditText newCommentContent;
  ImageView noCommentImage;
  TextView noCommentText;
  int page = 1;
  ImageView play;
  Boolean playing = Boolean.valueOf(false);
  PopupWindow popComments;
  PostClass post;
  private String postBase;
  ListView postComments;
  String postContent = null;
  String postDate;
  int postId;
  String postImage;
  String postLink;
  ImageView postNewComment;
  String postTitle;
  ProgressDialog progressDialog;
  RecyclerView relatedList;
  RelatedPostAdapter relatedPostAdapter;
  ArrayList<PostListClass> relatedPosts;
  TextView related_title;
  PostClass returnPost;
  private int returnState;
  ScrollView scrollView;
  private boolean shouldLoadHomeFragOnBackPress = true;
  SinglePostActivity singlePostActivity;
  String songUrl;
  String start;
  private AudioManager.OnAudioFocusChangeListener focusChangeListener;
  String style;
  TextView title;
  Toolbar toolbar;
  int width;
    Boolean loaded=false;
    Boolean loadingMusic;
    ProgressBar adLoading2;
    ProgressBar adLoading;
    SharedPreferences appSharedPref;
    SharedPreferences.Editor editor;
    ProgressBar musicLoading;
    Boolean audioAccess=false;
    InterstitialAd mInterstitialAd;


    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_single_music);
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        relatedList=(RecyclerView)findViewById(R.id.Related_list);
        related_title=(TextView) findViewById(R.id.related_title);
        relatedList.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        relatedList.setLayoutManager(layoutManager);
        audioManager=(AudioManager)getSystemService(Context.AUDIO_SERVICE);
        layoutinflater=(LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) ;
        musicLoading=(ProgressBar) findViewById(R.id.musicLoading) ;
        adLoading=(ProgressBar) findViewById(R.id.add_loading) ;
        adLoading.setVisibility(View.VISIBLE);
        musicLoading.setVisibility(View.INVISIBLE);
        commentsView=layoutinflater.inflate(R.layout.commentcordinator,null,false);

        title=(TextView) findViewById(R.id.post_title);
        blurBg=(RelativeLayout) findViewById(R.id.post_imageBg);
        MobileAds.initialize(this,getString(R.string.admob_app_id));
        AdView adView = (AdView) findViewById(R.id.adBig);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView.loadAd(adRequest1);
        AdView mAdView = (AdView) findViewById(R.id.adbottom);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener()
        {
            @Override
            public void onAdLoaded()
            {
                adLoading.setVisibility(View.INVISIBLE);
            }
            @Override
            public void onAdFailedToLoad(int errorCode)
            {
                adLoading.setVisibility(View.INVISIBLE);
            }
        });
        MobileAds.initialize(this,getString(R.string.admob_app_id));
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.add_full));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener()
        {
            @Override
            public void onAdLoaded()
            {
                showInterstitial();
            }
            @Override
            public void onAdFailedToLoad(int errorCode)
            {

            }
        });
        appSharedPref=getSharedPreferences("My_Preference", 0);
        editor=appSharedPref.edit();


        closeDialogue=(ImageView) commentsView.findViewById(R.id.close);
        closeDialogue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newCommentContent.setText("");
                popComments.dismiss();

            }
        });
        featured_image=(ImageView) findViewById(R.id.featured_image);

        postComments=(ListView) commentsView.findViewById(R.id.Comment_list);
        commentLoader=(ProgressBar) commentsView.findViewById(R.id.commentProgressBar);
        postNewComment=(ImageView) commentsView.findViewById(R.id.send_comment);
        newCommentContent=(EditText) commentsView.findViewById(R.id.comment_content);
        content=(WebView) findViewById(R.id.content);
        commentLoader.setVisibility(View.INVISIBLE);
        noCommentImage= (ImageView) commentsView.findViewById(R.id.noCommentIcon);
        noCommentText=(TextView) commentsView.findViewById(R.id.noCommentText);
        noCommentText.setVisibility(View.INVISIBLE);
        noCommentImage.setVisibility(View.INVISIBLE);
        mediaPlayer=new MediaPlayer();
        play=(ImageView) findViewById(R.id.play);


        post=getIntent().getParcelableExtra("post");
        relatedPosts=getIntent().getParcelableArrayListExtra("relatedPosts");
        returnPost=getIntent().getParcelableExtra("returnPost");

        id= getIntent().getExtras().getInt("id");
        returnState=getIntent().getExtras().getInt("returnState");
        final android.support.v7.app.ActionBar actionBar=getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_action_nav_left);
        toolbar.setTitleTextColor(Color.WHITE);
        coordinatorLayout= (CoordinatorLayout) findViewById(R.id.singleMusic);
        WebSettings webSettings=content.getSettings();
        webSettings.setJavaScriptEnabled(true);
        content.setWebViewClient(new BrowserClient());
        postComments.setHeaderDividersEnabled(true);
        relatedList.setFocusable(false);
        post=getIntent().getParcelableExtra("post");
        relatedPosts=getIntent().getParcelableArrayListExtra("relatedPosts");
        returnPost=getIntent().getParcelableExtra("returnPost");


        id= getIntent().getExtras().getInt("id");
        returnState=getIntent().getExtras().getInt("returnState");
        if(relatedPosts.isEmpty())
        {
            related_title.setVisibility(View.GONE);
        }
        relatedPostAdapter=new RelatedPostAdapter(this,relatedPosts);
        relatedPostAdapter.setClickListener(this);
        DisplayMetrics displayMetrics=new DisplayMetrics();
        WindowManager windowManager=(WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        width=displayMetrics.widthPixels;
        height=displayMetrics.heightPixels;
        start="<html><head><meta name=\"viewport\"\"content=\"width="+width+", initial-scale=1 \" /></head>";
        style="<style> img,figure,p img,iframe{ margin:auto  !important; display:block; height: auto !important;max-width:100%  !important;}.inserted_ad{display:none !important;}.sharedaddy, .sd-sharing-enabled, .zem_rp_wrap, .zem_rp_th_modern,.socialmedia,.tptn_counter { display: none !important} </style>";

        PostClass p;

        if(returnPost !=null)
        {
            p = returnPost;
            postTitle = p.title;
            authorName = p.authorName;
            postDate = p.date;
            postImage = p.featuredImage;
            postId = p.id;
            postContent=p.content;
            postLink=p.link;
            category=p.category;
            songUrl=p.song_url;
            content.loadDataWithBaseURL(null, start + style + postContent + "</html>", "text/html", "UTF-8", null);
            forwardReturnPost=new PostClass(postId,postTitle,postDate,authorName,postContent,postImage,postLink,category,songUrl);

        }

        else

        {
            p=post;
            postTitle = p.title;
            authorName = p.authorName;
            postDate = p.date;
            postImage = p.featuredImage;
            postId = p.id;
            postLink=p.link;
            postContent=p.content.trim();
            postBase = "https://vstarvibes.com/wp-json/dec_29/posts/"+postId+"?fields=content";
            category=p.category;
            songUrl=p.song_url;
            if(!TextUtils.isEmpty(postContent))
            {

                content.loadDataWithBaseURL(null, start + style + postContent + "</html>", "text/html", "UTF-8", null);
                forwardReturnPost=new PostClass(postId,postTitle,postDate,authorName,postContent,postImage,postLink,category,songUrl);
            }
            else {
                fetchPosts();
            }


        }

        {
            relatedList.setAdapter(this.relatedPostAdapter);
            commentBase = "https://vstarvibes.com/wp-json/dec_29/comments/?post=" + postId + "&fields=post,author_name,date,author_avatar,content&per_page=6&page=";
            title.setText(this.postTitle);
            Glide.with(this).load(postImage).asBitmap().diskCacheStrategy(DiskCacheStrategy.ALL).into(new SimpleTarget<Bitmap>(width, 200) {

                @Override
                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                    Drawable draw = new BitmapDrawable(resource);
                    BitmapDrawable drawable = (BitmapDrawable) draw;
                    Bitmap bitmap = drawable.getBitmap();
                    Bitmap blurred = blurRenderScript(bitmap, 25);
                    blurred.setDensity(25);
                    Drawable picture = new BitmapDrawable(blurred);
                    picture.setDither(true);
                    picture.setAlpha(200);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        blurBg.setBackground(picture);
                    }
                }
            });

            Glide.with(this).load(this.postImage).diskCacheStrategy(DiskCacheStrategy.ALL).crossFade().into(this.featured_image);
            CommentsList = new ArrayList();
            commentAdapter = new CommentAdapter(this, this.CommentsList);
            postComments.setAdapter(this.commentAdapter);
            postNewComment.setOnClickListener(new View.OnClickListener() {
                public void onClick(View paramAnonymousView) {
                    SingleMusicActivity.this.postComment();
                }
            });

            if (appSharedPref.getBoolean("audioAutoLoad", false)) {
                loadAudio(false);
            }
            focusChangeListener =new AudioManager.OnAudioFocusChangeListener() {
                @Override
                public void onAudioFocusChange(int focusChange) {

                   /* try
                    {
                        switch (focusChange)
                        {
                            case (AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK):
                                mediaPlayer.setVolume(0.2f,0.2f);

                                break;
                            case (AudioManager.AUDIOFOCUS_LOSS):
                                mediaPlayer.pause();
                                mediaPlayer.release();
                                playing=!playing;
                                break;
                            case (AudioManager.AUDIOFOCUS_LOSS_TRANSIENT):
                                mediaPlayer.pause();
                                playing=!playing;
                                break;


                        }
                   }catch (Exception e)
                    {

                    }*/

                }
            };
        }
    }

    @Override
    public void onClick(View view, int position) {

        SinglePostActivity activity = new SinglePostActivity();
        PostListClass postlistclass = relatedPosts.get(position);
        PostClass newPost = new PostClass(postlistclass.id, Html.fromHtml(postlistclass.title).toString(), activity.formatDate(postlistclass.date), postlistclass.authorName, postlistclass.content, postlistclass.featuredImage, postlistclass.link, postlistclass.categoryOne, postlistclass.song_url);
        ArrayList<PostListClass> Posts = new ArrayList<>();
        Posts.addAll(relatedPosts);
        Posts.remove(position);
        Intent intent;
        if (newPost.category.equalsIgnoreCase("MUSIC") && !TextUtils.isEmpty(newPost.song_url)) {
            intent = new Intent(SingleMusicActivity.this, SingleMusicActivity.class);
        } else {
            intent = new Intent(SingleMusicActivity.this, SinglePostActivity.class);
        }



        intent.putExtra("post", newPost);
        intent.putExtra("returnState", 0);
        intent.putParcelableArrayListExtra("relatedPosts", Posts);
        startActivity(intent);
    }


  private Bitmap RGB565toARGB888(Bitmap paramBitmap)
    throws Exception
  {
    int[] arrayOfInt = new int[paramBitmap.getWidth() * paramBitmap.getHeight()];
    paramBitmap.getPixels(arrayOfInt, 0, paramBitmap.getWidth(), 0, 0, paramBitmap.getWidth(), paramBitmap.getHeight());
    paramBitmap = Bitmap.createBitmap(paramBitmap.getWidth(), paramBitmap.getHeight(), Bitmap.Config.ARGB_8888);
    paramBitmap.setPixels(arrayOfInt, 0, paramBitmap.getWidth(), 0, 0, paramBitmap.getWidth(), paramBitmap.getHeight());
    return paramBitmap;
  }



    private void loadAudio(final Boolean start)
    {
        try
        {

            loadingMusic=true;
            mediaPlayer.setDataSource(this, Uri.parse(this.songUrl));
            mediaPlayer.prepareAsync();
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    musicLoading.setVisibility(View.INVISIBLE);
                    loaded=true;
                    loadingMusic=false;
                    if(start && audioAccess)
                    {
                        mediaPlayer.start();
                        play.setImageResource(R.drawable.icon_pause);
                        playing=!playing;
                    }
                }
            });
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
            {
                public void onCompletion(MediaPlayer paramAnonymousMediaPlayer)
                {

                    play.setImageResource(R.drawable.icon_play);
                    playing=!playing;
                }
            });

        }
        catch (Exception e) {}

    }

  
  private void addFavourite()
  {
    Gson localGson = new Gson();
    new ArrayList();
    Type localType = new TypeToken() {}.getType();
    ArrayList localArrayList = (ArrayList)localGson.fromJson(this.appSharedPref.getString("favourites", ""), localType);
    Object localObject = new PostListClass(this.forwardReturnPost.id, this.forwardReturnPost.date, this.forwardReturnPost.title, "music", "", this.forwardReturnPost.featuredImage, this.forwardReturnPost.authorName, this.forwardReturnPost.content, this.forwardReturnPost.link, this.forwardReturnPost.song_url);
    localArrayList.add(this.forwardReturnPost.id, localObject);
    localObject = this.appSharedPref.edit();
    ((SharedPreferences.Editor)localObject).putString("favourites", localGson.toJson(localArrayList, localType));
    ((SharedPreferences.Editor)localObject).commit();
  }

    @SuppressLint("NewApi")
    private Bitmap blurRenderScript(Bitmap smallBitmap, int radius) {

        try {
            smallBitmap = RGB565toARGB888(smallBitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }


        Bitmap bitmap = Bitmap.createBitmap(
                smallBitmap.getWidth(), smallBitmap.getHeight(),
                Bitmap.Config.ARGB_8888);

        RenderScript renderScript = RenderScript.create(this);

        Allocation blurInput = Allocation.createFromBitmap(renderScript, smallBitmap);
        Allocation blurOutput = Allocation.createFromBitmap(renderScript, bitmap);

        ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(renderScript,
                Element.U8_4(renderScript));
        blur.setInput(blurInput);
        blur.setRadius(radius); // radius must be 0 < r <= 25
        blur.forEach(blurOutput);

        blurOutput.copyTo(bitmap);
        renderScript.destroy();

        return bitmap;

    }

    private void fetchComments()
  {
    new DataFetcher(this, commentBase + page, new SinglePostFragment(), this, "commentMusic", "", "", "").execute();
  }

    @Override
    public void onResume()
    {
        super.onResume();

    }

  
  private void fetchPosts()
  {
    new DataFetcher(this, this.postBase, new SinglePostFragment(), this, "postMusic", "", "", "").execute();

  }
  
  private void removeFavourite()
  {
    Gson localGson = new Gson();
    new ArrayList();
    Type localType = new TypeToken() {}.getType();
    this.appSharedPref = getSharedPreferences("My_Preference", 0);
    ArrayList localArrayList = (ArrayList)localGson.fromJson(this.appSharedPref.getString("favourites", ""), localType);
    new PostListClass(this.forwardReturnPost.id, this.forwardReturnPost.date, this.forwardReturnPost.title, "music", "", this.forwardReturnPost.featuredImage, this.forwardReturnPost.authorName, this.forwardReturnPost.content, this.forwardReturnPost.link, this.forwardReturnPost.song_url);
    localArrayList.remove(this.forwardReturnPost.id);
    SharedPreferences.Editor localEditor = this.appSharedPref.edit();
    localEditor.putString("favourites", localGson.toJson(localArrayList, localType));
    localEditor.commit();
  }
  

  
  private void sharePost()
  {
    String str = Uri.parse(this.postLink).toString();
    Intent localIntent = new Intent("android.intent.action.SEND");
    localIntent.setType("text/plain");
    localIntent.putExtra("android.intent.extra.TEXT", this.postTitle + " " + ":" + str);
    startActivity(Intent.createChooser(localIntent, "Share via"));
  }

    public void download(View v )
    {
        try {
            if (isExternalStorageWritable()) {
                Uri source = Uri.parse(songUrl);
                DownloadManager.Request request = new DownloadManager.Request(source);

                request.setTitle(postTitle);
                request.setDescription("Donwloading..");

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    String songTitle=postTitle.replace("MUSIC","");
                    songTitle=songTitle+"[vstarvibes.com].mp3";
                    request.setDestinationInExternalPublicDir(Environment.DIRECTORY_MUSIC, songTitle);
                   //request.setDestinationUri(downloadLocation);
                    request.allowScanningByMediaScanner();
                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                    DownloadManager manager = (DownloadManager) this.getSystemService(Context.DOWNLOAD_SERVICE);
                    manager.enqueue(request);

                }

            }

        }catch (Exception e)
        {

        }
    }


    /*--Check if external files is writable() */
    public  Boolean isExternalStorageWritable()
    {
        String state=Environment.getExternalStorageState();
        if(Environment.MEDIA_MOUNTED.equals(state))
        {
            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M)
            {
                if(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED)
                {
                    return true;
                }
                else
                {
                    requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},1);
                    requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},1);
                }

            }
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,int[] grantResults){
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (isExternalStorageWritable()) {
                download(new View(this));
                //continueAction();
            }
        }
    }

    public  Boolean continueAction()
    {
        return  true;
    }
  public void favouriteAdded() {}

    public String formatDate(String date) {

        Date today = new Date();
        String current = today.toString();
        Date postdate = null;

        Long mSeconds;
        long day, hours, minutes, seconds;
        Boolean sameYear=false;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat yearFormat=new SimpleDateFormat("yyyy");
        SimpleDateFormat defaultFormat=new SimpleDateFormat("MMM dd");
        SimpleDateFormat format=new SimpleDateFormat("MMM dd, yyyy");

        try {
            postdate = dateFormat.parse(date);
            today= dateFormat.parse(current);

        } catch (Exception e) {
        }

        mSeconds = today.getTime() - postdate.getTime();
        //seconds= TimeUnit.MILLISECONDS.toSeconds(mSeconds);
        minutes=TimeUnit.MILLISECONDS.toMinutes(mSeconds);
        hours=TimeUnit.MILLISECONDS.toHours(mSeconds);
        day=TimeUnit.MILLISECONDS.toDays(mSeconds);
        String originalYear=yearFormat.format(postdate);
        String yearNow=yearFormat.format(today);
        if(originalYear.equals(yearNow))
        {
            sameYear=true;
        }

        if(day>7 &&sameYear )
        {
            return defaultFormat.format(postdate);
        }

        else if(day==1 &&sameYear )
        {
            return Long.toString(day) +  " day ago";
        }

        else if(day>1 &&day<=7 &&sameYear )
        {
            return Long.toString(day) +  " days ago";
        }
        else if(hours>1 &&hours <=23 && sameYear)
        {
            return Long.toString(hours) +  " hours ago";
        }
        else if(minutes>1 && minutes<60)
        {
            return Long.toString(minutes) +" minutes ago";
        }
        else if(hours==1)
        {
            return  Long.toString(hours) + " hour ago";
        }

        else if(minutes==1)
        {

            return Long.toString(minutes) +" minute ago";
        }

        else if(minutes<1 && sameYear)
        {

            return " just now";
        }

        else
        {
            return format.format(postdate);
        }



    }

    public void getComments(String comments, String method)
    {

        if(!TextUtils.isEmpty(comments) && comments !="[]" &&comments !=null )
        {

            try
            {
                gson = new Gson();
                Type type=new TypeToken<List<CommentClass>>(){}.getType();
                CommentsList=gson.fromJson(comments, type);
                if(CommentsList.size()==0)
                {

                    commentLoader.setVisibility(View.GONE);
                    noCommentText.setVisibility(View.VISIBLE);
                    noCommentImage.setVisibility(View.VISIBLE);
                    progressDialog.dismiss();
                }
                else
                {
                    commentLoader.setVisibility(View.GONE);
                    if(method=="get")
                    {
                        commentAdapter.updateView(CommentsList);
                    }
                    else
                    {

                        commentAdapter.addToComment(CommentsList);
                        progressDialog.dismiss();
                        noCommentText.setVisibility(View.INVISIBLE);
                        noCommentImage.setVisibility(View.INVISIBLE);
                        newCommentContent.setText("");
                    }

                }
            }catch (Exception e)
            {

                commentLoader.setVisibility(View.GONE);
                noCommentText.setVisibility(View.VISIBLE);
                noCommentImage.setVisibility(View.VISIBLE);
            }



        }
        else
        {

            commentLoader.setVisibility(View.GONE);
        }
    }

    public void getPost(String response)
    {
        if (!TextUtils.isEmpty(response)) {

            gson = new Gson();
            try {


                PostClass post = gson.fromJson(response, PostClass.class);
                postContent = post.content.trim();
            }catch (Exception e)
            {

                Snackbar snacky=Snackbar.make(coordinatorLayout,"No connection",Snackbar.LENGTH_INDEFINITE).setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        fetchPosts();

                    }
                });

                snacky.setActionTextColor(Color.GREEN);

                TextView tv = (TextView) snacky.getView().findViewById(android.support.design.R.id.snackbar_text);
                tv.setTextColor(Color.WHITE);
                snacky.show();

            }
            forwardReturnPost=new PostClass(postId,postTitle,postDate,authorName,postContent,postImage,postLink,category,songUrl);
            if(postContent!=null)
            {
                content.loadDataWithBaseURL(null, start + style + postContent + "</html>", "text/html", "UTF-8", null);
            }


            related_title.setVisibility(View.VISIBLE);

        }


        else
        {

            Snackbar snacky=Snackbar.make(coordinatorLayout,"No connection",Snackbar.LENGTH_INDEFINITE).setAction("RETRY", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    fetchPosts();

                }
            });

            snacky.setActionTextColor(Color.GREEN);

            TextView tv = (TextView) snacky.getView().findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.WHITE);
            snacky.show();
        }


    }
  
  public void onBackPressed()
  {
    super.onBackPressed();
      try
      {
          mediaPlayer.pause();
          mediaPlayer.release();
      }catch (Exception e)
      {


      }
    Intent localIntent = new Intent(this, MainActivity.class);
    localIntent.putExtra("returnState", this.returnState);
    startActivity(localIntent);
  }



    @Override
    protected  void onStop()
    {
        super.onStop();
        try
        {
            mediaPlayer.pause();
            play.setImageResource(R.drawable.icon_play);
            playing=!playing;
            mediaPlayer.release();
        }catch (Exception e)
        {

        }
    }
  


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.single_post_option, menu);
        return true;
    }


    private void showInterstitial()
    {
        try {
            if(mInterstitialAd.isLoaded())
            {
                mInterstitialAd.show();
            }
        }catch (Exception e)

        {

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id==android.R.id.home) {
            try
            {
                mediaPlayer.pause();
                mediaPlayer.release();
            }catch (Exception e)
            {


            }
            Intent intent=new Intent(this, MainActivity.class);
            intent.putExtra("returnState",returnState);
            startActivity(intent);
            return true;
        }
       /* if (id == R.id.action_favourite) {

            return super.onOptionsItemSelected(item);
        }*/

        if (id == R.id.action_comments) {
            showComments();
            return true;
        }

        if (id == R.id.action_share) {
            sharePost();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }


  
  public void play(View paramView)
  {

    try
    {
        int result=audioManager.requestAudioFocus(focusChangeListener,AudioManager.STREAM_MUSIC,AudioManager.AUDIOFOCUS_GAIN);
        if(result==AudioManager.AUDIOFOCUS_REQUEST_GRANTED)
        {
            audioAccess=true;
        }
        if(!playing )
        {
            if(loaded && audioAccess)
            {

                mediaPlayer.start();
                play.setImageResource(R.drawable.icon_pause);
                playing=!playing;
            }
            else
            {
                musicLoading.setVisibility(View.VISIBLE);
               loadAudio(true);
            }


        }
        else
        {
            mediaPlayer.pause();
            play.setImageResource(R.drawable.icon_play);
            playing=!playing;
        }
    }
    catch (Exception e)
    {
       // Toast.makeText(this,"error",Toast.LENGTH_LONG).show();
    }
  }



    public void postComment()
    {
        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Posting...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        HelperClass helperClass=new HelperClass();
        UserClass user=helperClass.getUser(this);
        if(user !=null)
        {
            String comments= newCommentContent.getText().toString();
            int post_Id=postId;
            String username=user.name;
            Date date = new Date();
            String date_string="2017-05-26";
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            try {
                date_string= dateFormat.format(date);

            } catch (Exception e) {
            }
            CommentClass commentClass=new CommentClass(post_Id,username,user.profilePics,date_string,comments);
            ArrayList <CommentClass> comment=new ArrayList<>();
            comment.add(commentClass);
            gson=new Gson();

            Type type=new TypeToken<List<CommentClass>>(){}.getType();
            String newComments=gson.toJson(comment,type);

            getComments(newComments,"post");
            //String posCommentBase = "https://vstarvibes.com/wp-json/wp/v2/comments/?&author_name="+username+"&,content="+comments+"&post="+post_Id+"";
            new DataFetcher(this, "",new SinglePostFragment(), this,"postComment",username,comments,Integer.toString(post_Id)).execute();

        }
        else
        {
            Toast.makeText(this,"Login to comments",Toast.LENGTH_LONG).show();
            progressDialog.dismiss();
        }
    }

    public void showComments()
    {
        commentLoader.setVisibility(View.VISIBLE);
        popComments=new PopupWindow(commentsView, width,height,true);
        popComments.setAnimationStyle(android.R.style.Animation_Dialog);
        popComments.setBackgroundDrawable(getResources().getDrawable(R.drawable.commentbackground));
        popComments.setFocusable(true);
        popComments.setOutsideTouchable(true);
        popComments.showAtLocation(commentsView, Gravity.CENTER,0,0);
        fetchComments();
    }

    private class BrowserClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView webView, String url)
        {

            if(url.endsWith(".mp3") || url.endsWith(".mp4") || url.endsWith(".3gp")|| url.endsWith(".pdf") || url.endsWith(".apk") || url.endsWith(".avi")|| url.endsWith(".flv"))
            {
                if(isExternalStorageWritable()) {

                    String[] splits = url.split(Pattern.quote("."));
                    Uri source = Uri.parse(url);
                    DownloadManager.Request request = new DownloadManager.Request(source);
                    request.setDescription("Donwloading..");
                    String extension = splits[splits.length - 1];



                    try {
                        if (singlePostActivity.isExternalStorageWritable()) {

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                                String title = postTitle + "." + extension;
                                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, title);
                                request.allowScanningByMediaScanner();
                                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                                DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                                manager.enqueue(request);

                            }

                        }


                    } catch (Exception e) {

                    }
                }
            }
            else {

                Intent open = new Intent(SingleMusicActivity.this, ActivityBrowser.class);
                open.putExtra("url", url);
                open.putExtra("forwardReturnPost", forwardReturnPost);
                open.putParcelableArrayListExtra("relatedPosts", relatedPosts);
                content.clearCache(true);
                startActivity(open);

            }

            return true;



        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);

        }


    }
}
