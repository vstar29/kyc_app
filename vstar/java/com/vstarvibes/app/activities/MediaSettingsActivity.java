package com.vstarvibes.app.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;

import com.vstarvibes.app.R;

/**
 * Created by VICTOR on 14-Dec-16.
 */

public class MediaSettingsActivity extends AppCompatActivity {

    Toolbar toolbar;
    private boolean shouldLoadHomeFragOnBackPress = true;
    private SwitchCompat showImage;
    private SwitchCompat audioAutoLoad;
    private CheckBox medium;
    private CheckBox low;
    RelativeLayout relativeLayout;
    SharedPreferences appSharedPref;
    SharedPreferences.Editor editor;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_media_settings);
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        toolbar.setTitle("Notification");
        toolbar.setTitleTextColor(Color.WHITE);
        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_action_nav_left);
        showImage=(SwitchCompat) findViewById(R.id.showImage);
        audioAutoLoad=(SwitchCompat) findViewById(R.id.music_autoload);
        appSharedPref=this.getSharedPreferences("My_Preference", 0);
        editor = appSharedPref.edit();
        relativeLayout=(RelativeLayout) findViewById(R.id.swap);

        medium=(CheckBox) findViewById(R.id.medium);
        low=(CheckBox) findViewById(R.id.low);

        if(appSharedPref.getBoolean("showImage",true))
        {
            showImage.setChecked(true);
        }
        else
        {
            showImage.setChecked(false);
            relativeLayout.setVisibility(View.INVISIBLE);
        }

        if(appSharedPref.getBoolean("audioAutoLoad",false))
        {
            audioAutoLoad.setChecked(true);
        }
        else
        {
            audioAutoLoad.setChecked(false);
        }
        if(appSharedPref.getString("imageQuality","Low")=="Low")
        {
            low.setChecked(true);
            medium.setChecked(false);
        }
        else if(appSharedPref.getString("imageQuality","Low")=="Medium")
        {
            low.setChecked(false);
            medium.setChecked(true);
        }
        else
        {
            low.setChecked(true);
            medium.setChecked(false);
        }



        showImage.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    relativeLayout.setVisibility(View.VISIBLE);
                    editor.putBoolean("showImage", true);
                    editor.commit();
                }else
                {

                    relativeLayout.setVisibility(View.INVISIBLE);
                    editor.putBoolean("showImage", false);
                    editor.commit();
                }
            }
        });

        audioAutoLoad.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            if(isChecked)
            {
                editor.putBoolean("audioAutoLoad",true);
                editor.commit();
            }
            else
            {
                editor.putBoolean("audioAutoLoad",false);
                editor.commit();

            }


            }
        });



        setSupportActionBar(toolbar);
    }



    public void setImageQuality(View v)
    {

        boolean checked=((CheckBox) v).isChecked();
        switch(v.getId())
        {

            case R.id.medium:
                if(checked)
                {
                    editor.putString("imageQuality","Medium");
                    editor.commit();
                    low.setChecked(false);
                }

                break;

            case R.id.low:
                if(checked)
                {
                    editor.putString("imageQuality","Low");
                    editor.commit();
                    medium.setChecked(false);
                }

                break;



        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(this,SettingActivity.class);
        startActivity(intent);
    }
}
