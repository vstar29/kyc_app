package com.vstarvibes.app.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.vstarvibes.app.R;
import com.vstarvibes.app.others.HelperClass;
import com.vstarvibes.app.others.SettingsListAdapter;
import com.vstarvibes.app.others.UserClass;

/**
 * Created by VICTOR on 12-Dec-16.
 */

public class SettingActivity extends AppCompatActivity {

    Toolbar toolbar;
    private boolean shouldLoadHomeFragOnBackPress = true;
    private ListView listView;
    UserClass user;
    HelperClass helperClass;
    Boolean logIn=false;
    private ImageView  imgProfile;
    private TextView  txtUserName;
    Bitmap avatar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.setting_main);
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        listView=(ListView) findViewById(R.id.listView);
        View listHeader=getLayoutInflater().inflate(R.layout.settings_header,null);
        listView.addHeaderView(listHeader);
        listView.setHeaderDividersEnabled(true);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Settings");
        toolbar.setTitleTextColor(Color.WHITE);
        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_action_nav_left);
        actionBar.setTitle("Settings");
        SettingsListAdapter settingsListAdapter=new SettingsListAdapter(this);
        txtUserName = (TextView) listHeader.findViewById(R.id.username);
        imgProfile = (ImageView) listHeader.findViewById(R.id.img_profile);
        helperClass=new HelperClass();
        user=helperClass.getUser(this);
        if(user!=null)
        {
            txtUserName.setText(user.name);

            if(avatar!=null)
            {
                imgProfile.setImageBitmap(avatar);
            }
            else
            {

                Glide.with(this).load(user.profilePics).asBitmap().diskCacheStrategy(DiskCacheStrategy.ALL).into(new SimpleTarget<Bitmap>(100, 100) {

                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        avatar=resource;

                        imgProfile.setImageBitmap(avatar);

                    }
                });
            }
            //Glide.with(this).load(user.profilePics).placeholder(R.drawable.unknown).error(R.drawable.unknown).crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(imgProfile);
            logIn=true;

        }

       listView.setAdapter(settingsListAdapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                position -= listView.getHeaderViewsCount();

                switch (position)

                {
                    case 0:
                        Intent intent=new Intent(SettingActivity.this, NotificationSettingsActivity.class);
                        startActivity(intent);
                        break;
                    case 1:

                        Intent intentMedia=new Intent(SettingActivity.this, MediaSettingsActivity.class);
                        startActivity(intentMedia);
                        break;

                    case 2:
                        Intent external=new Intent(Intent.ACTION_VIEW, Uri.parse("https://vacvi.com"));
                        startActivity(external);
                        break;

                }



            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(this,MainActivity.class);;
        startActivity(intent);
    }
}
