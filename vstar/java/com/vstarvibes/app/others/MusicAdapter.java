package com.vstarvibes.app.others;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.vstarvibes.app.R;

import java.util.ArrayList;

/**
 * Created by VICTOR on 15-Oct-16.
 */
public class MusicAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<PostListClass> postList;
    private LayoutInflater layoutinflater;
    SharedPreferences appSharedPref;


    public MusicAdapter(Context context, ArrayList<PostListClass> postList) {
        this.context = context;
        layoutinflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.postList = postList;
        this.appSharedPref=context.getSharedPreferences("My_Preference", 0);

    }

    public void updateGridData(ArrayList<PostListClass> fresh)
    {
       /* postList.clear();
        postList.addAll(fresh);
       this.notifyDataSetChanged();*/
        postList=fresh;
        this.notifyDataSetChanged();
    }



   @Override
    public int getCount() {
        return postList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder listViewHolder;
        View musicView;
        if(convertView == null){
            musicView = layoutinflater.inflate(R.layout.music_model,parent,false);
            listViewHolder = new ViewHolder();
            listViewHolder.title = (TextView)musicView.findViewById(R.id.post_title);
            listViewHolder.postFeaturedImage = (ImageView)musicView.findViewById(R.id.post_image);
            musicView.setTag(listViewHolder);
        }else{
            musicView=convertView;
            listViewHolder = (ViewHolder)musicView.getTag();
        }

        listViewHolder.title = (TextView)musicView.findViewById(R.id.post_title);
        listViewHolder.postFeaturedImage = (ImageView)musicView.findViewById(R.id.post_image);

        listViewHolder.title.setText(postList.get(position).getTitle());
        if(appSharedPref.getBoolean("showImage",true)) {
            Glide.with(context).load(postList.get(position).getFeaturedImage()).into(listViewHolder.postFeaturedImage);
        }



        return musicView;
    }

     class ViewHolder{

        ImageView postFeaturedImage;
        TextView title;
    }

}
