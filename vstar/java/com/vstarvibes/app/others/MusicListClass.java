package com.vstarvibes.app.others;

/**
 * Created by VICTOR on 15-Oct-16.
 */
public class MusicListClass {

    public int id;
    public String title;
    public int featuredImage;


    public MusicListClass(int id, String title, int featuredImage )
    {
        this.id=id;
        this.title=title;
        this.featuredImage=featuredImage;

    }

    public String getTitle()
    {
        return title;
    }


    public int getId() {
        return id;
    }

    public int getFeaturedImage() {
        return featuredImage;
    }

}
