package com.vstarvibes.app.others;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.vstarvibes.app.others.PostListClass;
import com.vstarvibes.app.others.UserClass;

/**
 * Created by VICTOR on 08-Nov-16.
 */
public class HelperClass {
    SharedPreferences appSharedPref;
    Gson gson;
    Type type=new TypeToken<List<PostListClass>>(){}.getType();
    Type userType=new TypeToken<List<UserClass>>(){}.getType();



    public void savePostList( Context context,String name, ArrayList<PostListClass> arrayList )
    {
        gson=new Gson();
        appSharedPref= context.getSharedPreferences("My_Preference", 0);
        SharedPreferences.Editor editor=appSharedPref.edit();
        String latestPosts=gson.toJson(arrayList,type);
        editor.putString(name, latestPosts);
        editor.commit();

    }

    public ArrayList<PostListClass> getPostList(Context context,String name)

    {
        gson=new Gson();
        appSharedPref= context.getSharedPreferences("My_Preference", 0);
        String userString=appSharedPref.getString(name, "");
        ArrayList<PostListClass> postList=gson.fromJson(userString, type);
        return postList;
    }

    public void saveUser(Context context, UserClass user)
    {
        gson=new Gson();
        appSharedPref= context.getSharedPreferences("My_Preference", 0);
        SharedPreferences.Editor editor=appSharedPref.edit();
        String userDetails=gson.toJson(user,UserClass.class);
        editor.putString("user", userDetails);
        editor.commit();

    }


    public UserClass getUser(Context context)
    {
        gson=new Gson();
        appSharedPref= context.getSharedPreferences("My_Preference", 0);
        String userString=appSharedPref.getString("user", "");
        if(userString!="")
        {
            UserClass user=gson.fromJson(userString, UserClass.class);
            return user;
        }
        else
        {
            return null;
        }


    }

    public void logoutUser(Context context)
    {
        appSharedPref= context.getSharedPreferences("My_Preference", 0);
        SharedPreferences.Editor editor=appSharedPref.edit();
        editor.remove("user");
        editor.commit();

    }

}
