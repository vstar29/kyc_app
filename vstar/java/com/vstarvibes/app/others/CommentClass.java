package com.vstarvibes.app.others;

import android.text.Html;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by VICTOR on 01-Nov-16.
 */
public class CommentClass {

    @SerializedName("post")
    private int postId;
    @SerializedName("author_name")
    private String authorName;
    @SerializedName("author_avatar")
    private  String authorImage ;
    private String date;
    private String content;



    public CommentClass(int postId, String authorName, String authorImage, String date, String content )
    {
        this.postId=postId;
        this.authorName=authorName;
        this.authorImage=authorImage;
        this.date=date;
        this.content=content;
    }

    public int getPostId()
    {
        return postId;
    }

    public String getAuthorName() {

        return authorName;
    }

    public String getAuthorImage() {
        return authorImage;
    }

    public String getDate() {
        
        Date today = new Date();
        String current = today.toString();
        Date postdate = null;

        Long mSeconds;
        long day, hours, minutes, seconds;
        Boolean sameYear=false;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat yearFormat=new SimpleDateFormat("yyyy");
        SimpleDateFormat defaultFormat=new SimpleDateFormat("MMM dd");
        SimpleDateFormat format=new SimpleDateFormat("MMM dd, yyyy");

        try {
            postdate = dateFormat.parse(date);
            today= dateFormat.parse(current);

        } catch (Exception e) {
        }

        mSeconds = System.currentTimeMillis() - postdate.getTime();
        seconds= TimeUnit.MILLISECONDS.toSeconds(mSeconds);
        minutes=TimeUnit.MILLISECONDS.toMinutes(mSeconds);
        hours=TimeUnit.MILLISECONDS.toHours(mSeconds);
        day=TimeUnit.MILLISECONDS.toDays(mSeconds);
        String originalYear=yearFormat.format(postdate);
        String yearNow=yearFormat.format(today);
        if(originalYear.equals(yearNow))
        {
            sameYear=true;
        }

        if(day>7 &&sameYear )
        {
            return defaultFormat.format(postdate);
        }

        else if(day==1 &&sameYear )
        {
            return Long.toString(day) +  " day ago";
        }

        else if(day>1 &&day<=7 &&sameYear )
        {
            return Long.toString(day) +  " days ago";
        }
        else if(hours>1 &&hours <=23 && sameYear)
        {
            return Long.toString(hours) +  " hours ago";
        }

        else if(minutes>1 && minutes<60)
        {
            return Long.toString(minutes) +" minutes ago";
        }
       else if(hours==1)
        {
            return  Long.toString(hours) + " hour ago";
        }

        else if(minutes==1)
        {

            return Long.toString(minutes) +" minute ago";
        }

        else if(minutes<1 && sameYear)
        {

            return " just now";
        }

        else
        {
            return format.format(postdate);
        }



    }

   public String getContent() {
        return Html.fromHtml(content).toString();
    }
}
