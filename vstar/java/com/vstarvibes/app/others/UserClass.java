package com.vstarvibes.app.others;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by VICTOR on 08-Nov-16.
 */
public class UserClass implements Parcelable {

    public     String id;
    public     String name;
    public     String email;
    public     String profilePics;


    public UserClass(String id, String name, String email, String profilePics)
    {
        this.id=id;
        this.name=name;
        this.email=email;
        this.profilePics=profilePics;


    }

    public UserClass(Parcel in)
    {
        id=in.readString();
        name=in.readString();
        email=in.readString();
        profilePics=in.readString();

    }

    @Override
    public void writeToParcel(Parcel out, int flags)
    {
        out.writeString(id);
        out.writeString(name);
        out.writeString(email);
        out.writeString(profilePics);

    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserClass> CREATOR
            = new Creator<UserClass>() {
        // @Override
        public UserClass createFromParcel(Parcel in) {
            return new UserClass(in);
        }

        // @Override
        public UserClass[] newArray(int size) {
            return new UserClass[size];
        }
    };


    public String getId()
    {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getProfilePics() {
        return profilePics;

    }


}
