package com.vstarvibes.app.others;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;

import com.vstarvibes.app.fragments.SinglePostFragment;

import java.util.ArrayList;

/**
 * Created by VICTOR on 21-Oct-16.
 */
public class SinglePostAdapter extends FragmentPagerAdapter {


    private Context context;
    private PostClass post;
    private LayoutInflater layoutinflater;
    private ArrayList<PostListClass> relatedPosts;
    private PostClass returnPost;
    private  String category;

    public SinglePostAdapter(Context context, FragmentManager fm, PostClass post, ArrayList<PostListClass> relatedPosts, PostClass returnPost, String category) {
        super(fm);
        this.post = post;
        this.relatedPosts = relatedPosts;
        this.returnPost=returnPost;
        this.category=category;
        this.context=context;

    }


    @Override
    public int getCount() {
        return (1);
    }

    @Override
    public Fragment getItem(int position) {


        Fragment fragment = new SinglePostFragment();
        Bundle posts = new Bundle();
        posts.putParcelable("posts", post);
        posts.putParcelableArrayList("relatedPosts", relatedPosts);
        posts.putParcelable("returnPost", returnPost);
        fragment.setArguments(posts);
        return fragment;

    }

    @Override
    public long getItemId(int position) {
        return position;
    }


}