package com.vstarvibes.app.others;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vstarvibes.app.R;

/**
 * Created by VICTOR on 13-Dec-16.
 */

public class SettingsListAdapter extends BaseAdapter {


    private Context context;
    private String[] Name;
    private int[] Icon;
    private LayoutInflater layoutinflater;
    String versionName="";
    TypedArray typedArray;



    public SettingsListAdapter(Context context) {
        this.context = context;
        layoutinflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typedArray=context.getResources().obtainTypedArray(R.array.setting_icon);
        Resources resources=context.getResources();
        Name=new String[]{"Notification","Media","About and help","Version:"+getVersion()};
      Icon=resources.getIntArray(R.array.setting_icon);

    }

    @Override
    public int getCount() {
        return Name.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    private  String getVersion()
    {

        try {

                PackageInfo packageInfo=context.getPackageManager().getPackageInfo(context.getPackageName(),0);
                versionName=packageInfo.versionName;
        }catch (PackageManager.NameNotFoundException e)
        {

        }
        return versionName;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

     ViewHolder listViewHolder;
        if(convertView == null){

            convertView = layoutinflater.inflate(R.layout.settings_model,parent,false);
            listViewHolder = new ViewHolder();
            listViewHolder.title = (TextView)convertView.findViewById(R.id.name);
            listViewHolder.image=(ImageView) convertView.findViewById(R.id.icon);
            listViewHolder.divider=(View) convertView.findViewById(R.id.divider);
            listViewHolder.wrapper=(LinearLayout) convertView.findViewById(R.id.wrapper);



            convertView.setTag(listViewHolder);
        }else{
            listViewHolder = (ViewHolder)convertView.getTag();
        }

        listViewHolder.title.setText(Name[position]);
        listViewHolder.image.setImageResource(typedArray.getResourceId(position,R.drawable.ic_blank));
        if(position==0)
        {
            listViewHolder.divider.setVisibility(View.INVISIBLE);
           RelativeLayout.LayoutParams params=new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            params.setMargins(1,10,0,0);
            listViewHolder.wrapper.setLayoutParams(params);

        }



        return convertView;
    }

    static class ViewHolder{

        ImageView image;
        TextView title;
        View divider;
        LinearLayout wrapper;
    }


}

