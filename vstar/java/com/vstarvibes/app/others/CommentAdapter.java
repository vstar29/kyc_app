package com.vstarvibes.app.others;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.vstarvibes.app.R;

import java.util.ArrayList;

/**
 * Created by VICTOR on 15-Oct-16.
 */
public class CommentAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<CommentClass> Comments;
    private LayoutInflater layoutinflater;


    public CommentAdapter(Context context, ArrayList<CommentClass> Comments) {
        this.context = context;
        layoutinflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.Comments=Comments;
    }

    @Override
    public int getCount() {
        return Comments.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public  void updateView(ArrayList<CommentClass> fresh)
    {
        Comments.clear();
        Comments.addAll(fresh);
        notifyDataSetChanged();
    }

    public  void addToComment(ArrayList<CommentClass> updated)
    {
        Comments.addAll(updated);
        notifyDataSetChanged();

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View comments;
        View Rest=layoutinflater.inflate(R.layout.single_content,parent,false);
        Holder listHolder;
         HelperClass helperClass=new HelperClass();
         UserClass user=helperClass.getUser(context);

        if(convertView == null){
            comments = layoutinflater.inflate(R.layout.comment_model,parent,false);

            listHolder = new Holder();
            listHolder.AuthorName = (TextView)comments.findViewById(R.id.author_name);
            listHolder.date = (TextView)comments.findViewById(R.id.comment_date);
            listHolder.content=(TextView)comments.findViewById(R.id.content);
            listHolder.AuthorImage = (ImageView)comments.findViewById(R.id.author_image);
            listHolder.ImageAlternative=(TextView)comments.findViewById(R.id.avatarTag);
            comments.setTag(listHolder);
        }else{
            comments=convertView;
            listHolder = (Holder)comments.getTag();
        }
        if(!Comments.isEmpty())
        {

            if(user!=null && user.name.equalsIgnoreCase(Comments.get(position).getAuthorName()))

            {
                listHolder.ImageAlternative.setVisibility(View.GONE);
                Glide.with(context).load(user.profilePics).diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.unknown).into(listHolder.AuthorImage);
            }
            else if(Comments.get(position).getAuthorImage().isEmpty())
            {
                listHolder.AuthorImage.setVisibility(View.GONE);
                listHolder.ImageAlternative.setText(Comments.get(position).getAuthorName().charAt(0));
            }
            else
            {
                listHolder.ImageAlternative.setVisibility(View.GONE);
                Glide.with(context).load(Comments.get(position).getAuthorImage()).diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.unknown).into(listHolder.AuthorImage);
            }
        }
        listHolder.date.setText(Comments.get(position).getDate());

        listHolder.AuthorName.setText(Comments.get(position).getAuthorName());
        listHolder.content.setText(Comments.get(position).getContent());
        return comments;
    }

    static class Holder{

        ImageView AuthorImage;
        TextView content;
        TextView date;
        TextView AuthorName;
        TextView ImageAlternative;

    }

}
