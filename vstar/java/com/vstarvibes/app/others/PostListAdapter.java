package com.vstarvibes.app.others;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.NativeExpressAdView;
import com.vstarvibes.app.R;

import java.util.List;

/**
 * Created by VICTOR on 15-Oct-16.
 */
public class PostListAdapter extends BaseAdapter {

    private Context context;
    private List<Object> postList;
    private LayoutInflater layoutinflater;
    private  String viewType;
    Fragment fragment;
    private static final int  NEWS_VIEW_TYPE = 0;
    private static final int ADVERT_VIEW_TYPE = 1;
    SharedPreferences appSharedPref;

    public PostListAdapter(Context context, List<Object> postList, String viewType, Fragment fragment) {
        this.context = context;
        layoutinflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.postList = postList;
        this.viewType=viewType;
        this.fragment=fragment;
        this.appSharedPref=context.getSharedPreferences("My_Preference", 0);
    }

    public void updatePostView(List<Object> fresh)
    {
        postList=fresh;
       this.notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return postList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getViewTypeCount()
    {
        return  (9);
    }

   public  View getViewType(String type,ViewGroup parent)
    {

        return  layoutinflater.inflate(R.layout.post_model, parent, false);
    }

    public int getItemViewType(int position) {
        if(position==0)
        {
            return NEWS_VIEW_TYPE;
        }
        else
        {
            return (position % 6 == 0) ? ADVERT_VIEW_TYPE
                    : NEWS_VIEW_TYPE;
        }

    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        int view = getItemViewType(position);
        switch (view) {
            case NEWS_VIEW_TYPE:
                convertView= getViewType(viewType,parent);
                ViewHolder listViewHolder;
                listViewHolder = new ViewHolder();
                listViewHolder.title = (TextView) convertView.findViewById(R.id.post_title);
                listViewHolder.postFeaturedImage = (ImageView) convertView.findViewById(R.id.post_image);
                listViewHolder.date = (TextView) convertView.findViewById(R.id.post_date);
                listViewHolder.commentCount = (TextView) convertView.findViewById(R.id.post_comment_count);
                listViewHolder.writer = (TextView) convertView.findViewById(R.id.post_author);
                convertView.setTag(listViewHolder);
                PostListClass post = (PostListClass) postList.get(position);
                if(appSharedPref.getBoolean("showImage",true)) {
                    if(post.categoryOne.contains("Jobs") || post.categoryOne.contains("Careers"))
                    {
                        String featuredImage = "https://res.cloudinary.com/vstarvibes/image/upload/v1502566975/jobs.png";
                        Glide.with(context).load(featuredImage).crossFade().thumbnail(0.5f).override(150,150).into(listViewHolder.postFeaturedImage);

                    }
                    else
                    {
                        Glide.with(context).load(post.getFeaturedImage()).crossFade().error(R.drawable.vstar_logo).into(listViewHolder.postFeaturedImage);
                    }
                }



                listViewHolder.title.setText(post.getTitle());
                listViewHolder.writer.setText(post.getAuthorName());
                listViewHolder.date.setText(post.getDate());
                listViewHolder.commentCount.setText(post.getCommentCount());
                return convertView;


            case ADVERT_VIEW_TYPE:
                // fall through
            default:

                ViewGroup layout = (ViewGroup) layoutinflater.inflate(R.layout.big_advert, parent, false);
                NativeExpressAdView adView =
                        (NativeExpressAdView) postList.get(position);
                adView.setBackgroundColor(Color.parseColor("#ede8e8e8"));


                if (layout.getChildCount() > 0) {
                    layout.removeAllViews();
                }
                if (adView.getParent() != null) {
                    ((ViewGroup) adView.getParent()).removeView(adView);
                }

                layout.addView(adView);
                return layout;
        }
    }

    static class ViewHolder{

        ImageView postFeaturedImage;
        TextView title;
        TextView writer;
        TextView date;
        TextView commentCount;

    }

}
