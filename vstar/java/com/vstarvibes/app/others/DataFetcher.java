package com.vstarvibes.app.others;

import android.app.Activity;
import android.os.AsyncTask;

import com.vstarvibes.app.activities.SingleMusicActivity;
import com.vstarvibes.app.fragments.SinglePostFragment;

/**
 * Created by VICTOR on 04-Dec-16.
 */

public class DataFetcher extends AsyncTask<Void, Void, String> {

    private Activity activity;
    private  String  url;
    private  String type;
    SinglePostFragment singlePostFragment;
    SingleMusicActivity singleMusicActivity;
    String comment;
    String id;
    String author;


    public DataFetcher(Activity activity, String url, SinglePostFragment singlePostFragment, SingleMusicActivity singleMusicActivity, String type,String author,String  comment,String id) {
        super();
        this.activity = activity;
        this.url=url;
        this.singlePostFragment=singlePostFragment;
        this.type=type;
        this.comment=comment;
        this.singleMusicActivity=singleMusicActivity;
        this.author=author;
        this.comment=comment;
        this.id=id;

    }

    @Override
    protected void onPreExecute() {


    }

    @Override
    protected String doInBackground(Void... params) {
        String Method="GET";
        HttpHandler httpHandler = new HttpHandler();
        if(type=="postComment")
        {

            String response= httpHandler.postComment(author,comment,id);
            return response;
        }
        else
        {
            String response = httpHandler.makeServiceCall(url,Method);
            return response;
        }





    }

    @Override
    protected void onPostExecute(String result) {
        if(type=="post")
        {
            singlePostFragment.getPost( result);
        }

        else if(type=="postMusic")
        {
            singleMusicActivity.getPost(result);
        }
        else if(type=="commentMusic")
        {
            singleMusicActivity.getComments(result,"get");
        }

        else if(type=="comment")
        {
            singlePostFragment.getComments(result,"get");
        }


    }




}