package com.vstarvibes.app.others;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.vstarvibes.app.fragments.FashionFragment;
import com.vstarvibes.app.fragments.GistFragment;
import com.vstarvibes.app.fragments.HomeFragment;
import com.vstarvibes.app.fragments.JobFragment;
import com.vstarvibes.app.fragments.MovieFragment;
import com.vstarvibes.app.fragments.MusicFragment;
import com.vstarvibes.app.fragments.NewsFragment;
import com.vstarvibes.app.fragments.RomanceFragment;
import com.vstarvibes.app.fragments.SportFragment;
import com.vstarvibes.app.fragments.TechFragment;
import com.vstarvibes.app.fragments.VideoFragment;

import java.util.ArrayList;

/**
 * Created by VICTOR on 14-Oct-16.
 */
public class TabsPageAdapter extends FragmentPagerAdapter {
   private FragmentManager fm;
   private int returnState=-1;
   private String posts;
    private ArrayList<PairItemClass> favouriteTabs=null;
    public TabsPageAdapter(FragmentManager fm, String posts, int returnState) {
        super(fm);
        this.fm=fm;
        this.returnState=returnState;
        this.posts=posts;
    }

    @Override
    public Fragment getItem(int index) {


        switch (index) {
            case 0:
                HomeFragment homeFragment=new HomeFragment();

                return homeFragment;
            case 1:
                NewsFragment newsFragment=new NewsFragment();
                Bundle bundle=new Bundle();
                bundle.putInt("reload",returnState);
                newsFragment.setArguments(bundle);
                return newsFragment;
            case 2:

                MusicFragment musicFragment=new MusicFragment();
                Bundle musicBundle=new Bundle();
                musicBundle.putInt("reload",returnState);
                musicFragment.setArguments(musicBundle);
                return musicFragment;

            case 3:

                GistFragment gistFragment=new GistFragment();
                Bundle gistBundle=new Bundle();
                gistBundle.putInt("reload",returnState);
                gistFragment.setArguments(gistBundle);
                return gistFragment;
            case 4:

                VideoFragment videoFragment=new VideoFragment();
                Bundle videoBundle=new Bundle();
                videoBundle.putInt("reload",returnState);
                videoFragment.setArguments(videoBundle);
                return videoFragment;
            case 5:

                SportFragment sportFragment=new SportFragment();
                Bundle sportBundle=new Bundle();
                sportBundle.putInt("reload",returnState);
                sportFragment.setArguments(sportBundle);
                return sportFragment;
            case 6:

                RomanceFragment romanceFragment=new RomanceFragment();
                Bundle romanceBundle=new Bundle();
                romanceBundle.putInt("reload",returnState);
                romanceFragment.setArguments(romanceBundle);
                return romanceFragment;
            case 7:

                MovieFragment movieFragment=new MovieFragment();
                Bundle movieBundle=new Bundle();
                movieBundle.putInt("reload",returnState);
                movieFragment.setArguments(movieBundle);
                return movieFragment;

            case 8:

                TechFragment techFragment=new TechFragment();
                Bundle techBundle=new Bundle();
                techBundle.putInt("reload",returnState);
                techFragment.setArguments(techBundle);
                return techFragment;

            case 9:

                JobFragment jobFragment=new JobFragment();
                Bundle jobBundle=new Bundle();
                jobBundle.putInt("reload",returnState);
                jobFragment.setArguments(jobBundle);
                return jobFragment;

            case 10:

                FashionFragment fashionFragment=new FashionFragment();
                Bundle fashionBundle=new Bundle();
                fashionBundle.putInt("reload",returnState);
                fashionFragment.setArguments(fashionBundle);
                return fashionFragment;


            default:

                return new HomeFragment();
        }


    }
   /* @Override
    public Fragment getItem(int index) {
            if(favouriteTabs!=null)
            {
                String cat=favouriteTabs.get(index).value;

                switch (cat) {
                    case "Home":
                        HomeFragment homeFragment=new HomeFragment();
                        return homeFragment;
                    case "News":
                        NewsFragment newsFragment=new NewsFragment();
                        Bundle bundle=new Bundle();
                        bundle.putInt("reload",index);
                        newsFragment.setArguments(bundle);
                        return newsFragment;
                    case "Music":

                        MusicFragment musicFragment=new MusicFragment();
                        Bundle musicBundle=new Bundle();
                        musicBundle.putInt("reload",index);
                        musicFragment.setArguments(musicBundle);
                        return musicFragment;

                    case "Gist":

                        GistFragment gistFragment=new GistFragment();
                        Bundle gistBundle=new Bundle();
                        gistBundle.putInt("reload",index);
                        gistFragment.setArguments(gistBundle);
                        return gistFragment;
                    case "Video":

                        VideoFragment videoFragment=new VideoFragment();
                        Bundle videoBundle=new Bundle();
                        videoBundle.putInt("reload",returnState);
                        videoFragment.setArguments(videoBundle);
                        return videoFragment;
                    case "Sport":

                        SportFragment sportFragment=new SportFragment();
                        Bundle sportBundle=new Bundle();
                        sportBundle.putInt("reload",returnState);
                        sportFragment.setArguments(sportBundle);
                        return sportFragment;
                    case "Romance":

                        RomanceFragment romanceFragment=new RomanceFragment();
                        Bundle romanceBundle=new Bundle();
                        romanceBundle.putInt("reload",returnState);
                        romanceFragment.setArguments(romanceBundle);
                        return romanceFragment;
                    case "Movie":

                        MovieFragment movieFragment=new MovieFragment();
                        Bundle movieBundle=new Bundle();
                        movieBundle.putInt("reload",returnState);
                        movieFragment.setArguments(movieBundle);
                        return movieFragment;

                    case "Tech":

                        TechFragment techFragment=new TechFragment();
                        Bundle techBundle=new Bundle();
                        techBundle.putInt("reload",returnState);
                        techFragment.setArguments(techBundle);
                        return techFragment;

                    case "Job":

                        JobFragment jobFragment=new JobFragment();
                        Bundle jobBundle=new Bundle();
                        jobBundle.putInt("reload",returnState);
                        jobFragment.setArguments(jobBundle);
                        return jobFragment;

                    case "Fashion":

                        FashionFragment fashionFragment=new FashionFragment();
                        Bundle fashionBundle=new Bundle();
                        fashionBundle.putInt("reload",returnState);
                        fashionFragment.setArguments(fashionBundle);
                        return fashionFragment;


                    default:

                        return new HomeFragment();
                }
            }
        else
            {
                switch (index) {
                    case 0:
                        HomeFragment homeFragment=new HomeFragment();

                        return homeFragment;
                    case 1:
                        NewsFragment newsFragment=new NewsFragment();
                        Bundle bundle=new Bundle();
                        bundle.putInt("reload",returnState);
                        newsFragment.setArguments(bundle);
                        return newsFragment;
                    case 2:

                        MusicFragment musicFragment=new MusicFragment();
                        Bundle musicBundle=new Bundle();
                        musicBundle.putInt("reload",returnState);
                        musicFragment.setArguments(musicBundle);
                        return musicFragment;

                    case 3:

                        GistFragment gistFragment=new GistFragment();
                        Bundle gistBundle=new Bundle();
                        gistBundle.putInt("reload",returnState);
                        gistFragment.setArguments(gistBundle);
                        return gistFragment;
                    case 4:

                        VideoFragment videoFragment=new VideoFragment();
                        Bundle videoBundle=new Bundle();
                        videoBundle.putInt("reload",returnState);
                        videoFragment.setArguments(videoBundle);
                        return videoFragment;
                    case 5:

                        SportFragment sportFragment=new SportFragment();
                        Bundle sportBundle=new Bundle();
                        sportBundle.putInt("reload",returnState);
                        sportFragment.setArguments(sportBundle);
                        return sportFragment;
                    case 6:

                        RomanceFragment romanceFragment=new RomanceFragment();
                        Bundle romanceBundle=new Bundle();
                        romanceBundle.putInt("reload",returnState);
                        romanceFragment.setArguments(romanceBundle);
                        return romanceFragment;
                    case 7:

                        MovieFragment movieFragment=new MovieFragment();
                        Bundle movieBundle=new Bundle();
                        movieBundle.putInt("reload",returnState);
                        movieFragment.setArguments(movieBundle);
                        return movieFragment;

                    case 8:

                        TechFragment techFragment=new TechFragment();
                        Bundle techBundle=new Bundle();
                        techBundle.putInt("reload",returnState);
                        techFragment.setArguments(techBundle);
                        return techFragment;

                    case 9:

                        JobFragment jobFragment=new JobFragment();
                        Bundle jobBundle=new Bundle();
                        jobBundle.putInt("reload",returnState);
                        jobFragment.setArguments(jobBundle);
                        return jobFragment;

                    case 10:

                        FashionFragment fashionFragment=new FashionFragment();
                        Bundle fashionBundle=new Bundle();
                        fashionBundle.putInt("reload",returnState);
                        fashionFragment.setArguments(fashionBundle);
                        return fashionFragment;


                    default:

                        return new HomeFragment();
                }

            }



    }*/

    @Override
    public int getCount() {

        return 11;

    }

   /* public int getReturnState(int position)
    {
        String cat=favouriteTabs.get(position).value;
        switch (position) {
            case 0:
            {
                HomeFragment homeFragment=new HomeFragment();

                Bundle post=new Bundle();
                post.putString("posts",posts);
                homeFragment.setArguments(post);

                return homeFragment;

            }

            case 1:

                return new NewsFragment();
            case 2:

                return new MusicFragment();

            case 3:

                return new GistFragment();
            case 4:

                return new VideoFragment();
            case 5:

                return new SportFragment();
            case 6:

                return new RomanceFragment();
            case 7:

                return new MovieFragment();

            case 8:

                return new TechFragment();

            case 9:

                return new JobFragment();

            case 10:

                return new FashionFragment();
            default:

                return new HomeFragment();
        }

    }*/


}
