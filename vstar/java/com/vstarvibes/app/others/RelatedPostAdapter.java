package com.vstarvibes.app.others;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.vstarvibes.app.R;

import java.util.ArrayList;

/**
 * Created by VICTOR on 15-Oct-16.
 */
public class RelatedPostAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<PostListClass> relatedPosts;
    private LayoutInflater layoutinflater;
    private ItemClickListener clickListener;
    SharedPreferences appSharedPref;




    public RelatedPostAdapter(Context context, ArrayList<PostListClass> relatedPosts) {
        this.context = context;
        layoutinflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.relatedPosts = relatedPosts;
        this.appSharedPref=context.getSharedPreferences("My_Preference", 0);
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView postFeaturedImage;
        TextView title;
        RelativeLayout parent;

        Holder(View view) {
            super(view);
            title=(TextView) view.findViewById(R.id.title);
            parent=(RelativeLayout) view.findViewById(R.id.parent);
            postFeaturedImage=(ImageView) view.findViewById(R.id.image);
            view.setOnClickListener(this);
        };

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }

    }


    @Override
    public int getItemCount() {
        return relatedPosts.size();
    }




    @Override
    public long getItemId(int position) {
        return position;
    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View relatedModel = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.related_postmodel, viewGroup, false);
        return new Holder (relatedModel);


    }

    public void setClickListener(ItemClickListener itemClickListener) {

        this.clickListener = itemClickListener;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        final int position_final=position;
            Holder itemHolder=(Holder) holder;
       itemHolder.title.setText(relatedPosts.get(position).getTitle());
                if(appSharedPref.getBoolean("showImage",true))
                {
                    Glide.with(context).load(relatedPosts.get(position).getFeaturedImage()).diskCacheStrategy(DiskCacheStrategy.ALL ).into(itemHolder.postFeaturedImage);

                }

                setAnimation(((Holder) holder).parent,position);



        }

        private  void setAnimation(RelativeLayout view, int position)
        {

                Animation animation= AnimationUtils.loadAnimation(context,android.R.anim.slide_in_left);
                view.startAnimation(animation);

        }

    }
