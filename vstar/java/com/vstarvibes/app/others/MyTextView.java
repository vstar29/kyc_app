package com.vstarvibes.app.others;


import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class MyTextView extends TextView {

    public MyTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public MyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyTextView(Context context) {
        super(context);
        init();
    }

    public void init() {
        AssetManager assetManager=getContext().getAssets();
        Typeface typefaceSanserif=Typeface.createFromAsset(assetManager, "fonts/ralewayregular.ttf");


        setTypeface(typefaceSanserif);

    }
}