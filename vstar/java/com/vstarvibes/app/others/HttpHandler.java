package com.vstarvibes.app.others;

import android.net.Uri;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by VICTOR on 06-Dec-16.
 */

public class HttpHandler {

    private static final String TAG = HttpHandler.class.getSimpleName();

    public HttpHandler() {
    }

    public String makeServiceCall(String Url,String Method) {
        String response = "empty";
        try {
            URL url = new URL(Url);
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setRequestMethod(Method);
            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());;
            response = convertStreamToString(in);
        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
        return response;
    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.d(TAG,"error: "+ e.getMessage());
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
                Log.d(TAG,"error: "+ e.getMessage());
            }
        }

        return sb.toString();
    }

    public String postComment(String author,String content, String id)
    {
        Log.d(TAG, "posting");
        String response = "empty";

        try
        {
            URL url = new URL("https://vstarvibes.com/wp-json/dec_29/comments");
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("author_name", author)
                    .appendQueryParameter("content", content)
                     .appendQueryParameter("post", id);

            String query = builder.build().getEncodedQuery();
            Log.d(TAG,"query: "+query);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();


            int responseCode=conn.getResponseCode();
            Log.d(TAG,"responsecode: "+Integer.toString(responseCode));
            InputStream in = new BufferedInputStream(conn.getInputStream());;
            response = convertStreamToString(in);

            //conn.connect();

        }catch (Exception e)
        {
            Log.d(TAG, "result: "+e.getMessage());
        }

        Log.d(TAG, "response: "+response);
        return response;

    }

    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }

}