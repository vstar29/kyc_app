package com.vstarvibes.app.others;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by VICTOR on 15-Oct-16.
 */
public class PairItemClass implements Parcelable {


    public String key;
    public String value;


    public PairItemClass( String key, String value )
    {
        this.key=key;
        this.value=value;

    }

    private PairItemClass(Parcel in )
    {
        key=in.readString();
        value=in.readString();

    }

    @Override
    public void writeToParcel(Parcel out, int flags) {

        out.writeString(key);
        out.writeString(value);


    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PairItemClass> CREATOR
            = new Creator<PairItemClass>() {
        // @Override
        public PairItemClass createFromParcel(Parcel in) {
            return new PairItemClass(in);
        }

        // @Override
        public PairItemClass[] newArray(int size) {
            return new PairItemClass[size];
        }
    };



    public String getValue()
    {
        return value;
    }
}
