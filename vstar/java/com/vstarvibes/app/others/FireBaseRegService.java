package com.vstarvibes.app.others;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;


/**
 * Created by VICTOR on 10-Nov-16.
 */
public class FireBaseRegService extends FirebaseInstanceIdService {
    
    @Override
    public void onTokenRefresh() {
        
        //subscribedAll();
        FirebaseMessaging.getInstance().subscribeToTopic("Music");
        FirebaseMessaging.getInstance().subscribeToTopic("Tech");



    }

    public  String getToken()
    {
        return  FirebaseInstanceId.getInstance().getToken();
    }

    public void subscribedUser(String topic) {

        FirebaseMessaging.getInstance().subscribeToTopic(topic);
    }

    public void unSubscribedUser(String topic) {
        FirebaseMessaging.getInstance().unsubscribeFromTopic(topic);
    }

    public void subscribedAll()
    {
        FirebaseMessaging.getInstance().subscribeToTopic("News");
        FirebaseMessaging.getInstance().subscribeToTopic("Music");
        FirebaseMessaging.getInstance().subscribeToTopic("Tech");
        FirebaseMessaging.getInstance().subscribeToTopic("Video");
        FirebaseMessaging.getInstance().subscribeToTopic("Movie");
        FirebaseMessaging.getInstance().subscribeToTopic("Sports");
        FirebaseMessaging.getInstance().subscribeToTopic("Gist");
        FirebaseMessaging.getInstance().subscribeToTopic("Love");
        FirebaseMessaging.getInstance().subscribeToTopic("Careers");
        FirebaseMessaging.getInstance().subscribeToTopic("Fashion");
    }

    public  void unSubscribedAll()
    {
        FirebaseMessaging.getInstance().unsubscribeFromTopic("News");
        FirebaseMessaging.getInstance().unsubscribeFromTopic("Music");
        FirebaseMessaging.getInstance().unsubscribeFromTopic("Tech");
        FirebaseMessaging.getInstance().unsubscribeFromTopic("Video");
        FirebaseMessaging.getInstance().unsubscribeFromTopic("Movie");
        FirebaseMessaging.getInstance().unsubscribeFromTopic("Sports");
        FirebaseMessaging.getInstance().unsubscribeFromTopic("Gist");
        FirebaseMessaging.getInstance().unsubscribeFromTopic("Love");
        FirebaseMessaging.getInstance().unsubscribeFromTopic("Careers");
        FirebaseMessaging.getInstance().unsubscribeFromTopic("Fashion");
    }

   /* public void reSub()
    {
        if(sharedPreferences.getBoolean("music_notify",true))
        {
            subscribedUser("Music");
        }
           
        if(sharedPreferences.getBoolean("video_notify",false))
        {
            subscribedUser("Video");
        }

        if(sharedPreferences.getBoolean("sport_notify",false))
        {
             subscribedUser("Sports");
        }

        if(sharedPreferences.getBoolean("news_notify",false))
        {
             subscribedUser("News");
        }

        if(sharedPreferences.getBoolean("gist_notify",false))
        {
             subscribedUser("Gist");
        }

        if(sharedPreferences.getBoolean("movie_notify",false))
        {
             subscribedUser("Movie");
        }


        if(sharedPreferences.getBoolean("tech_notify",true))
        {
             subscribedUser("Tech");
        }


        if(sharedPreferences.getBoolean("job_notify",false))
        {
             subscribedUser("Jobs");
        }

        if(sharedPreferences.getBoolean("fashion_notify",false))
        {
         subscribedUser("Fashion");
        }


        if(sharedPreferences.getBoolean("love_notify",false))
        {
             subscribedUser("Love");
        }

        
    }*/
}
