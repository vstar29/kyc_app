package com.vstarvibes.app.others;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by VICTOR on 15-Oct-16.
 */
public class PostClass implements Parcelable {

    public int id;
    public String title;
    public String date;
    @SerializedName("author_name")
    public String authorName;
    public String content;
    @SerializedName("featured_media")
    public String featuredImage;
    public String link;
    @SerializedName("categoryOne")
    public String category;
    public String song_url;


    public PostClass(int id, String title, String date, String authorName, String content, String featuredImage, String link, String category, String song_url )
    {
        this.id=id;
        this.title=title;
        this.date=date;
        this.authorName=authorName;
        this.content=content;
        this.featuredImage=featuredImage;
        this.link=link;
        this.category=category;
        this.song_url=song_url;

    }

    private PostClass(Parcel in )
    {
        id=in.readInt();
        title=in.readString();
        date=in.readString();
        authorName=in.readString();
        content=in.readString();
        featuredImage=in.readString();
        link=in.readString();
        category=in.readString();
        song_url=in.readString();

    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(id);
        out.writeString(title);
        out.writeString(date);
        out.writeString(authorName);
        out.writeString(content);
        out.writeString(featuredImage);
        out.writeString(link);
        out.writeString(category);
        out.writeString(song_url);

    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PostClass> CREATOR
            = new Creator<PostClass>() {
        // @Override
        public PostClass createFromParcel(Parcel in) {
            return new PostClass(in);
        }

        // @Override
        public PostClass[] newArray(int size) {
            return new PostClass[size];
        }
    };

    public String getTitle()
    {
        return title;
    }
    public String getDate() {

        Date today = new Date();
        String current = today.toString();
        Date postdate = null;

        Long mSeconds;
        long day, hours, minutes, seconds;
        Boolean sameYear=false;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat yearFormat=new SimpleDateFormat("yyyy");
        SimpleDateFormat defaultFormat=new SimpleDateFormat("MMM dd");
        SimpleDateFormat format=new SimpleDateFormat("MMM dd, yyyy");

        try {
            postdate = dateFormat.parse(date);
            today= dateFormat.parse(current);

        } catch (Exception e) {
        }

        mSeconds = today.getTime() - postdate.getTime();
        seconds= TimeUnit.MILLISECONDS.toSeconds(mSeconds);
        minutes=TimeUnit.MILLISECONDS.toMinutes(mSeconds);
        hours=TimeUnit.MILLISECONDS.toHours(mSeconds);
        day=TimeUnit.MILLISECONDS.toDays(mSeconds);
        String originalYear=yearFormat.format(postdate);
        String yearNow=yearFormat.format(today);
        if(originalYear.equals(yearNow))
        {
            sameYear=true;
        }

        if(day>7 &&sameYear )
        {
            return defaultFormat.format(postdate);
        }

        else if(day==1 &&sameYear )
        {
            return Long.toString(day) +  " day ago";
        }

        else if(day>1 &&day<=7 &&sameYear )
        {
            return Long.toString(day) +  " days ago";
        }
        else if(hours>1 &&hours <=23 && sameYear)
        {
            return Long.toString(hours) +  " hours ago";
        }
        else if(minutes>1 && minutes<60)
        {
            return Long.toString(minutes) +" minutes ago";
        }
        else if(hours==1)
        {
            return  Long.toString(hours) + " hour ago";
        }

        else if(minutes==1)
        {

            return Long.toString(minutes) +" minute ago";
        }

        else if(minutes<1 && sameYear)
        {

            return " just now";
        }

        else
        {
            return format.format(postdate);
        }


    }

    public int getId() {
        return id;
    }

    public String getFeaturedImage() {
        if(featuredImage=="") {
            if(category.equalsIgnoreCase("Jobs"))
            {
                return   "https://res.cloudinary.com/vstarvibes/image/upload/v1502566975/jobs.png";

            }
            return    "https://vstarvibes.com/wp-content/themes/vstavibe/assets/img/vstarvibe-logo.png";
        }

        return featuredImage;
    }
    public String getAuthorName()
    {
        return authorName;
    }

    public  String getContent()
    {
        return content.trim();
    }

    public String getLink()
    {
        return link;
    }

    public  String getCategory()
    {
        return category;
    }

    public String getSong_url()
    {
        return song_url;
    }
}
