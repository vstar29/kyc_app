package com.vstarvibes.app.others;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.andexert.library.RippleView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.ads.NativeExpressAdView;
import com.vstarvibes.app.R;
import com.vstarvibes.app.activities.MainActivity;
import com.vstarvibes.app.fragments.HomeFragment;
import com.vstarvibes.app.fragments.MovieFragment;

import java.util.List;

/**
 * Created by VICTOR on 15-Oct-16.
 */
public class HomePostListAdapter extends BaseAdapter {

    private Context context;
    private List<Object> postList;
    private LayoutInflater layoutinflater;
    private HomeFragment homefragment;
    private MovieFragment moviefragment;
    private MainActivity mainActivity;
    SharedPreferences appSharedPref;
    private  String type;
    private static final int  NEWS_VIEW_TYPE = 0;
    private static final int ADVERT_VIEW_TYPE = 1;
    private ItemClickListener clickListener;


    public HomePostListAdapter(Context context, List<Object> postList, HomeFragment homeFragment, MainActivity mainActivity, MovieFragment movieFragment, String type) {
        this.context = context;
        layoutinflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.postList = postList;
        this.homefragment=homeFragment;
        this.mainActivity=mainActivity;
        this.moviefragment=movieFragment;

        this.type=type;
        this.appSharedPref=context.getSharedPreferences("My_Preference", 0);
    }

    @Override
    public int getCount() {
        return postList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        if(position==0)
        {
            return NEWS_VIEW_TYPE;
        }
        else
        {
            return (position % 6 == 0) ? ADVERT_VIEW_TYPE
                    : NEWS_VIEW_TYPE;
        }

    }

    public void updateView(List<Object> fresh)
    {
        if(fresh !=null)
        {
            postList=fresh;
            this.notifyDataSetChanged();
        }

    }

    public  View getViewType(String type,ViewGroup parent)
    {

        return  layoutinflater.inflate(R.layout.home_model, parent, false);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        int viewType = getItemViewType(position);
        final int position_final = position;
        switch (viewType) {
            case NEWS_VIEW_TYPE:
                convertView= getViewType(type,parent);
                ViewHolder newsItemHolder = new ViewHolder();
                newsItemHolder.title = (TextView) convertView.findViewById(R.id.post_title);

                newsItemHolder.categoryOne = (TextView) convertView.findViewById(R.id.post_category);
                newsItemHolder.postFeaturedImage = (ImageView) convertView.findViewById(R.id.post_image);
                newsItemHolder.date = (TextView) convertView.findViewById(R.id.post_date);
                newsItemHolder.commentCount = (TextView) convertView.findViewById(R.id.post_comment_count);
                newsItemHolder.share = (ImageView) convertView.findViewById(R.id.sharePost);
                newsItemHolder.rippleView=(RippleView) convertView.findViewById(R.id.ripple);
                newsItemHolder.rippleView.setTag(position);
                convertView.setTag(newsItemHolder);
                PostListClass post = (PostListClass) postList.get(position);

                newsItemHolder.categoryOne.setText(post.categoryOne);

                if (appSharedPref.getBoolean("showImage", true)) {
                    Glide.with(context).load(post.getFeaturedImage()).placeholder(android.R.drawable.progress_indeterminate_horizontal).thumbnail(0.5f) .error(R.drawable.vstar_logo).crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(newsItemHolder.postFeaturedImage);

                }
                newsItemHolder.title.setText(post.getTitle());
                newsItemHolder.date.setText(post.getDate());
                newsItemHolder.commentCount.setText(post.getCommentCount());

                newsItemHolder.share.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (type == "home") {
                            homefragment.sharePost(position_final);
                        } else {
                            moviefragment.sharePost(position_final);
                        }
                    }
                });
                newsItemHolder.rippleView.setOnRippleCompleteListener(new  RippleView.OnRippleCompleteListener()
                {
                    @Override
                    public void onComplete(RippleView rippleView)
                    {
                        if (type == "home") {

                            homefragment.onClick(rippleView,position);
                        } else {
                            moviefragment.onClick(rippleView,position);
                        }
                    }
                });


                return convertView;
            case ADVERT_VIEW_TYPE:
                // fall through
            default:
                ViewGroup layout = (ViewGroup) layoutinflater.inflate(R.layout.big_advert, parent, false);
                NativeExpressAdView adView =
                        (NativeExpressAdView) postList.get(position);
                adView.setBackgroundColor(Color.parseColor("#ede8e8e8"));


                if (layout.getChildCount() > 0) {
                    layout.removeAllViews();
                }
                if (adView.getParent() != null) {
                    ((ViewGroup) adView.getParent()).removeView(adView);
                }

                layout.addView(adView);
                return layout;

        }

    }

    static class ViewHolder  {

        ImageView postFeaturedImage;
        TextView title;
        TextView categoryOne;
        TextView date;
        TextView commentCount;
        ImageView share;
        RippleView rippleView;


    }

}
