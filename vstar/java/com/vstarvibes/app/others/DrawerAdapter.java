package com.vstarvibes.app.others;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vstarvibes.app.R;
import com.vstarvibes.app.activities.MainActivity;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by VICTOR on 15-Oct-16.
 */
public class DrawerAdapter extends BaseAdapter {

    private Context context;
    private static ArrayList<DrawerItemClass> drawerItem=new ArrayList<DrawerItemClass>();
    private static ArrayList<PairItemClass> favouriteList=new ArrayList<PairItemClass>();
    private LayoutInflater layoutinflater;
    private String favouriteString;
    private String exclude="";
    SharedPreferences appSharedPref;
    FireBaseRegService  fireBaseRegService=new FireBaseRegService();
    SharedPreferences.Editor editor;
    Gson gson;
    Boolean listen=true;
    Type type=new TypeToken<List<PairItemClass>>(){}.getType();



    public DrawerAdapter(Context context) {
        this.context = context;
        layoutinflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.appSharedPref=context.getSharedPreferences("My_Preference", 0);
        favouriteString= appSharedPref.getString("favouriteList","") ;
        this.exclude=appSharedPref.getString("exclude",exclude);
        gson = new Gson();
        if(!favouriteString.isEmpty())
        {
            favouriteList=gson.fromJson(favouriteString,type);
        }
        else
        {
            favouriteList.add(0,new PairItemClass("All","true"));
            favouriteList.add(1,new PairItemClass("News","true"));
            favouriteList.add(2,new PairItemClass("Music","true"));
            favouriteList.add(3,new PairItemClass("Gist","true"));
            favouriteList.add(4,new PairItemClass("Videos","true"));
            favouriteList.add(5,new PairItemClass("Sports","true"));
            favouriteList.add(6,new PairItemClass("Love","true"));
            favouriteList.add(7,new PairItemClass("Movies","true"));
            favouriteList.add(8,new PairItemClass("Tech","true"));
            favouriteList.add(9,new PairItemClass("Careers","true"));
            favouriteList.add(10,new PairItemClass("Fashion","true"));
        }
        this.editor=appSharedPref.edit();

        if(drawerItem !=null)
        {

                drawerItem.add(0,new DrawerItemClass("Home",R.drawable.all));
                drawerItem.add(1,new DrawerItemClass("News",R.drawable.news));
                drawerItem.add(2,new DrawerItemClass("Music",R.drawable.music));
                drawerItem.add(3,new DrawerItemClass("Gist",R.drawable.gist));
                drawerItem.add(4,new DrawerItemClass("Videos",R.drawable.video));
                drawerItem.add(5,new DrawerItemClass("Sports",R.drawable.sports));
                drawerItem.add(6,new DrawerItemClass("Love & Sex",R.drawable.love));
                drawerItem.add(7,new DrawerItemClass("Movies",R.drawable.movie));
                drawerItem.add(8,new DrawerItemClass("Technology",R.drawable.tech));
                drawerItem.add(9,new DrawerItemClass("Jobs",R.drawable.jobs));
                drawerItem.add(10,new DrawerItemClass("Fashion",R.drawable.fashion));
                drawerItem.add(11,new DrawerItemClass("Invites",R.drawable.invites));

        }

    }

    @Override
    public int getCount() {
        return 12;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View drawerView, ViewGroup parent) {

        ViewHolder listViewHolder;

        if(drawerView == null){
            drawerView = layoutinflater.inflate(R.layout.drawer_model,parent,false);
            listViewHolder = new ViewHolder();
            listViewHolder.title = (TextView)drawerView.findViewById(R.id.title);
            listViewHolder.bgImage = (RelativeLayout) drawerView.findViewById(R.id.bg_image);
            listViewHolder.selector=(CheckBox) drawerView.findViewById(R.id.selector);
           listViewHolder.selector.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   boolean isChecked=((CheckBox) view).isChecked();
                    try{

                        switch (position) {

                            case 1:
                                if (isChecked) {
                                    favouriteList.set(1,new PairItemClass("News","true"));
                                    fireBaseRegService.subscribedUser("News");
                                    editor.putBoolean("news_notify",true);
                                   exclude= exclude.replaceAll(",?19","");

                                } else {
                                    favouriteList.set(1,new PairItemClass("News","false"));
                                    fireBaseRegService.unSubscribedUser("News");
                                    exclude=exclude+",19";
                                    editor.putBoolean("news_notify",false);
                                }
                                break;
                            case 2:
                                if (isChecked) {
                                    favouriteList.set(2,new PairItemClass("Music","true"));
                                    fireBaseRegService.subscribedUser("Music");
                                    editor.putBoolean("music_notify",true);
                                    exclude= exclude.replaceAll(",?21","");
                                } else {
                                    favouriteList.set(2,new PairItemClass("Music","false"));
                                    fireBaseRegService.unSubscribedUser("Music");
                                    editor.putBoolean("music_notify",false);
                                    exclude=exclude+",21";
                                }
                                break;

                            case 3:
                                if (isChecked) {
                                    favouriteList.set(3,new PairItemClass("Gist","true"));
                                    fireBaseRegService.subscribedUser("Gist");
                                    editor.putBoolean("gist_notify",true);
                                    exclude=exclude.replaceAll(",?20","");
                                } else {
                                    favouriteList.set(3,new PairItemClass("Gist","false"));
                                    fireBaseRegService.unSubscribedUser("Gist");
                                    editor.putBoolean("gist_notify",false);
                                    exclude= exclude+",20";
                                }
                                break;
                            case 4:
                                if (isChecked) {
                                    favouriteList.set(4,new PairItemClass("Videos","true"));
                                    fireBaseRegService.subscribedUser("Videos");
                                    editor.putBoolean("video_notify",true);
                                    exclude= exclude.replaceAll(",?30","");
                                } else {
                                    favouriteList.set(4,new PairItemClass("Videos","false"));
                                    fireBaseRegService.subscribedUser("Videos");
                                    editor.putBoolean("video_notify",false);
                                    exclude= exclude+",30";
                                }
                                break;
                            case 5:
                                if (isChecked) {
                                    favouriteList.set(5,new PairItemClass("Sports","true"));
                                    fireBaseRegService.subscribedUser("Sports");
                                    editor.putBoolean("sport_notify",true);
                                    exclude= exclude.replaceAll(",?23","");
                                } else {
                                    favouriteList.set(5,new PairItemClass("Sports","false"));
                                    fireBaseRegService.subscribedUser("Sports");
                                    editor.putBoolean("sport_notify",false);
                                    exclude= exclude+",23";
                                }
                                break;
                            case 6:
                                if (isChecked) {
                                    favouriteList.set(6,new PairItemClass("Love","true"));;
                                    fireBaseRegService.subscribedUser("Love");
                                    editor.putBoolean("love_notify",true);
                                    exclude.replaceAll(",?15","");
                                } else {
                                    favouriteList.set(6,new PairItemClass("Love","false"));;
                                    fireBaseRegService.subscribedUser("Love");
                                    editor.putBoolean("love_notify",false);
                                    exclude=exclude+",15";
                                }
                                break;
                            case 7:
                                if (isChecked) {
                                    favouriteList.set(7,new PairItemClass("Movies","true"));
                                    fireBaseRegService.subscribedUser("Movies");
                                    editor.putBoolean("movie_notify",true);
                                    exclude=  exclude.replaceAll(",?25","");
                                } else {
                                    favouriteList.set(7,new PairItemClass("Movies","false"));
                                    fireBaseRegService.subscribedUser("Movies");
                                    editor.putBoolean("movie_notify",false);
                                    exclude=exclude+",25";
                                }
                                break;
                            case 8:
                                if (isChecked) {
                                    favouriteList.set(8,new PairItemClass("Tech","true"));;
                                    fireBaseRegService.subscribedUser("Tech");
                                    editor.putBoolean("tech_notify",true);
                                    exclude=exclude.replaceAll(",?24","");
                                } else {
                                    favouriteList.set(8,new PairItemClass("Tech","false"));
                                    fireBaseRegService.subscribedUser("Tech");
                                    editor.putBoolean("tech_notify",false);
                                    exclude= exclude+",24";
                                }
                                break;

                            case 9:
                                if (isChecked) {
                                    favouriteList.set(9,new PairItemClass("Careers","true"));
                                    fireBaseRegService.subscribedUser("Careers");
                                    editor.putBoolean("job_notify",true);
                                    exclude= exclude.replaceAll(",?159","");
                                } else {
                                    favouriteList.set(9,new PairItemClass("Careers","false"));
                                    fireBaseRegService.subscribedUser("Careers");
                                    editor.putBoolean("job_notify",false);
                                    exclude= exclude+",159";
                                }
                                break;
                            case 10:
                                if (isChecked) {
                                    favouriteList.set(10,new PairItemClass("Fashion","true"));
                                    fireBaseRegService.subscribedUser("Fashion");
                                    editor.putBoolean("fashion_notify",true);
                                    exclude=exclude.replaceAll(",?161","");
                                } else {
                                    favouriteList.set(10,new PairItemClass("Fashion","false"));
                                    fireBaseRegService.subscribedUser("Fashion");
                                    editor.putBoolean("fashion_notify",false);
                                  exclude= exclude+",161";
                                }
                                break;

                        }

                        favouriteString=gson.toJson(favouriteList,type);
                        editor.putString("favouriteList",favouriteString);
                        if(exclude.length()>0 && exclude.charAt(0)==',')
                        {
                            exclude=exclude.substring(1);
                        }
                        editor.putString("exclude",exclude);
                        editor.commit();
                        Intent intent=new Intent(context, MainActivity.class);
                        context.startActivity(intent);
                   }catch (Exception e)
                    {

                    }
                }
            });
            drawerView.setTag(listViewHolder);
        }else{

            listViewHolder = (ViewHolder)drawerView.getTag();
        }
        if (position == 0|| position==11) {
            listViewHolder.selector.setVisibility(View.INVISIBLE);
        }

        String title=drawerItem.get(position).title;
        listViewHolder.title.setText(title);
        listViewHolder.bgImage.setBackground(ContextCompat.getDrawable(context, drawerItem.get(position).featuredImage));
        //listViewHolder.selector.setChecked(false);

        if (position != 0&& position!=11) {
            String status=favouriteList.get(position).value;

            if(status.equalsIgnoreCase("false"))
            {
                //((HeaderGridView)parent).setItemChecked(position,false);
                listViewHolder.selector.setChecked(false);
                //listen=false;
            }
            //listen=true;

        }
        return drawerView;
    }

    class ViewHolder{

        RelativeLayout bgImage;
        TextView title;
        CheckBox selector;
    }





}
