package com.vstarvibes.app.others;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.vstarvibes.app.R;

import java.util.ArrayList;

/**
 * Created by VICTOR on 15-Oct-16.
 */
public class SearchListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<PostListClass> relatedPosts;
    private LayoutInflater layoutinflater;
    SharedPreferences appSharedPref;



    public SearchListAdapter(Context context, ArrayList<PostListClass> relatedPosts) {
        this.context = context;
        layoutinflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.relatedPosts = relatedPosts;
        this.appSharedPref=context.getSharedPreferences("My_Preference", 0);
    }

    @Override
    public int getCount() {
        return relatedPosts.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View related;
        Holder listHolder;
        if(convertView == null){
            related = layoutinflater.inflate(R.layout.search_list_model,parent,false);
            listHolder = new Holder();
            listHolder.title = (TextView)related.findViewById(R.id.title);
            listHolder.postFeaturedImage = (ImageView)related.findViewById(R.id.image);


            related.setTag(listHolder);
        }else{
            related=convertView;
            listHolder = (Holder)related.getTag();
        }

        if(appSharedPref.getBoolean("showImage",true)) {
            Glide.with(context).load(relatedPosts.get(position).getFeaturedImage()).diskCacheStrategy(DiskCacheStrategy.ALL).into(listHolder.postFeaturedImage);
        }
        listHolder.title.setText(relatedPosts.get(position).getTitle());

        return related;
    }

    static class Holder{

        ImageView postFeaturedImage;
        TextView title;

    }




}
