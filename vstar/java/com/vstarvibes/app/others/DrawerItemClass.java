package com.vstarvibes.app.others;

/**
 * Created by VICTOR on 11-Aug-17.
 */

public class DrawerItemClass {

    String title;
    int featuredImage;

    public DrawerItemClass(String title,int featuredImage)
    {
        this.title=title;
        this.featuredImage=featuredImage;
    }

    public String getTitle()
    {
        return title;
    }

    public int getFeaturedImage()
    {
        return featuredImage;
    }
}
