package com.vstarvibes.app.fragments;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vstarvibes.app.R;
import com.vstarvibes.app.activities.MainActivity;
import com.vstarvibes.app.activities.SingleMusicActivity;
import com.vstarvibes.app.activities.SinglePostActivity;
import com.vstarvibes.app.others.MusicAdapter;
import com.vstarvibes.app.others.PostClass;
import com.vstarvibes.app.others.PostListClass;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MusicFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MusicFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MusicFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ArrayList <PostListClass> allMusic;
    private GridView gridView;
    Gson gson;
    private int page=1;
    private int postID;
    private String postTitle;
    private String postContent;
    List<Object> musicList;
    private String
            musicBase = "https://vstarvibes.com/wp-json/dec_29/posts?categories=21&fields=id,my_title,content,link,song_url,categoryOne,author_name,featured_media,date,comment_count&per_page=18&page=";
    private SwipeRefreshLayout swipeRefresh;
    MusicAdapter musicAdapter;
    private OnFragmentInteractionListener mListener;
    String latestPosts;
    Boolean connected;
    String postDate;
    String author;
    String postImage;
    String songUrl;
    SharedPreferences appSharedPref;
    MainActivity mainActivity;
    String postLink;
    ProgressBar loading;
    private  int reload;
    Boolean fetching=false;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MusicFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MusicFragment newInstance(String param1, String param2) {
        MusicFragment fragment = new MusicFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public MusicFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
           // mParam1 = getArguments().getString(ARG_PARAM1);
           // mParam2 = getArguments().getString(ARG_PARAM2);
            //allMusic=savedInstanceState.getParcelableArrayList("posts");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View musicView=inflater.inflate(R.layout.fragment_music, container, false);
        gridView=(GridView) musicView.findViewById(R.id.GridView);
        swipeRefresh=(SwipeRefreshLayout) musicView.findViewById(R.id.swipe_refreshLayout);

        appSharedPref= getContext().getSharedPreferences("My_Preference", 0);
        latestPosts=appSharedPref.getString("musicPosts", "");
        allMusic = new ArrayList<>();
        musicList=null;
        if(!latestPosts.isEmpty())
        {
            gson = new Gson();
            Type type=new TypeToken<List<PostListClass>>(){}.getType();
            allMusic=gson.fromJson(latestPosts,type);
        }
        if(appSharedPref.getString("imageQuality","")=="Medium")
        {
            musicBase = "https://vstarvibes.com/wp-json/dec_29/posts?categories=21&fields=id,my_title,song_url,link,content,categoryOne,author_name,featured_media,date,comment_count&per_page=18&img_quality=blog-first-image&page=";
        }
        musicAdapter=new MusicAdapter(getContext(),allMusic);
        gridView.setAdapter(musicAdapter);
        Bundle bundle=getArguments();
        reload=bundle.getInt("reload",-1);
        if(reload==-1)
        {
            swipeRefresh.post(new Runnable() {
                @Override
                public void run() {
                    swipeRefresh.setRefreshing(true);
                    fetchPosts();
                }
            });
        }

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Activity activity=getActivity();
                if(activity instanceof MainActivity)
                {
                    mainActivity=(MainActivity) activity;
                    connected= mainActivity.checkConnection();

                }
            ArrayList<PostListClass>relatedPosts=new ArrayList<>();
            try {
                PostListClass post = allMusic.get(position);
                postID = post.id;
                postTitle = post.title;
                postTitle = Html.fromHtml(postTitle).toString();
                postDate = post.date;
                postDate = mainActivity.formatDate(postDate);
                author = post.authorName;
                postImage = post.featuredImage;
                postLink = post.link;
                postContent=post.content;
                songUrl=post.song_url;
                ArrayList<PostListClass> Posts = allMusic;
                Posts.remove(position);
                for (int i = 0; i < 7; i++) {
                    PostListClass eachPost = Posts.get(i);
                    relatedPosts.add(0, eachPost);
                }
           }catch (Exception e)
            {

            }
                PostClass newPost=new PostClass(postID,postTitle,postDate,author,postContent,postImage,postLink,"music",songUrl);
                Intent intent;
                if( !TextUtils.isEmpty( songUrl))
                {
                    intent = new Intent(getActivity(), SingleMusicActivity.class);
                }
                else
                {
                    intent = new Intent(getActivity(), SinglePostActivity.class);
                }

                intent.putExtra("post",newPost);
                intent.putExtra("returnState",2);
                intent.putParcelableArrayListExtra("relatedPosts",relatedPosts);


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                    /*Pair<View, String> pair1 = Pair.create(featuredImage, featuredImage.getTransitionName());
                    ActivityOptions options = ActivityOptions.
                            makeSceneTransitionAnimation(getActivity(), pair1);
                   startActivity(intent, options.toBundle());*/
                    startActivity(intent,
                            ActivityOptions
                                    .makeSceneTransitionAnimation(getActivity()).toBundle());


                } else {
                    startActivity(intent);
                }


            }
        });


        loading =(ProgressBar) musicView.findViewById(R.id.loading);

        gridView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view,
                                             int scrollState) { // TODO Auto-generated method stub
                int threshold = 1;
                int count = gridView.getCount();

                if (scrollState == SCROLL_STATE_IDLE) {
                    if (gridView.getLastVisiblePosition() >= count
                            - threshold && !fetching) {
                        // Execute LoadMoreDataTask AsyncTask
                        page+=1;
                        fetching=true;
                        new PostsFetcher().execute();
                        swipeRefresh.setRefreshing(false);
                        loading.setVisibility(View.VISIBLE);

                    }


                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                // TODO Auto-generated method stub

            }

        });
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                fetchPosts();
            }
        });
        return musicView;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }



    public void getPost(String data)
    {
        fetching=false;
        if(!TextUtils.isEmpty(data) && data !=latestPosts) {
            Gson Converter = new Gson();
            Type type = new TypeToken<List<PostListClass>>() {
            }.getType();

            try {

                if(page==1)
                {
                    allMusic = Converter.fromJson(data, type);
                    musicAdapter.updateGridData(allMusic);
                    appSharedPref = getContext().getSharedPreferences("My_Preference", 0);
                    SharedPreferences.Editor editor = appSharedPref.edit();
                    editor.putString("musicPosts", data);
                    editor.commit();
                }
                else
                {
                    ArrayList<PostListClass> holder=Converter.fromJson(data, type);
                    allMusic.addAll(holder);
                     musicAdapter.updateGridData(allMusic);
                    gridView.smoothScrollToPosition(gridView.getCount()-gridView.getCount()/page);

                }

            } catch (Exception e) {

            }

            swipeRefresh.setRefreshing(false);
            loading.setVisibility(View.INVISIBLE);
        }
        else {
            swipeRefresh.setRefreshing(false);
            loading.setVisibility(View.INVISIBLE);
        }

    }

    public void fetchPosts()
    {
        swipeRefresh.setRefreshing(true);
        new PostsFetcher().execute();

    }

    @Override
    public void onSaveInstanceState(Bundle outBundle)
    {
        super.onSaveInstanceState(outBundle);
        outBundle.putParcelableArrayList("posts", allMusic);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity;
        if(context instanceof  Activity)
        {
            activity=(Activity) context;
        }
        else
        {
            throw new  RuntimeException(context.toString()+"must implement OnFragmentInteractionListener");
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the com.vstarvibe.vstarvibeapp.activity and potentially other fragments contained in that
     * com.vstarvibe.vstarvibeapp.activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    private class PostsFetcher extends AsyncTask<Void, Void, String> {



        @Override
        protected void onPreExecute() {


        }

        @Override
        protected String doInBackground(Void... params) {


            HttpHandler httpHandler = new HttpHandler();

            // Making a request to url and getting response
            String response = httpHandler.makeServiceCall(musicBase+page);
            return response;

        }

        @Override
        protected void onPostExecute(String result) {

            getPost(result);

        }
    }

    private class HttpHandler {

        String response;


        public String makeServiceCall(String Url) {


            try {
                URL url = new URL(Url);
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                // read the response
                InputStream in = new BufferedInputStream(conn.getInputStream());

                response = convertStreamToString(in);
            } catch (MalformedURLException e) {

            } catch (ProtocolException e) {

            } catch (IOException e) {

            } catch (Exception e) {

            }
            return response;
        }

        private String convertStreamToString(InputStream is) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();

            String line;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line).append('\n');
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return sb.toString();
        }
    }

}
