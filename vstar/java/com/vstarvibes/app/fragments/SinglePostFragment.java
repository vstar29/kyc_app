package com.vstarvibes.app.fragments;

import android.app.Activity;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.NativeExpressAdView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vstarvibes.app.R;
import com.vstarvibes.app.activities.ActivityBrowser;
import com.vstarvibes.app.activities.SingleMusicActivity;
import com.vstarvibes.app.activities.SinglePostActivity;
import com.vstarvibes.app.others.CommentAdapter;
import com.vstarvibes.app.others.CommentClass;
import com.vstarvibes.app.others.DataFetcher;
import com.vstarvibes.app.others.HelperClass;
import com.vstarvibes.app.others.ItemClickListener;
import com.vstarvibes.app.others.PostClass;
import com.vstarvibes.app.others.PostListClass;
import com.vstarvibes.app.others.RelatedPostAdapter;
import com.vstarvibes.app.others.UserClass;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SinglePostFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SinglePostFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SinglePostFragment extends Fragment implements ItemClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private ArrayList<PostClass> post;
    TextView title;
    TextView date;
    TextView author;
    ImageView featured_image;
    WebView content;
    ListView postComments;
    ProgressBar progressBar;
   ImageView postNewComment;
    EditText newCommentContent;
    ProgressBar commentLoader;
    SinglePostActivity singlePostActivity;
    Gson gson;
    private String postBase;
    String style;
    String start;
    Boolean favourite=false;
    SharedPreferences appSharedPref;
    RecyclerView relatedList;
    RelatedPostAdapter relatedPostAdapter;
    RelativeLayout relativeLayout;
    TextView error;
    ArrayList<PostListClass> relatedPosts;
    private String commentBase;
    ArrayList<CommentClass> CommentsList;
    CommentAdapter commentAdapter;
    int commentPage=1;
    View commentsView;
    PopupWindow popComments;
    int height;
    int width;
    int postId;
    ProgressDialog progressDialog;
    String postContent=null;
    ImageView closeDialogue;
    ImageView noCommentImage;
    TextView noCommentText;
    PostClass returnPost;
    PostClass forwardReturnPost;
    String postTitle;
    String authorName;
    String postDate;
    String postImage;
    String postLink;
    int page=1;
    String category;
    String songUrl;
    NativeExpressAdView adView;
    ProgressBar adLoading;
    ProgressBar adLoading2;
    LinearLayout singleContent;
    TextView  related_title;


    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SinglePostFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SinglePostFragment newInstance(String param1, String param2) {
        SinglePostFragment fragment = new SinglePostFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public SinglePostFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View singleView= inflater.inflate(R.layout.fragment_single_post, container, false);
        View topView= inflater.inflate(R.layout.single_content, null);
        singleContent=(LinearLayout)topView.findViewById(R.id.singleContent);
        commentsView=inflater.inflate(R.layout.commentcordinator,container,false);
        AssetManager assetManager=getContext().getAssets();
        progressBar=(ProgressBar) singleView.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
        Typeface typefaceSanserif=Typeface.createFromAsset(assetManager, "fonts/sanSerifNarrowBold.ttf");
        title=(TextView) singleView.findViewById(R.id.post_title);
        related_title=(TextView) singleView.findViewById(R.id.related_title);
        title.setTypeface(typefaceSanserif);
        author=(TextView) getActivity().findViewById(R.id.author);
        date=(TextView) getActivity().findViewById(R.id.date);
        adLoading=(ProgressBar) singleView.findViewById(R.id.add_loading) ;
        adLoading.setVisibility(View.VISIBLE);

        AdView mAdView = (AdView) singleView.findViewById(R.id.adbottom);

        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);
        AdView adView = (AdView) singleView.findViewById(R.id.adBig);
        AdRequest adRequest1 = new AdRequest.Builder()
                .build();
        adView.loadAd(adRequest1);
        mAdView.setAdListener(new AdListener()
        {
            @Override
            public void onAdLoaded()
            {
                adLoading.setVisibility(View.INVISIBLE);
            }
            @Override
            public void onAdFailedToLoad(int errorCode)
            {
                adLoading.setVisibility(View.INVISIBLE);
            }
        });

        closeDialogue=(ImageView) commentsView.findViewById(R.id.close);
        mAdView.setAdListener(new AdListener()
        {
            @Override
            public void onAdLoaded()
            {
                adLoading.setVisibility(View.INVISIBLE);
            }
            @Override
            public void onAdFailedToLoad(int errorCode)
            {
                adLoading.setVisibility(View.INVISIBLE);
            }
        });
        closeDialogue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newCommentContent.setText("");
                popComments.dismiss();

            }
        });
        featured_image=(ImageView) getActivity().findViewById(R.id.post_image);
        postComments=(ListView) commentsView.findViewById(R.id.Comment_list);
        commentLoader=(ProgressBar) commentsView.findViewById(R.id.commentProgressBar);
        postNewComment=(ImageView) commentsView.findViewById(R.id.send_comment);
        newCommentContent=(EditText) commentsView.findViewById(R.id.comment_content);
        content=(WebView) singleView.findViewById(R.id.content);
        commentLoader.setVisibility(View.INVISIBLE);
        noCommentImage= (ImageView) commentsView.findViewById(R.id.noCommentIcon);
        noCommentText=(TextView) commentsView.findViewById(R.id.noCommentText);
        noCommentText.setVisibility(View.INVISIBLE);
        noCommentImage.setVisibility(View.INVISIBLE);



        relativeLayout=(RelativeLayout)singleView.findViewById(R.id.single_fragment);
        WebSettings webSettings=content.getSettings();
        webSettings.setJavaScriptEnabled(true);
        content.setWebViewClient(new BrowserClient());
        relatedList=(RecyclerView)singleView.findViewById(R.id.Related_list);
        relatedList.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);
        relatedList.setLayoutManager(layoutManager);
        postComments.setHeaderDividersEnabled(true);
        relatedList.setFocusable(false);
        Bundle args=getArguments();
        relatedPosts =new ArrayList<>();
        relatedPosts=args.getParcelableArrayList("relatedPosts");
        returnPost=args.getParcelable("returnPost");
        relatedPostAdapter=new RelatedPostAdapter(getContext(),relatedPosts);
        relatedPostAdapter.setClickListener(this);
        DisplayMetrics displayMetrics=new DisplayMetrics();
        WindowManager windowManager=(WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        width=displayMetrics.widthPixels;
        height=displayMetrics.heightPixels;
        start="<html><head><meta name=\"viewport\"\"content=\"width="+width+", initial-scale=1 \" /></head>";
        style="<style> img,figure,p img,iframe{ margin:auto  !important; display:block; height: auto !important;max-width:100%  !important;}.inserted_ad{display:none !important;}.sharedaddy, .sd-sharing-enabled, .zem_rp_wrap, .zem_rp_th_modern,.socialmedia,.tptn_counter { display: none !important} </style>";
        progressBar.setVisibility(View.INVISIBLE);
        PostClass p;

        Activity activity=getActivity();
        if(activity instanceof SinglePostActivity )
        {  singlePostActivity=(SinglePostActivity) activity;}

        if(returnPost !=null)
        {
            progressBar.setVisibility(View.GONE);
            p = returnPost;
            postTitle = p.title;
            authorName = p.authorName;
            postDate = p.date;
            postImage = p.featuredImage;
            postId = p.id;
            postContent=p.content;
            postLink=p.link;
            category=p.category;
            songUrl=p.song_url;
            content.loadDataWithBaseURL(null, start + style + postContent + "</html>", "text/html", "UTF-8", null);
            forwardReturnPost=new PostClass(postId,postTitle,postDate,authorName,postContent,postImage,postLink,category,songUrl);

        }

        else

        {
            p= args.getParcelable("posts");
            postTitle = p.title;
            authorName = p.authorName;
            postDate = p.date;
            postImage = p.featuredImage;
            postId = p.id;
            postLink=p.link;
            postContent=p.content.trim();
            postBase = "https://vstarvibes.com/wp-json/dec_29/posts/"+postId+"?fields=content";
            category=p.category;
            songUrl=p.song_url;
            if(!TextUtils.isEmpty(postContent))
            {

                content.loadDataWithBaseURL(null, start + style + postContent + "</html>", "text/html", "UTF-8", null);
                forwardReturnPost=new PostClass(postId,postTitle,postDate,authorName,postContent,postImage,postLink,category,songUrl);
            }
            else {
                fetchPosts();
            }


        }

        relatedList.setAdapter(relatedPostAdapter);
        commentBase = "https://vstarvibes.com/wp-json/dec_29/comments/?post="+postId+"&fields=post,author_name,date,author_avatar,content&per_page=12&page=";

        title.setText(postTitle);
        date.setText(postDate);
        author.setText(authorName);
        Glide.with(this).load(postImage).diskCacheStrategy(DiskCacheStrategy.ALL).crossFade().into(featured_image);

        CommentsList=new ArrayList<>();
        commentAdapter=new CommentAdapter(getContext(),CommentsList);
        postComments.setAdapter(commentAdapter);


        postNewComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postComment();
            }
        });
        Animation fadeIn=new AlphaAnimation(0,1);
        fadeIn.setInterpolator(new DecelerateInterpolator());
        fadeIn.setDuration(900);
        singleContent.startAnimation(fadeIn);

        return singleView;
    }

    public void showInterstitialAdd()
    {
        InterstitialAd mInterstitialAd = new InterstitialAd(getContext());
        mInterstitialAd.setAdUnitId(getString(R.string.add_full));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
    }

    private void fetchPosts() {
        new DataFetcher(getActivity(), postBase,SinglePostFragment.this, new SingleMusicActivity(),"post","","","").execute();
        progressBar.setVisibility(View.VISIBLE);
    }

    public void getPost(String response)
    {
        if (!TextUtils.isEmpty(response)) {

            gson = new Gson();
            try {


            PostClass post = gson.fromJson(response, PostClass.class);
            postContent = post.content.trim();
        }catch (Exception e)
            {
                progressBar.setVisibility(View.INVISIBLE);
                Snackbar snacky=Snackbar.make(relativeLayout,"No connection",Snackbar.LENGTH_INDEFINITE).setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        fetchPosts();

                    }
                });

                snacky.setActionTextColor(Color.GREEN);

                TextView tv = (TextView) snacky.getView().findViewById(android.support.design.R.id.snackbar_text);
                tv.setTextColor(Color.WHITE);
                snacky.show();

            }
            forwardReturnPost=new PostClass(postId,postTitle,postDate,authorName,postContent,postImage,postLink,category,songUrl);
            if(postContent!=null)
            {
                content.loadDataWithBaseURL(null, start + style + postContent + "</html>", "text/html", "UTF-8", null);
            }

            progressBar.setVisibility(View.GONE);
        }


        else
        {
            progressBar.setVisibility(View.INVISIBLE);
            Snackbar snacky=Snackbar.make(relativeLayout,"No connection",Snackbar.LENGTH_INDEFINITE).setAction("RETRY", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    fetchPosts();

                }
            });

            snacky.setActionTextColor(Color.GREEN);

            TextView tv = (TextView) snacky.getView().findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.WHITE);
            snacky.show();
        }


    }

    @Override
    public void onClick(View view, int position) {
        //position -= relatedList.getHeaderViewsCount();
        SinglePostActivity activity=new SinglePostActivity();
        PostListClass postlistclass = relatedPosts.get(position);
        PostClass newPost=new PostClass(postlistclass.id, Html.fromHtml(postlistclass.title).toString(),activity.formatDate(postlistclass.date),postlistclass.authorName,postlistclass.content,postlistclass.featuredImage,postlistclass.link,postlistclass.categoryOne,postlistclass.song_url);
        ArrayList<PostListClass> Posts = new ArrayList<>();
        Posts.addAll(relatedPosts);
        Posts.remove(position);
        Intent intent;
        if(newPost.category.equalsIgnoreCase("MUSIC") && !TextUtils.isEmpty(newPost.song_url ))
        {
            intent = new Intent(getContext(), SingleMusicActivity.class);
        }
        else
        {
            intent = new Intent(getContext(), SinglePostActivity.class);
        }


        intent.putExtra("post",newPost);
        intent.putExtra("returnState",0);
        intent.putParcelableArrayListExtra("relatedPosts",Posts);
        startActivity(intent);
    }
    public void showComments()
    {
        commentLoader.setVisibility(View.VISIBLE);
        popComments=new PopupWindow(commentsView, width,height,true);
        popComments.setAnimationStyle(android.R.style.Animation_Dialog);
        popComments.setBackgroundDrawable(getResources().getDrawable(R.drawable.commentbackground));
        popComments.setFocusable(true);
        popComments.setOutsideTouchable(true);
        popComments.showAtLocation(commentsView, Gravity.CENTER,0,0);
        fetchComments();
    }


      /*----======GET POST COMMENTS FROM  WEB API====--*/

    private void fetchComments() {


        new DataFetcher(getActivity(), commentBase+page, SinglePostFragment.this, new SingleMusicActivity(),"comment","","","").execute();
    }
    public void getComments(String comments, String method)
     {


         if(!TextUtils.isEmpty(comments) && comments !="[]" &&comments !=null )
         {

                try
                {
                    gson = new Gson();
                    Type type=new TypeToken<List<CommentClass>>(){}.getType();
                    CommentsList=gson.fromJson(comments, type);
                    if(CommentsList.size()==0)
                    {

                        commentLoader.setVisibility(View.GONE);
                        noCommentText.setVisibility(View.VISIBLE);
                        noCommentImage.setVisibility(View.VISIBLE);
                        progressDialog.dismiss();
                    }
                    else
                    {
                        commentLoader.setVisibility(View.GONE);
                        if(method=="get")
                        {
                            commentAdapter.updateView(CommentsList);
                        }
                        else
                        {

                            commentAdapter.addToComment(CommentsList);
                            progressDialog.dismiss();
                            noCommentText.setVisibility(View.INVISIBLE);
                            noCommentImage.setVisibility(View.INVISIBLE);
                            newCommentContent.setText("");
                        }

                    }
                }catch (Exception e)
                {
                    commentLoader.setVisibility(View.GONE);
                    noCommentText.setVisibility(View.VISIBLE);
                    noCommentImage.setVisibility(View.VISIBLE);
                }



            }
         else
         {

                 commentLoader.setVisibility(View.GONE);
         }
        }
    public void postComment()
    {
        progressDialog=new ProgressDialog(getContext());
        progressDialog.setMessage("Posting...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        HelperClass helperClass=new HelperClass();
        UserClass user=helperClass.getUser(getContext());
          if(user !=null)
          {
              String comments= newCommentContent.getText().toString();
              int post_Id=postId;
              Date date = new Date();
              String date_string="2017-05-26";
              SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


              try {
                  date_string= dateFormat.format(date);

              } catch (Exception e) {
              }

              CommentClass commentClass=new CommentClass(post_Id,user.name,user.profilePics,date_string,comments);
              ArrayList <CommentClass> comment=new ArrayList<>();
              comment.add(commentClass);
              gson=new Gson();

              Type type=new TypeToken<List<CommentClass>>(){}.getType();
              String newComments=gson.toJson(comment,type);

              getComments(newComments,"post");
              //String posCommentBase = "https://vstarvibes.com/wp-json/wp/v2/comments/?&author_name="+username+"&,content="+comments+"&post="+post_Id+"";
              new DataFetcher(getActivity(), "", SinglePostFragment.this, new SingleMusicActivity(),"postComment",user.name,comments,Integer.toString(post_Id)).execute();
          }
          else
          {
              Toast.makeText(getContext(),"Login to comments",Toast.LENGTH_LONG).show();
              progressDialog.dismiss();
          }
      }


    private void sharePost()
    {

        try {
            String link = Uri.parse(postLink).toString();
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("text/plain");
            share.putExtra(Intent.EXTRA_TEXT, postTitle + " " + ":" + link);
            startActivity(Intent.createChooser(share, "Share via"));
        }catch (Exception e)
        {

        }

    }

    private void addFavourite()
    {
        Gson gson=new Gson();
        ArrayList<PostListClass> favourites=new ArrayList<>();
        Type type=new TypeToken<List<PostListClass>>(){}.getType();
        appSharedPref= getContext().getSharedPreferences("My_Preference", 0);
        String jsonFavourite=appSharedPref.getString("favourites", "");
        favourites=gson.fromJson(jsonFavourite, type);
        PostListClass putFavourite=new PostListClass(forwardReturnPost.id, forwardReturnPost.date,forwardReturnPost.title,"","",forwardReturnPost.featuredImage,forwardReturnPost.authorName,forwardReturnPost.content,forwardReturnPost.link,forwardReturnPost.song_url);
        favourites.add(forwardReturnPost.id,putFavourite);

        SharedPreferences.Editor editor=appSharedPref.edit();
        jsonFavourite =gson.toJson(favourites,type);
        editor.putString("favourites", jsonFavourite);
        editor.commit();

    }

    private void removeFavourite()
    {
        Gson gson=new Gson();
        ArrayList<PostListClass> favourites=new ArrayList<>();
        Type type=new TypeToken<List<PostListClass>>(){}.getType();
        appSharedPref= getContext().getSharedPreferences("My_Preference", 0);
        String jsonFavourite=appSharedPref.getString("favourites", "");
        favourites=gson.fromJson(jsonFavourite, type);
        PostListClass putFavourite=new PostListClass(forwardReturnPost.id, forwardReturnPost.date,forwardReturnPost.title,"","",forwardReturnPost.featuredImage,forwardReturnPost.authorName,forwardReturnPost.content,forwardReturnPost.link,songUrl);
        favourites.remove(forwardReturnPost.id);

        SharedPreferences.Editor editor=appSharedPref.edit();
        jsonFavourite =gson.toJson(favourites,type);
        editor.putString("favourites", jsonFavourite);
        editor.commit();

    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id==android.R.id.home) {
            return super.onOptionsItemSelected(item);
        }
        /*if (id == R.id.action_favourite) {

            if(favourite==false)
            {
                item.setIcon(R.drawable.ic_favourite);
                addFavourite();
                favourite=!favourite;
            }
            else
            {
                removeFavourite();
                item.setIcon(R.drawable.ic_action_favourite);
                favourite=!favourite;
            }

            return true;
        }*/

        if (id == R.id.action_comments) {
            showComments();
            return true;
        }

        if (id == R.id.action_share) {
           sharePost();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity;
        if(context instanceof  Activity)
        {
            activity=(Activity) context;
            showInterstitialAdd();
        }
        else
        {
            throw new  RuntimeException(context.toString()+"must implement OnFragmentInteractionListener");
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the com.vstarvibe.vstarvibeapp.activity and potentially other fragments contained in that
     * com.vstarvibe.vstarvibeapp.activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }


    private class BrowserClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

      @Override
        public boolean shouldOverrideUrlLoading(WebView webView, String url)
        {

            if(url.endsWith(".mp3") || url.endsWith(".mp4") || url.endsWith(".3gp")|| url.endsWith(".pdf") || url.endsWith(".apk") || url.endsWith(".avi")|| url.endsWith(".flv")|| url.endsWith(".zip")||url.endsWith(".docx") ||url.endsWith(".MP4"))
            {
                if(singlePostActivity.isExternalStorageWritable()) {

                   String[] splits = url.split(Pattern.quote("."));
                    Uri source = Uri.parse(url);
                    DownloadManager.Request request = new DownloadManager.Request(source);
                    request.setDescription("Donwloading..");
                    String extension = splits[splits.length - 1];



                    try {
                        if (singlePostActivity.isExternalStorageWritable()) {

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                                String title = postTitle + "." + extension;
                                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, title);
                                request.allowScanningByMediaScanner();
                                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                                DownloadManager manager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
                                manager.enqueue(request);

                            }


                        }


                    } catch (Exception e) {

                    }
                }
            }
            else
            {

                Intent open=new Intent(getActivity(), ActivityBrowser.class);
                open.putExtra("url", url);
                open.putExtra("forwardReturnPost", forwardReturnPost);
                open.putParcelableArrayListExtra("relatedPosts", relatedPosts);
                content.clearCache(true);
                startActivity(open);

            }
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.INVISIBLE);
            related_title.setVisibility(View.VISIBLE);

        }




    }


}
