package com.vstarvibes.app.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.NativeExpressAdView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vstarvibes.app.R;
import com.vstarvibes.app.activities.MainActivity;
import com.vstarvibes.app.activities.SinglePostActivity;
import com.vstarvibes.app.others.ConnectivityReceiver;
import com.vstarvibes.app.others.PostClass;
import com.vstarvibes.app.others.PostListAdapter;
import com.vstarvibes.app.others.PostListClass;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link JobFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link JobFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class JobFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    Gson gson;
    MainActivity mainActivity;
    private int page=1;
    private int postID;
    private String postTitle;
    private ListView listView;
    int reload=-1;
    private String postBase = "https://vstarvibes.com/wp-json/dec_29/posts?categories=159&fields=id,my_title,song_url,low_img,link,content,categoryOne,author_name,featured_media,date,comment_count&per_page=18&page=";
    private ArrayList <PostListClass> postListClass;
    private List<Object> allPosts;
    private SwipeRefreshLayout swipeRefresh;
    private PostListAdapter postListAdapter;
    String latestPosts;
    Boolean connected;
    private String postDate;
    private String author;
    private String postImage;
    private String postContent;
    private String category;
    private String songUrl;
    SharedPreferences appSharedPref;
    String postLink;
    ProgressBar loading;
    public static final int ITEMS_PER_AD = 6;
    private static final int ADD_HEIGHT = 150;
    private static final int ADD_WIDTH = 330;
    Boolean fetching=false;


    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment JobFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static JobFragment newInstance(String param1, String param2) {
        JobFragment fragment = new JobFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public JobFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            // mParam1 = getArguments().getString(ARG_PARAM1);
            // mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View JobView = inflater.inflate(R.layout.fragment_job, container, false);
        listView = (ListView) JobView.findViewById(R.id.ListView);
        swipeRefresh = (SwipeRefreshLayout) JobView.findViewById(R.id.swipe_refresh_layout);

        appSharedPref= getContext().getSharedPreferences("My_Preference", 0);
        latestPosts=appSharedPref.getString("JobPosts", "");
        allPosts = new ArrayList<>();
        if(!latestPosts.isEmpty()) {
            gson = new Gson();
            try
            {
                Type type = new TypeToken<List<PostListClass>>() {}.getType();
                allPosts = gson.fromJson(latestPosts, type);
                postListClass=gson.fromJson(latestPosts,type);
            }catch (Exception r)
            {

            }

        }
        if(appSharedPref.getString("imageQuality","")=="Medium")
        {
            postBase = "https://vstarvibes.com/wp-json/dec_29/posts?categories=159&fields=id,my_title,song_url,link,content,categoryOne,author_name,featured_media,date,comment_count&per_page=18&img_quality=blog-first-image&page=";
        }
        addNativeExpressAds();
        setUpAndLoadNativeExpressAds();
        postListAdapter=new PostListAdapter(getContext(), allPosts,"Job",JobFragment.this);
        listView.setAdapter(postListAdapter);
        boolean isConnected = ConnectivityReceiver.isConnected();
        Bundle bundle=getArguments();
        reload=bundle.getInt("reload",-1);

        if(reload==-1)
        {
            swipeRefresh.post(new Runnable() {
                @Override
                public void run() {
                    swipeRefresh.setRefreshing(true);
                    fetchPosts();
                }
            });
        }
        Activity activity=getActivity();
        if(activity instanceof MainActivity)
        {
            mainActivity=(MainActivity) activity;
            connected= mainActivity.checkConnection();

        }

        loading=(ProgressBar) JobView.findViewById(R.id.loading);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                ArrayList<PostListClass>relatedPosts=new ArrayList<>();
                try {

                    PostListClass post = (PostListClass) allPosts.get(position);
                    postID = post.id;
                    postTitle = post.title;
                    postTitle = Html.fromHtml(postTitle).toString();
                    postDate = post.date;
                    postDate = mainActivity.formatDate(postDate);
                    author = post.authorName;
                    postImage = "https://res.cloudinary.com/vstarvibes/image/upload/v1502566975/jobs.png";
                    postLink = post.link;
                    postContent=post.content;
                    category=post.categoryOne;
                    songUrl=post.song_url;
                    ArrayList<PostListClass> Posts = postListClass;
                    Posts.remove(position);
                    for (int i = 0; i < 7; i++) {
                        PostListClass eachPost = Posts.get(i);
                        relatedPosts.add(i, eachPost);
                    }
                }catch (Exception e)
                {

                }
                PostClass newPost=new PostClass(postID,postTitle,postDate,author,postContent,postImage,postLink,category,songUrl);
                Intent intent = new Intent(getActivity(), SinglePostActivity.class);
                intent.putExtra("post",newPost);
                intent.putExtra("returnState",9);
                intent.putParcelableArrayListExtra("relatedPosts",relatedPosts);
                startActivity(intent);
            }
        });

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view,
                                             int scrollState) { // TODO Auto-generated method stub
                int threshold = 1;
                int count = listView.getCount();

                if (scrollState == SCROLL_STATE_IDLE) {
                    if (listView.getLastVisiblePosition() >= count
                            - threshold && !fetching) {
                        // Execute LoadMoreDataTask AsyncTask
                        page=page+1;
                        fetching=true;
                        new PostsFetcher().execute();
                        swipeRefresh.setRefreshing(false);
                        loading.setVisibility(View.VISIBLE);

                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                // TODO Auto-generated method stub

            }

        });
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                fetchPosts();
            }
        });
        return JobView;
    }

    /*----======GET POST FROM  WEB API====--*/

    public void getPost(String data)
    {

        fetching=false;
        if(!TextUtils.isEmpty(data) && data !=latestPosts) {
            Gson Converter = new Gson();
            Type type = new TypeToken<List<PostListClass>>() {
            }.getType();

            try {

                if(page==1)
                {
                    allPosts.clear();
                    postListClass= Converter.fromJson(data, type);
                    allPosts.addAll(postListClass);
                    addNativeExpressAds();
                    setUpAndLoadNativeExpressAds();
                    postListAdapter.updatePostView(allPosts);
                    appSharedPref = getContext().getSharedPreferences("My_Preference", 0);
                    SharedPreferences.Editor editor = appSharedPref.edit();
                    editor.putString("JobPosts", data);
                    editor.commit();
                }
                else
                {
                    ArrayList<PostListClass> holder=Converter.fromJson(data, type);
                    postListClass.addAll(holder);
                    allPosts.clear();
                    allPosts.addAll(postListClass);
                    addNativeExpressAds();
                    setUpAndLoadNativeExpressAds();
                    postListAdapter.updatePostView(allPosts);
                    listView.smoothScrollToPosition(listView.getCount()-listView.getCount()/page);

                }

            } catch (Exception e) {

            }

            swipeRefresh.setRefreshing(false);
            loading.setVisibility(View.INVISIBLE);
        }
        else {
            swipeRefresh.setRefreshing(false);

        }

    }

    public void fetchPosts()
    {

        //page=1;
        if (connected)
        {
            swipeRefresh.setRefreshing(true);
            new PostsFetcher().execute();
        }
        new PostsFetcher().execute();

    }
       /*---Advert insertion in listView--*/

    /**
     * Adds Native Express ads to the items list.
     */
    private void addNativeExpressAds() {
        try
        {

            for (int i = 0; i <= allPosts.size(); i += ITEMS_PER_AD) {
                if(i!=0) {

                    final NativeExpressAdView adView = new NativeExpressAdView(getContext());
                    allPosts.add(i, adView);
                }
            }
        }catch (Exception e)
        {

        }

        // Loop through the items array and place a new Native Express ad in every ith position in
        // the items List.

    }

    /**
     * Sets up and loads the Native Express ads.
     */
    private void setUpAndLoadNativeExpressAds() {
        // Use a Runnable to ensure that the RecyclerView has been laid out before setting the
        // ad size for the Native Express ad. This allows us to set the Native Express ad's
        // width to match the full width of the RecyclerView.
        try
        {
            listView.post(new Runnable() {
                @Override
                public void run() {
                    //final float scale = getContext().getResources().getDisplayMetrics().density;
                    // Set the ad size and ad unit ID for each Native Express ad in the items list.
                    for (int i = 0; i <= allPosts.size(); i += ITEMS_PER_AD) {
                        if(i!=0)
                        {
                            final NativeExpressAdView adView =
                                    (NativeExpressAdView) allPosts.get(i);
                            AdSize adSize = new AdSize(ADD_WIDTH, ADD_HEIGHT);
                            adView.setAdSize(adSize);
                            adView.setAdUnitId(getString(R.string.add_big));

                        }

                    }

                    // Load the first Native Express ad in the items list.
                    loadNativeExpressAd(6);
                }
            });

        }catch (Exception e)
        {

        }

    }

    private void loadNativeExpressAd(final int index) {
        try
        {
            if (index >= allPosts.size()) {
                return;
            }

            Object item =allPosts.get(index);
            if (!(item instanceof NativeExpressAdView)) {
                throw new ClassCastException("Expected item at index " + index + " to be a Native"
                        + " Express ad.");
            }

            final NativeExpressAdView adView = (NativeExpressAdView) item;

            // Set an AdListener on the NativeExpressAdView to wait for the previous Native Express ad
            // to finish loading before loading the next ad in the items list.
            adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    // The previous Native Express ad loaded successfully, call this method again to
                    // load the next ad in the items list.
                    loadNativeExpressAd(index + ITEMS_PER_AD);
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
                    // The previous Native Express ad failed to load. Call this method again to load
                    // the next ad in the items list.

                    Log.e("MainActivity", "The previous Native Express ad failed to load. Attempting to"
                            + " load the next Native Express ad in the items list.");
                    loadNativeExpressAd(index + ITEMS_PER_AD);
                }
            });

            // Load the Native Express ad.

            AdRequest request = new AdRequest.Builder().build();
            adView.loadAd(request);
        }catch (Exception e)
        {

        }


    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity;
        if(context instanceof  Activity)
        {
            activity=(Activity) context;
        }
        else
        {
            throw new  RuntimeException(context.toString()+"must implement OnFragmentInteractionListener");
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the com.vstarvibe.vstarvibeapp.activity and potentially other fragments contained in that
     * com.vstarvibe.vstarvibeapp.activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }


    private class PostsFetcher extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(Void... params) {


            JobHttpHandler httpHandler = new JobHttpHandler();

            // Making a request to url and getting response

            String response = httpHandler.makeServiceCall(postBase+page);

            return response;


        }

        @Override
        protected void onPostExecute(String result) {
            getPost(result);
        }
    }

    private class JobHttpHandler {

        String response;
        public String makeServiceCall(String Url) {
            try {
                URL url = new URL(Url);
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                // read the response
                InputStream in = new BufferedInputStream(conn.getInputStream());

                response = convertStreamToString(in);
            } catch (MalformedURLException e) {

            } catch (ProtocolException e) {

            } catch (IOException e) {

            } catch (Exception e) {

            }
            return response;
        }

        private String convertStreamToString(InputStream is) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();

            String line;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line).append('\n');
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return sb.toString();
        }
    }
}


