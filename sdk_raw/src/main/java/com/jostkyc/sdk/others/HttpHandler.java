package com.jostkyc.sdk.others;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by JUMIA-1888 on 2/20/2018.
 */

public class HttpHandler {
    private Context context;
    private RequestDoneInterface callBackSuccess;
    private static final String BASE_URL="https://jostkyc.com/api/";
    private static final String API_NAME="KYC_46";
    private static final String API_KEY="KYC_006star23aqpq5";


    public HttpHandler(Context context)
    {
        this.context=context;

    }



    /*---make http request to the server--*/
    public  String  makePost(String url, final Uri.Builder params, final RequestDoneInterface callback) {


        String response="";
        try
        {

            ApplicationInfo ai = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = ai.metaData;
            String conn_ID = bundle.getString("com.jostkyc.sdk.connection_id");

            url = BASE_URL + url;
            URL uri = new URL(url);
            HttpsURLConnection conn = (HttpsURLConnection) uri.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(30000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            params.appendQueryParameter("api_name", API_NAME);
            params.appendQueryParameter("api_key", API_KEY);
            params.appendQueryParameter("conn_id", conn_ID);
            String query = params.build().getEncodedQuery();
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);
            //Log.d("d","vibe_result: "+response);

        }catch (Exception e)
        {

           e.printStackTrace();
        }

        return response;
    }


    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();

            }
        }

        return sb.toString();
    }



}
