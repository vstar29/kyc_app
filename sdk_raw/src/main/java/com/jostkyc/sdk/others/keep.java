package com.jostkyc.sdk.others;

/**
 * Created by Vstar on 26/03/2018.
 */

public class keep {

    private class Constant {

        public static final String BASE_URL="https://jostkyc.com/api/";
        public static final String API_NAME="KYC_46";
        public static final String API_KEY="KYC_006star23aqpq5";
        public static final String KYC_METHOD_NAME="kyc_method_name";
        public static final String KYC_METHOD_VALUE="kyc_method_value";
        public static final String VERIFY_BANK="verify_bank";
        public static final String SEND_EMAIL_TOKEN="send_email_token";
        public static final String VERIFY_CONN_ID="verifyconn_id";
        public static final String VERIFY_LICENSE="verify_license";
        public static final String SUBMIT_KYC="submit_kyc";
        public static final String SAVE_FACE="save_face";
        public static final String SAVE_ID="save_id";
        public static final int VERIFY_USER_REQUEST=5592;
        public static final String KYC_BANK="kyc_bank";
        public static final String KYC_LICENSE="kyc_license";
        public static final String KYC_FACE_CAPTURE="kyc_face_capture";
        public static final String KYC_ID_CAPTURE="kyc_id_capture";
        public static final String KYC_FRAG_NAME="kyc_frag_name";
        public static final String KYC_FRAG_EMAIL="kyc_frag_email";
        public static final String KYC_FRAG_CAPTURE="kyc_frag_capture";
        public static final String KYC_FRAG_PREVIEW="kyc_frag_preview";
        public static final int RC_HANDLE_CAMERA_PERM = 278;

    }
}
