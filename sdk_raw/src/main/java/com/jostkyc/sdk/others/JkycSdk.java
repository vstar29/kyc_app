package com.jostkyc.sdk.others;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jostkyc.sdk.activities.JkycActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

/**
 * Created by JUMIA-1888 on 3/5/2018.
 */

public class JkycSdk {

	static String conn_ID="";
    static String website="";
	Context context;
	static String errorMsg="";


	public  static void initialize(final Context context)
    {

        try {
            ApplicationInfo ai = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = ai.metaData;
            conn_ID = bundle.getString("com.jostkyc.sdk.connection_id");
            website = bundle.getString("com.jostkyc.sdk.website");
        }catch (Exception e)
        {

        }
        //validate id and units and get request id if empty
        Gson gson = new Gson();
        Uri.Builder params = new Uri.Builder();
        params.appendQueryParameter("conn_id", conn_ID);
        params.appendQueryParameter("website", website);
        AsyncHandler asyncHandler=new AsyncHandler (context,Constant.VERIFY_CONN_ID, params, new RequestDoneInterface()
        {
            @Override
            public void RequestDone(String result) {

                try{
                    JSONObject resp = new JSONObject(result);
                    int status=resp.getInt("status");

                    if(status==0)
                    {
                        errorMsg=resp.getString("error");

                    }
                    else if(status==1)
                    {
                        //JSONObject response = resp.getJSONObject("results");

                    }

                }catch (JSONException e)
                {
                    errorMsg="error";
                    e.printStackTrace();
                }

            }
        });

        asyncHandler.execute();

    }



	public  static void verify(Context context, KycUser kycUser,KycListener kycListener)
	{
		Intent intent = new Intent(context, JkycActivity.class);
		intent.putExtra("kycUser", kycUser);
		((Activity)context).startActivityForResult(intent,Constant.VERIFY_USER_REQUEST);
		
	}



	 /*-@param email{String}
     "response {boolean}--*/
    public static boolean isValidEmail(String email)
    {
        if(TextUtils.isEmpty(email))
        {
            return  false;
        }
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();

    }

	/*-@param email{String}
    "response {boolean}--*/
	public static boolean isValidName(String name)
	{
		if(TextUtils.isEmpty(name))
		{
			return  false;
		}
		name=name.trim();
		String[] names=name.split(" ");
		if(names.length<2)
		{
			return false;
		}
		return true;

	}

    /*-@param phone{String}
     "response {boolean}--*/
    public static boolean isValidPhoneNumber(String phone)
    {

        if (phone.matches("[0-9]+") && phone.length() > 11 && phone.length()<16) {
            return true;
        }
        return  false;

    }

    public static boolean isEmpty(String value)
    {
    	if(TextUtils.isEmpty(value))
    	{
    		return true;
    	}

    	return false;
    }

    public static String generateRequestID() 
    {
	    Random generator = new Random();
	    StringBuilder randomStringBuilder = new StringBuilder();
	    int randomLength = generator.nextInt(12);
	    char tempChar;
	    for (int i = 0; i < randomLength; i++){
	        tempChar = (char) (generator.nextInt(96) + 32);
	        randomStringBuilder.append(tempChar);
	    }
	    return randomStringBuilder.toString();
	}
}
