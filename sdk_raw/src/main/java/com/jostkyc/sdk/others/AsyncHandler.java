package com.jostkyc.sdk.others;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.widget.Toast;

import java.util.Map;


/**
 * Created by VICTOR on 23-Feb-18.
 */

public class AsyncHandler extends AsyncTask<Void, Void, String> {
    private Context context;
    private String url;
    private Uri.Builder data;
    private RequestDoneInterface callback;

    public AsyncHandler(Context context, String url, Uri.Builder data, RequestDoneInterface callback)
    {
        this.context=context;
        this.url=url;
        this.data=data;
        this.callback=callback;
    }


    @Override
    protected void onPreExecute() {
    }

    @Override
    protected String  doInBackground(Void... params) {

        HttpHandler httpHandler =new HttpHandler(context);
        String result =httpHandler.makePost(url,data,callback);
        return  result;
    }

    @Override
    protected void onPostExecute(String result) {
        if(TextUtils.isEmpty(result))
        {
            //Toast.makeText(context,"Connection error",Toast.LENGTH_LONG).show();
        }
        callback.RequestDone(result);

    }
}
