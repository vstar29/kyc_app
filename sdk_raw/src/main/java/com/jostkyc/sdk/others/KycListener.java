package com.jostkyc.sdk.others;

import java.io.Serializable;

/**
 * Created by VICTOR on 06-Mar-18.
 */

public interface KycListener extends Serializable {

    public  void onKError(String error);
}
