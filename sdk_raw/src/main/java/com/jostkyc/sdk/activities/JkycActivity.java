package com.jostkyc.sdk.activities;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.view.View;
import android.widget.TextView;

import com.jostkyc.sdk.R;
import com.jostkyc.sdk.fragments.CaptureFragment;
import com.jostkyc.sdk.fragments.EmailFragment;
import com.jostkyc.sdk.fragments.NameFragment;
import com.jostkyc.sdk.fragments.PreviewFragment;
import com.jostkyc.sdk.others.KycListener;
import com.jostkyc.sdk.others.KycUser;

public class JkycActivity extends AppCompatActivity {

	private  KycUser kycUser;
	private KycUser kycUserBackUp;
	private KycListener kycListener;
	Toolbar toolbar;
	TextView title;
	Fragment fragment;
	TextView kycStageOne, kycStageTwo,kycStageThree;
	private FragmentManager fragmentManager;
	FragmentTransaction fragmentTransaction;
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;
    static final String FRAG_SAVE_INSTANCE="frag_save";
	

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jkyc);
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        title=(TextView) findViewById(R.id.title);
        kycStageOne=(TextView) findViewById(R.id.kyc_step1);
        kycStageTwo=(TextView) findViewById(R.id.kyc_step2);
        kycStageThree=(TextView) findViewById(R.id.kyc_step3);
        setSupportActionBar(toolbar);
        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("");
        toolbar.setTitleTextColor(Color.WHITE);
        sharedPreferences=this.getSharedPreferences("My_preference",0);
        editor= sharedPreferences.edit();
        kycUser= getIntent().getParcelableExtra("kycUser");
        int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (rc != PackageManager.PERMISSION_GRANTED) {
            requestCameraPermission();
        }
        if(savedInstanceState!=null)
        {
            if(kycUserBackUp!=null)
            {
                kycUser=kycUserBackUp;
            }

            fragment=getSupportFragmentManager().getFragment(savedInstanceState,FRAG_SAVE_INSTANCE);
            if(fragment!=null)
            {
                String tag=fragment.getTag();
                switch (tag)
                {
                    case Constant.KYC_FRAG_EMAIL:
                        goNext(0,kycUser);
                        break;
                    case Constant.KYC_FRAG_NAME:
                        goNext(1,kycUser);
                        break;

                    case Constant.KYC_FRAG_CAPTURE:
                        goNext(2,kycUser);
                        break;
                    case Constant.KYC_FRAG_PREVIEW:
                        goNext(2,kycUser);
                        break;

                    default:goNext(0,kycUser);
                        break;
                }

                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        }
        else
        {
            goNext(0,kycUser);
        }


    }

    /**
     * Handles the requesting of the camera permission.  This includes
     * showing a "Snackbar" message of why the permission is needed then
     * sending the request.
     */
    private void requestCameraPermission() {


        final String[] permissions = new String[]{Manifest.permission.CAMERA};

        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this, permissions, Constant.RC_HANDLE_CAMERA_PERM);
            return;
        }

        final Activity thisActivity = this;

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityCompat.requestPermissions(thisActivity, permissions,
                        Constant.RC_HANDLE_CAMERA_PERM);
            }
        };


    }

    public  void changeTitle(String newTitle)
    {
        title.setText(newTitle);
    }


    public void goPreview(KycUser kycUser,byte[] face,byte[] id)
    {
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        PreviewFragment previewFragment = new PreviewFragment();
        Bundle previewCapture = new Bundle();
        previewCapture.putParcelable("kycUser", kycUser);
        previewCapture.putByteArray("face",face);
        previewCapture.putByteArray("id",id);
        previewFragment.setArguments(previewCapture);
        fragmentTransaction.replace(R.id.fragment_container, previewFragment,Constant.KYC_FRAG_PREVIEW);
        title.setText("Preview");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    /*--move to next phase in kyc--*/
    public void goNext(int position,KycUser kycUser)
    {
        this.kycUser=kycUser;
    	switch(position)
    	{
    		case 0:
    		//go email fragments

            fragmentTransaction = getSupportFragmentManager().beginTransaction();
    		EmailFragment emailFragment = new EmailFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable("kycUser", kycUser);
            emailFragment.setArguments(bundle);
			fragmentTransaction.replace(R.id.fragment_container, emailFragment,Constant.KYC_FRAG_EMAIL);
			title.setText("Email verification");
			kycStageOne.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.bg_kyc_active));
    		break;
    		case 1:
    		//go name fragment(Bank/License)
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                NameFragment nameFragment = new NameFragment();
                Bundle bundleName = new Bundle();
                bundleName.putParcelable("kycUser", kycUser);
                nameFragment.setArguments(bundleName);
                fragmentTransaction.replace(R.id.fragment_container, nameFragment,Constant.KYC_FRAG_NAME);
                title.setText("Name verification");
                kycStageOne.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.bg_kyc_normal));
                kycStageTwo.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.bg_kyc_active));
                break;
    		case 2:
    		//go capture fragement
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                CaptureFragment captureFragment = new CaptureFragment();
                Bundle bundleCapture = new Bundle();
                bundleCapture.putParcelable("kycUser", kycUser);
                captureFragment.setArguments(bundleCapture);
                fragmentTransaction.replace(R.id.fragment_container, captureFragment,Constant.KYC_FRAG_CAPTURE);
                title.setText("Face Capture");
                kycStageOne.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.bg_kyc_normal));
                kycStageTwo.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.bg_kyc_normal));
                kycStageThree.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.bg_kyc_active));
                break;
    		case 3:
    		//go submit/preview fragment
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                PreviewFragment previewFragment = new PreviewFragment();
                Bundle previewCapture = new Bundle();
                previewCapture.putParcelable("kycUser", kycUser);
                previewFragment.setArguments(previewCapture);
                fragmentTransaction.replace(R.id.fragment_container, previewFragment,Constant.KYC_FRAG_PREVIEW);
                title.setText("Preview");
                break;
    	}
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode != Constant.RC_HANDLE_CAMERA_PERM) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            // we have permission, so create the camerasource

            return;
        }

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("No Camera")
                .setMessage(R.string.no_camera_permission)
                .setPositiveButton(R.string.ok, listener)
                .show();
    }


    /*---return result to client Activty on kyc finish--*/
    public  void kycFinished(KycUser kycUser,String face_url,String id_url)
    {
        String method=sharedPreferences.getString(Constant.KYC_METHOD_NAME,"");
        Intent returnIntent=new Intent();
        returnIntent.putExtra("STATUS","COMPLETED");
        returnIntent.putExtra("METHOD",method);
        returnIntent.putExtra("EMAIL",kycUser.getEmail());
        returnIntent.putExtra("COUNTRY",kycUser.getCountry());
        returnIntent.putExtra("PHONE",kycUser.getPhone());
        returnIntent.putExtra("REQUEST_ID",kycUser.getRequest_id());
        returnIntent.putExtra("NAME",kycUser.getName());
        returnIntent.putExtra("PHOTOCAPTURE","https://jostkyc.com/"+face_url);
        returnIntent.putExtra("IDCAPTURE","https://jostkyc.com/"+id_url);

        setResult(Activity.RESULT_OK,returnIntent);

        editor.remove(Constant.KYC_METHOD_VALUE);
        editor.remove(Constant.KYC_METHOD_NAME);
        editor.remove(Constant.KYC_FACE_CAPTURE);
        editor.remove(Constant.KYC_ID_CAPTURE);
        editor.commit();
        finish();
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent=new Intent();
        returnIntent.putExtra("STATUS","CANCELLED");
        returnIntent.putExtra("EMAIL",kycUser.getEmail());
        returnIntent.putExtra("REQUEST_ID",kycUser.getRequest_id());
        setResult(Activity.RESULT_OK,returnIntent);
        editor.remove(Constant.KYC_METHOD_VALUE);
        editor.remove(Constant.KYC_METHOD_NAME);
        editor.remove(Constant.KYC_FACE_CAPTURE);
        editor.remove(Constant.KYC_ID_CAPTURE);
        editor.commit();
        finish();
    }

    @Override

    protected  void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        Fragment f=getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        getSupportFragmentManager().putFragment(outState,FRAG_SAVE_INSTANCE,f);
        outState.putParcelable("kycUser",kycUser);
    }

    @Override
    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);
        if (state != null) {
            kycUserBackUp = state.getParcelable("kycUser");
        }
    }

    private class Constant {

        static final String KYC_FACE_CAPTURE="kyc_face_capture";
        static final String KYC_ID_CAPTURE="kyc_id_capture";
        static final int RC_HANDLE_CAMERA_PERM = 278;
        public static final String KYC_METHOD_NAME="kyc_method_name";
        public static final String KYC_METHOD_VALUE="kyc_method_value";
        public static final String KYC_FRAG_NAME="kyc_frag_name";
        public static final String KYC_FRAG_EMAIL="kyc_frag_email";
        public static final String KYC_FRAG_CAPTURE="kyc_frag_capture";
        public static final String KYC_FRAG_PREVIEW="kyc_frag_preview";

    }
}
