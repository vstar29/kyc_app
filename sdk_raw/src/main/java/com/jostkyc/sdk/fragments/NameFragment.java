package com.jostkyc.sdk.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.jostkyc.sdk.R;
import com.jostkyc.sdk.activities.JkycActivity;
import com.jostkyc.sdk.others.AsyncHandler;
import com.jostkyc.sdk.others.ItemClickListener;
import com.jostkyc.sdk.others.KycUser;
import com.jostkyc.sdk.others.RequestDoneInterface;
import com.jostkyc.sdk.others.VerifyMethodAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NameFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NameFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NameFragment extends Fragment implements  ItemClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private KycUser kycUser;
    private LinearLayout methodSelectView;
    private RecyclerView recyclerView;
    private LinearLayout bankView;
    private LinearLayout licenseView;
    private  EditText bankNameView;
    private EditText bankAccountView;
    private EditText licenseNumberView;
    private static EditText dobView;
    private TextView resultViewText;
    private  LinearLayout resultView;
    private String bankCode;
    private String bankName;
    private String[] bankCodes;
    private ProgressBar progressBar;
    private JkycActivity jkycActivity;
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;
    private  String methodName;
    private String methodValue;
    private Button btnVerifyBank,btnGoCapture;
    private  static Button btnVerifyLicense;



    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public NameFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NameFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NameFragment newInstance(String param1, String param2) {
        NameFragment fragment = new NameFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView= inflater.inflate(R.layout.fragment_name, container, false);

        Bundle bundle=getArguments();
        kycUser=bundle.getParcelable("kycUser");
        methodSelectView=(LinearLayout) fragmentView.findViewById(R.id.select);
        bankView=(LinearLayout) fragmentView.findViewById(R.id.bank);
        licenseView=(LinearLayout) fragmentView.findViewById(R.id.license);
        recyclerView=(RecyclerView) fragmentView.findViewById(R.id.recyclerView);
        bankAccountView=(EditText) fragmentView.findViewById(R.id.bank_account);
        bankNameView=(EditText) fragmentView.findViewById(R.id.bank_name);
        btnGoCapture=(Button) fragmentView.findViewById(R.id.btn_go_capture);
        bankNameView.setFocusable(false);
        bankNameView.setClickable(false);
        bankNameView.setLongClickable(false);

        licenseNumberView=(EditText) fragmentView.findViewById(R.id.license_no);
        dobView=(EditText) fragmentView.findViewById(R.id.license_dob);
        dobView.setFocusable(false);
        dobView.setClickable(false);
        dobView.setLongClickable(false);
        resultViewText=(TextView) fragmentView.findViewById(R.id.result_text);
        resultView=(LinearLayout) fragmentView.findViewById(R.id.result);

        final Resources resource= getContext().getResources();
        bankCodes=resource.getStringArray(R.array.kyc_banks_code);
        progressBar=(ProgressBar) fragmentView.findViewById(R.id.progressBar);
        recyclerView.setHasFixedSize(true);
        sharedPreferences=getContext().getSharedPreferences("My_preference",0);
        editor= sharedPreferences.edit();
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(),2);
        recyclerView.setLayoutManager(layoutManager);
        VerifyMethodAdapter verifyMethodAdapter=new VerifyMethodAdapter(getContext());
        recyclerView.setAdapter(verifyMethodAdapter);
        dobView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog(v);
            }
        });
        jkycActivity=(JkycActivity) getActivity();
        btnVerifyBank=(Button) fragmentView.findViewById(R.id.btn_verify_bank);
        btnVerifyLicense=(Button) fragmentView.findViewById(R.id.btn_verify_license);
        btnVerifyLicense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verifyLicense();
            }
        });
        btnVerifyBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verifyBank();
            }
        });
        verifyMethodAdapter.setClickListener(this);
        btnGoCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goFaceCapture();
            }
        });

        bankNameView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String[] banks=resource.getStringArray(R.array.kyc_banks_name);

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                String titleText = "SELECT BANKS";
                ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.BLACK);
                SpannableStringBuilder ssBuilder = new SpannableStringBuilder(titleText);
                ssBuilder.setSpan(
                        foregroundColorSpan,
                        0,
                        titleText.length(),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                );

                // Set the alert dialog title using spannable string builder
                builder.setTitle(ssBuilder);
                builder.setItems(banks, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int pos) {
                        bankCode=bankCodes[pos];
                        bankName=banks[pos];
                        bankNameView.setText(bankName);
                        bankAccountView.requestFocus();
                    }
                });
                builder.show();
            }
        });
        return fragmentView;
    }

    @Override
    public void onClick(View view, int position) {

        switch (position){

            case 0:
                methodSelectView.setVisibility(View.INVISIBLE);
                licenseView.setVisibility(View.INVISIBLE);
                bankView.setVisibility(View.VISIBLE);
                break;
            case 1 :
                methodSelectView.setVisibility(View.INVISIBLE);
                bankView.setVisibility(View.INVISIBLE);
                licenseView.setVisibility(View.VISIBLE);
        }
    }

    public void showDatePickerDialog(View v) {
        DialogFragment dateFragment = new DatePickerFragment();
        //dateFragment.setStyle(DatePickerFragment.,R.style.Theme_AppCompat_DayNight_Dialog);
        dateFragment.show( getActivity().getSupportFragmentManager(), "Date of birth");



    }


    /*--verify License number--*/

    public void verifyLicense()
    {
        progressBar.setVisibility(View.VISIBLE);
        String licenseNumber=licenseNumberView.getText().toString();
        String dob=dobView.getText().toString();
        if(TextUtils.isEmpty(licenseNumber))
        {
            licenseNumberView.setError("Enter  license number");
            licenseNumberView.requestFocus();
            progressBar.setVisibility(View.INVISIBLE);
        }
        else if(TextUtils.isEmpty(dob))
        {
            dobView.setError("Select dob");
            dobView.requestFocus();
            progressBar.setVisibility(View.INVISIBLE);
        }

        else
        {
            Uri.Builder params=new Uri.Builder();
            Gson gson=new Gson();
            String user=gson.toJson(kycUser,KycUser.class);
            params.appendQueryParameter("dob",dob);
            params.appendQueryParameter("license_no",licenseNumber);
           AsyncHandler asyncHandler= new AsyncHandler(getContext(), Constant.VERIFY_LICENSE, params, new RequestDoneInterface() {
                @Override
                public void RequestDone(String result) {
                    licenseVerifyDone(result);
                }
            });
           asyncHandler.execute();
        }
    }

    /*--called affter succesful license verification--*/

    public void licenseVerifyDone(String result)
    {
        try{
            JSONObject resp = new JSONObject(result);
            int status=resp.getInt("status");
            if(status==0)
            {
                String error=resp.getString("error");
                licenseNumberView.setError(error);
                licenseNumberView.requestFocus();
            }
            else if(status==1)
            {
                JSONObject response = resp.getJSONObject("results");
                String LICENSE=response.getString("LICENSE");
                methodName="LICENSE";
                methodValue=LICENSE;
                String name=response.getString("name");
                kycUser.setName(name);
                String phone=response.getString("phone");
                String msg="Your license verification was successful <br>Name: <b style='color:#3a7596'>"+name+"</b><br> Phone: <b style='color:#3a7596'>"+phone+"</b>";
                resultViewText.setText(Html.fromHtml(msg));
                licenseView.setVisibility(View.INVISIBLE);
                resultView.setVisibility(View.VISIBLE);

            }


            progressBar.setVisibility(View.INVISIBLE);
        }catch (JSONException e)
        {
            progressBar.setVisibility(View.INVISIBLE);
            e.printStackTrace();
        }
    }




    /*--verify Nuban account--*/

    public void verifyBank()
    {
        progressBar.setVisibility(View.VISIBLE);
        String accountNumber=bankAccountView.getText().toString();
        Boolean valid=true;
        View toRequest=null;
        if(TextUtils.isEmpty(bankName))
        {
            bankNameView.setError("Select a bank");
            toRequest=bankNameView;
            valid=false;
        }
        else if(TextUtils.isEmpty(accountNumber))
        {
            bankAccountView.setError("Enter your account number");
            toRequest=bankAccountView;
            valid=false;
        }
        else if(accountNumber.length()!=10)
        {
            bankAccountView.setError("Invalid account number");
            toRequest=bankAccountView;
            valid=false;
        }
        if(!valid)
        {
            toRequest.requestFocus();
            progressBar.setVisibility(View.INVISIBLE);
        }
        else
        {
            Uri.Builder params=new Uri.Builder();
            params.appendQueryParameter("bank_name",bankName);
            params.appendQueryParameter("bank_code",bankCode);
            params.appendQueryParameter("bank_account",accountNumber);
            AsyncHandler asyncHandler=new AsyncHandler(getContext(), Constant.VERIFY_BANK, params, new RequestDoneInterface() {
                @Override
                public void RequestDone(String result) {
                    bankVerifyDone(result);
                }
            });
            asyncHandler.execute();
        }
    }

    /*--called after succesful bank verification--*/

    public void bankVerifyDone(String result)
    {
        try{
            JSONObject resp = new JSONObject(result);
            int status=resp.getInt("status");
            if(status==0)
            {
                String error=resp.getString("error");
                bankAccountView.setError(error);
                bankAccountView.requestFocus();
            }
            else if(status==1)
            {
                JSONObject response = resp.getJSONObject("results");
                String BANK=response.getString("BANK");
                methodName="BANK";
                methodValue=BANK;
                String name=response.getString("name");
                kycUser.setName(name);
                String phone=response.getString("phone");
                String msg="Your bank verification was successful <br>Name: <b style='color:#3a7596'>"+name+"</b><br> Phone: <b style='color:#3a7596'>"+phone+"</b>";
                resultViewText.setText(Html.fromHtml(msg));
                bankView.setVisibility(View.INVISIBLE);
                resultView.setVisibility(View.VISIBLE);

            }


            progressBar.setVisibility(View.INVISIBLE);
        }catch (JSONException e)
        {
            progressBar.setVisibility(View.INVISIBLE);
            e.printStackTrace();
        }
    }




    /*--go to face capture fragment after succesful name verification--*/

    public void goFaceCapture()
    {
        editor.putString(Constant.KYC_METHOD_NAME,methodName);
        editor.putString(Constant.KYC_METHOD_VALUE,methodValue);
        editor.commit();
        jkycActivity.goNext(2,kycUser);
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity;
        if(context instanceof  Activity)
        {
            activity=(Activity) context;
        }
        else
        {
            throw new  RuntimeException(context.toString()+"must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


   public static class DatePickerFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

       @Override
       public Dialog onCreateDialog(Bundle savedInstanceState) {
           int year, month, day;
           if (TextUtils.isEmpty(dobView.getText().toString())) {
               final Calendar c = Calendar.getInstance();
               year = c.get(Calendar.YEAR);
               month = c.get(Calendar.MONTH);
               day = c.get(Calendar.DAY_OF_MONTH);
           } else {
               try {
                   String[] date = dobView.getText().toString().split("/");
                   year = Integer.parseInt(date[0]);
                   month = Integer.parseInt(date[1]);
                   day = Integer.parseInt(date[2]);
               } catch (Exception e) {
                   final Calendar c = Calendar.getInstance();
                   year = c.get(Calendar.YEAR);
                   month = c.get(Calendar.MONTH);
                   day = c.get(Calendar.DAY_OF_MONTH);
               }


           }

           // Create a new instance of DatePickerDialog and return it
           return new DatePickerDialog(getActivity(), DatePickerDialog.THEME_DEVICE_DEFAULT_LIGHT, this, year, month, day);
           //return new DatePickerDialog(getActivity(), this, year, month, day);
       }

       public void onDateSet(DatePicker view, int year, int month, int day) {
           // Do something with the date chosen by the user
           month = month + 1;
           String dayStr = Integer.toString(day);
           String monthStr = Integer.toString(month);
           if (dayStr.length() == 1) {
               dayStr = "0" + dayStr;
           }
           if (monthStr.length() == 1) {
               monthStr = "0" + monthStr;
           }
           dobView.setText(year + "/" + (monthStr) + "/" + dayStr);
           btnVerifyLicense.requestFocus();

       }


   }

    private class Constant {

        static final String VERIFY_BANK = "verify_bank";
        static final String VERIFY_LICENSE = "verify_license";
        static final String KYC_METHOD_NAME = "kyc_method_name";
        static final String KYC_METHOD_VALUE = "kyc_method_value";

    }


}
