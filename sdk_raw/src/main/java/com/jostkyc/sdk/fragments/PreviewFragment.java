package com.jostkyc.sdk.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jostkyc.sdk.R;
import com.jostkyc.sdk.activities.JkycActivity;
import com.jostkyc.sdk.others.AsyncHandler;
import com.jostkyc.sdk.others.KycUser;
import com.jostkyc.sdk.others.RequestDoneInterface;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PreviewFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PreviewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PreviewFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private ProgressBar progressBar;
    private JkycActivity jkycActivity;
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;
    private KycUser kycUser;
    private TextView name;
    private TextView email;
    private TextView status;
    private TextView phone;
    private TextView date;
    private TextView method;
    private TextView method_label;
    private ImageView photocapture;
    private ImageView idcapture;
    private LinearLayout toHide1;
    private Button btnSubmit;
    Bitmap faceImage;
    Bitmap idImage;
    private ProgressDialog progressDialog;
    private String face_url;
    private String id_url;
    private byte[] faceBackUp;
    private byte[] idBackUp;
    private byte[] face;
    private byte[] id;
    private  final String SAVE_ID_BYTE="s_id_byte";
    private  final String SAVE_PHOTO_BYTE="s_photo_byte";


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public PreviewFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PreviewFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PreviewFragment newInstance(String param1, String param2) {
        PreviewFragment fragment = new PreviewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        if(savedInstanceState !=null)
        {
            faceBackUp=savedInstanceState.getByteArray(SAVE_PHOTO_BYTE);
            idBackUp=savedInstanceState.getByteArray(SAVE_ID_BYTE);
            if(faceBackUp !=null && idBackUp !=null)
            {
                face=faceBackUp;
                id=idBackUp;
            }
        }
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView= inflater.inflate(R.layout.fragment_preview, container, false);
        Bundle bundle=getArguments();
        kycUser=bundle.getParcelable("kycUser");
        face=bundle.getByteArray("face");
        id=bundle.getByteArray("id");
        if(savedInstanceState !=null)
        {
            faceBackUp=savedInstanceState.getByteArray(SAVE_PHOTO_BYTE);
            idBackUp=savedInstanceState.getByteArray(SAVE_ID_BYTE);
            Log.d("s","vibe_ba:"+faceBackUp.toString());
            Log.d("s","vibe_ba:"+idBackUp.toString());
            if(faceBackUp !=null && idBackUp !=null)
            {
                face=faceBackUp;
                id=idBackUp;
            }
        }
        faceImage = BitmapFactory.decodeByteArray(face, 0, face.length);
        idImage = BitmapFactory.decodeByteArray(id, 0, id.length);
        name=(TextView) fragmentView.findViewById(R.id.name);
        email=(TextView) fragmentView.findViewById(R.id.email);
        btnSubmit=(Button) fragmentView.findViewById(R.id.btn_submit);
        phone=(TextView) fragmentView.findViewById(R.id.phone);
        date=(TextView) fragmentView.findViewById(R.id.date);
        method=(TextView) fragmentView.findViewById(R.id.method);
        method_label=(TextView) fragmentView.findViewById(R.id.method_label);
        photocapture=(ImageView) fragmentView.findViewById(R.id.photocapture);
        idcapture=(ImageView) fragmentView.findViewById(R.id.idcapture);
        sharedPreferences=getContext().getSharedPreferences("My_preference",0);
        progressDialog=new ProgressDialog(getContext(),ProgressDialog.THEME_HOLO_LIGHT);
        progressDialog.setTitle("Submitting..");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        jkycActivity=(JkycActivity) getActivity();
        toHide1=(LinearLayout) fragmentView.findViewById(R.id.hide1);
        name.setText(kycUser.getName());
        email.setText(kycUser.getEmail());
        phone.setText(kycUser.getPhone());
        photocapture.setImageBitmap(faceImage);
        idcapture.setImageBitmap(idImage);
        method.setText(sharedPreferences.getString(Constant.KYC_METHOD_NAME,""));
        if(kycUser.getCountry().equalsIgnoreCase("Others"))
        {
            toHide1.setVisibility(View.INVISIBLE);
            method.setVisibility(View.INVISIBLE);
        }
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.show();
                saveFaceCapture();

            }
        });

        return  fragmentView;

    }

    public  void saveFaceCapture()
    {
        String face_capture=sharedPreferences.getString(Constant.KYC_FACE_CAPTURE,"");
        Uri.Builder params=new Uri.Builder();
        params.appendQueryParameter("face_capture",face_capture);
        AsyncHandler asyncHandler=new AsyncHandler(getContext(), Constant.SAVE_FACE, params, new RequestDoneInterface() {
            @Override
            public void RequestDone(String result) {
                try{
                    JSONObject resp = new JSONObject(result);
                    int status=resp.getInt("status");
                    if(status==1)
                    {
                        JSONObject response = resp.getJSONObject("results");
                        face_url=response.getString("PHOTOCAPTURE");
                        saveIdCapture();
                    }
                    else
                    {

                    }

                }catch (Exception e)
                {
                    progressDialog.dismiss();
                }


            }
        });
        asyncHandler.execute();

    }

    public  void saveIdCapture()
    {

        String id_capture=sharedPreferences.getString(Constant.KYC_ID_CAPTURE,"");
        Uri.Builder params=new Uri.Builder();
        params.appendQueryParameter("id_capture",id_capture);
        AsyncHandler asyncHandler=new AsyncHandler(getContext(), Constant.SAVE_ID, params, new RequestDoneInterface() {
            @Override
            public void RequestDone(String result) {
                try{
                    JSONObject resp = new JSONObject(result);
                    int status=resp.getInt("status");
                    if(status==1)
                    {
                        JSONObject response = resp.getJSONObject("results");
                        id_url=response.getString("IDCAPTURE");
                        saveToDB();
                    }
                    else
                    {

                    }

                }catch (Exception e)
                {
                    progressDialog.dismiss();
                }


            }
        });
        asyncHandler.execute();

    }
    public void saveToDB()
    {
        String conn_ID="";
        String website="";
        String method=sharedPreferences.getString(Constant.KYC_METHOD_NAME,"");
        String method_value=sharedPreferences.getString(Constant.KYC_METHOD_VALUE,"");


        try {
            ApplicationInfo ai = getContext().getPackageManager().getApplicationInfo(getContext().getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = ai.metaData;
            conn_ID = bundle.getString("com.jostkyc.sdk.connection_id");
            website = bundle.getString("com.jostkyc.sdk.website");
            JSONObject jsonObject=new JSONObject();
            if(!TextUtils.isEmpty(method) && !TextUtils.isEmpty(method_value))
            {
                JSONObject method_obj=new JSONObject(method_value);
                jsonObject.put(method,method_obj);
            }

            try{

                jsonObject.put("EMAIL",kycUser.getEmail());
                jsonObject.put("FIRSTNAME",kycUser.getFirstname());
                jsonObject.put("SITE",website);
                jsonObject.put("COUNTRY",kycUser.getCountry());
                jsonObject.put("IDCAPTURE",id_url);
                jsonObject.put("LASTNAME",kycUser.getLastname());
                jsonObject.put("REQUEST_ID",kycUser.getRequest_id());
                jsonObject.put("PHONE",kycUser.getPhone());
                jsonObject.put("ID",conn_ID);
                jsonObject.put("PHOTOCAPTURE",face_url);
                jsonObject.put("STATUS", "COMPLETED");
                jsonObject.put("DETAILS", "Verification Details has been Submitted. Thank you");

            }catch (JSONException e)
            {
                e.printStackTrace();
            }

            Uri.Builder params=new Uri.Builder();
            params.appendQueryParameter("v_details",jsonObject.toString());

            AsyncHandler asyncHandler=new AsyncHandler(getContext(), Constant.SUBMIT_KYC, params, new RequestDoneInterface() {
                @Override
                public void RequestDone(String result) {
                    try{
                        JSONObject resp = new JSONObject(result);
                        int status=resp.getInt("status");

                        if(status==1)
                        {
                            jkycActivity.kycFinished(kycUser,face_url,id_url);
                        }
                        else
                        {

                        }
                        progressDialog.dismiss();
                        }catch (Exception e)
                        {
                            progressDialog.dismiss();
                        }


                }
            });
            asyncHandler.execute();
        }catch (Exception e)
        {

        }


        Intent returnIntent=new Intent();
        returnIntent.putExtra("STATUS","COMPLETED");
        returnIntent.putExtra("METHOD",method);
        returnIntent.putExtra("EMAIL",kycUser.getEmail());
        returnIntent.putExtra("COUNTRY",kycUser.getCountry());
        returnIntent.putExtra("PHONE",kycUser.getPhone());
        returnIntent.putExtra("REQUEST_ID",kycUser.getRequest_id());
        returnIntent.putExtra("NAME",kycUser.getName());
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity;
        if(context instanceof  Activity)
        {
            activity=(Activity) context;
        }
        else
        {
            throw new  RuntimeException(context.toString()+"must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public  void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putByteArray(SAVE_PHOTO_BYTE,face);
        outState.putByteArray(SAVE_ID_BYTE,face);
    }

    private class Constant {

        public static final String KYC_METHOD_NAME="kyc_method_name";
        public static final String KYC_METHOD_VALUE="kyc_method_value";
        public static final String SUBMIT_KYC="submit_kyc";
        public static final String SAVE_FACE="save_face";
        public static final String SAVE_ID="save_id";
        public static final String KYC_FACE_CAPTURE="kyc_face_capture";
        public static final String KYC_ID_CAPTURE="kyc_id_capture";

    }


}
