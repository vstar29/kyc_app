package com.jostkyc.sdk.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.images.Size;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;
import com.google.android.gms.vision.face.LargestFaceFocusingProcessor;
import com.jostkyc.sdk.R;
import com.jostkyc.sdk.activities.JkycActivity;
import com.jostkyc.sdk.others.KycUser;
import com.jostkyc.sdk.others.RequestDoneInterface;
import com.jostkyc.sdk.others.visions.CameraSourcePreview;
import com.jostkyc.sdk.others.visions.GraphicOverlay;
import com.jostkyc.sdk.others.visions.FaceGraphic;


import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CaptureFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CaptureFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CaptureFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "FaceTracker";

    private CameraSource mCameraSource = null;

    private CameraSourcePreview mPreview;
    private  FaceGraphic mFaceGraphic;
    private GraphicOverlay mGraphicOverlay;
    private TextView captureTitle;
    private  TextView errorView;
    private Boolean faceCapture=true;
    private JkycActivity jkycActivity;
    private KycUser kycUser;
    private Button btnCapture,btnGoNext;
    private  FaceDetector detector;
    private byte[] faceByte=null;
    private byte[] idByte=null;
    private CoordinatorLayout coordinatorLayout;
    GraphicOverlay mOverlay;
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;
    Snackbar snacky;

    private static final int RC_HANDLE_GMS = 9001;
    // permission request codes need to be < 256
  

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public CaptureFragment() {
        // Required empty public constructor
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CaptureFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CaptureFragment newInstance(String param1, String param2) {
        CaptureFragment fragment = new CaptureFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView= inflater.inflate(R.layout.fragment_capture, container, false);
        coordinatorLayout=(CoordinatorLayout) fragmentView.findViewById(R.id.main);
        mPreview = (CameraSourcePreview) fragmentView.findViewById(R.id.preview);
        mGraphicOverlay = (GraphicOverlay) fragmentView.findViewById(R.id.faceOverlay);
        btnCapture=(Button) fragmentView.findViewById(R.id.btn_capture);
        btnGoNext=(Button) fragmentView.findViewById(R.id.btn_go_next);
        errorView=(TextView) fragmentView.findViewById(R.id.error);
        captureTitle=(TextView) fragmentView.findViewById(R.id.capture_title);
        jkycActivity=(JkycActivity) getActivity();
        Bundle bundle=getArguments();
        kycUser=bundle.getParcelable("kycUser");
        sharedPreferences=getContext().getSharedPreferences("My_preference",0);
        editor= sharedPreferences.edit();

        // Check for the camera permission before accessing the camera.  If the
        // permission is not granted yet, request permission.
        int rc = ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA);
        if (rc == PackageManager.PERMISSION_GRANTED) {
            createCameraSource();
        }
        btnCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePicture();
            }
        });

        btnGoNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(idByte==null)
                {
                    goIDCapture();
                }
                else {
                    goToPreview();
                }
            }
        });

        return fragmentView;
    }




    private void goIDCapture()
    {
        captureTitle.setText(getString(R.string.kyc_id_capture_title));
        jkycActivity.changeTitle("ID CARD CAPTURE");
        btnGoNext.setVisibility(View.INVISIBLE);
        btnCapture.setVisibility(View.VISIBLE);
        try {

            Context context =getActivity().getApplicationContext();
            mCameraSource = new CameraSource.Builder(context, detector)
                    .setRequestedPreviewSize(320, 240)
                    .setFacing(CameraSource.CAMERA_FACING_BACK)
                    .setRequestedFps(30.0f)
                    .build();

            mPreview.start(mCameraSource, mGraphicOverlay);

        }catch (Exception e)
        {
            Toast.makeText(getContext(),"Problem switching camera",Toast.LENGTH_LONG).show();
        }
    }
    public void showSnackBar(String message)
    {
        snacky=Snackbar.make(coordinatorLayout,message,Snackbar.LENGTH_INDEFINITE).setAction("DISMISS", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }

        });


        snacky.setActionTextColor(Color.RED);
        TextView tv = (TextView) snacky.getView().findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        snacky.show();
    }


    /**
     * Creates and starts the camera.  Note that this uses a higher resolution in comparison
     * to other detection examples to enable the barcode detector to detect small barcodes
     * at long distances.
     */
    private void createCameraSource() {

        Context context =getActivity().getApplicationContext();
        detector = new FaceDetector.Builder(context) .setProminentFaceOnly(true)
                .setClassificationType(FaceDetector.ALL_CLASSIFICATIONS)
                .build();

        /*detector.setProcessor(
                new MultiProcessor.Builder<>(new GraphicFaceTrackerFactory())
                        .build());*/
        detector.setProcessor(
                new LargestFaceFocusingProcessor(
                        detector,
                        new GraphicFaceTracker(mGraphicOverlay)));

        if (!detector.isOperational()) {

            // Note: The first time that an app using face API is installed on a device, GMS will
            // download a native library to the device in order to do detection.  Usually this
            // completes before the app is run for the first time.  But if that download has not yet
            // completed, then the above call will not detect any faces.
            //
            // isOperational() can be used to check if the required native library is currently
            // available.  The detector will automatically become operational once the library
            // download completes on device.
            Log.w(TAG, "Face detector dependencies are not yet available.");
        }


        mCameraSource = new CameraSource.Builder(context, detector)
                .setRequestedPreviewSize(320, 240)
                .setFacing(CameraSource.CAMERA_FACING_FRONT)
                .setRequestedFps(30.0f)
                .build();


    }


    @Override
    public void onResume() {
        super.onResume();
        faceCapture=true;
        captureTitle.setText("Capture your face clearly");
        startCameraSource();
    }

    /**
     * Stops the camera.
     */
    @Override
    public void onPause() {
        super.onPause();
        mPreview.stop();
    }

    /**
     * Releases the resources associated with the camera source, the associated detector, and the
     * rest of the processing pipeline.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        try
        {
            if (mCameraSource != null) {
                mCameraSource.release();
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    /**
     * Callback for the result from requesting permissions. This method
     * is invoked for every call on {@link #requestPermissions(String[], int)}.
     * <p>
     * <strong>Note:</strong> It is possible that the permissions request interaction
     * with the user is interrupted. In this case you will receive empty permissions
     * and results arrays which should be treated as a cancellation.
     * </p>
     *
     * @param requestCode  The request code passed in {@link #requestPermissions(String[], int)}.
     * @param permissions  The requested permissions. Never null.
     * @param grantResults The grant results for the corresponding permissions
     *                     which is either {@link PackageManager#PERMISSION_GRANTED}
     *                     or {@link PackageManager#PERMISSION_DENIED}. Never null.
     * @see #requestPermissions(String[], int)
     */


    /**
     * Starts or restarts the camera source, if it exists.  If the camera source doesn't exist yet
     * (e.g., because onResume was called before the camera source was created), this will be called
     * again when the camera source is created.
     */
    private void startCameraSource() {

        // check that the device has play services available.
        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
                getActivity().getApplicationContext());
        if (code != ConnectionResult.SUCCESS) {
            Dialog dlg =
                    GoogleApiAvailability.getInstance().getErrorDialog(getActivity(), code, RC_HANDLE_GMS);
            dlg.show();
        }

        if (mCameraSource != null) {
            try {
                mPreview.start(mCameraSource, mGraphicOverlay);
            } catch (IOException e) {
                Log.e(TAG, "Unable to start camera source.", e);
                mCameraSource.release();
                mCameraSource = null;
            }
        }
    }

    //==============================================================================================
    // Graphic Face Tracker
    //==============================================================================================

    /**
     * Factory for creating a face tracker to be associated with a new face.  The multiprocessor
     * uses this factory to create face trackers as needed -- one for each individual.
     */
    private class GraphicFaceTrackerFactory implements MultiProcessor.Factory<Face> {
        @Override
        public Tracker<Face> create(Face face) {
            return new GraphicFaceTracker(mGraphicOverlay);
        }
    }

    /**
     * Face tracker for each detected individual. This maintains a face graphic within the app's
     * associated face overlay.
     */
    private class GraphicFaceTracker extends Tracker<Face> {


        GraphicFaceTracker(GraphicOverlay overlay) {
            mOverlay = overlay;
            mFaceGraphic = new FaceGraphic(overlay);

        }

        /**
         * Start tracking the detected face instance within the face overlay.
         */
        @Override
        public void onNewItem(int faceId, Face item) {
            mFaceGraphic.setId(faceId);
        }

        /**
         * Update the position/characteristics of the face within the overlay.
         */
        @Override
        public void onUpdate(FaceDetector.Detections<Face> detectionResults, Face face) {
            mOverlay.add(mFaceGraphic);
            mFaceGraphic.updateFace(face);
        }

        /**
         * Hide the graphic when the corresponding face was not detected.  This can happen for
         * intermediate frames temporarily (e.g., if the face was momentarily blocked from
         * view).
         */
        @Override
        public void onMissing(FaceDetector.Detections<Face> detectionResults) {
            mOverlay.remove(mFaceGraphic);
        }

        /**
         * Called when the face is assumed to be gone for good. Remove the graphic annotation from
         * the overlay.
         */
        @Override
        public void onDone() {
            mOverlay.remove(mFaceGraphic);
        }
    }




    private  void takePicture()
    {
        errorView.setVisibility(View.INVISIBLE);
        mCameraSource.takePicture(null, new CameraSource.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] bytes) {
        try
        {
            snacky.dismiss();
        }catch (Exception e)
        {

        }

                if(faceCapture)
                {

                    if(mFaceGraphic.isValidFaceCapture() && mOverlay.isValidCapture())
                    {
                        faceByte=bytes;
                        mCameraSource.release();
                        mPreview.stop();
                        faceCapture=false;
                        btnGoNext.setVisibility(View.VISIBLE);
                        btnCapture.setVisibility(View.INVISIBLE);


                    }
                    else
                    {

                        showSnackBar(getString(R.string.kyc_face_capture_error));

                    }
                }
                else{
                    mCameraSource.release();
                    mPreview.stop();
                    idByte=bytes;
                    btnCapture.setVisibility(View.INVISIBLE);
                    btnGoNext.setVisibility(View.VISIBLE);
                    /*Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                    mPreview.stop();
                        if(isDark(bitmap))
                        {

                            showSnackBar(getString(R.string.kyc_id_capture_error));
                            try {
                                mPreview.start(mCameraSource, mGraphicOverlay);
                            }catch (Exception e)
                            {

                            }

                        }
                        else
                        {
                            mCameraSource.release();
                            mPreview.stop();
                            idByte=bytes;
                            btnCapture.setVisibility(View.INVISIBLE);
                            btnGoNext.setVisibility(View.VISIBLE);

                        }*/

                    }

            }
        });
    }

    private void goToPreview()
    {
        String face_capture = Base64.encodeToString(faceByte, Base64.DEFAULT);
        String id_capture = Base64.encodeToString(idByte, Base64.DEFAULT);

        editor.putString(Constant.KYC_FACE_CAPTURE,face_capture);
        editor.putString(Constant.KYC_ID_CAPTURE,id_capture);
        editor.commit();
        jkycActivity.goPreview(kycUser,faceByte,idByte);
    }

    /*--check for darkness of capture--*/

    private static boolean isDark(Bitmap bitmap){
        return false;
       /* boolean dark=false;

        float darkThreshold = bitmap.getWidth()*bitmap.getHeight()*1f;
        Log.d("d","vibe_th: +"+darkThreshold);
        int darkPixels=0;

        int[] pixels = new int[bitmap.getWidth()*bitmap.getHeight()];
        bitmap.getPixels(pixels,0,bitmap.getWidth(),0,0,bitmap.getWidth(),bitmap.getHeight());

        for(int i=0;i<pixels.length; i++){
            int color = pixels[i];
            int r = Color.red(color);
            int g = Color.green(color);
            int b = Color.blue(color);
            double luminance = (0.299*r+0.0f + 0.587*g+0.0f + 0.114*b+0.0f);
            if (luminance<150) {
                darkPixels++;
            }
        }
        Log.d("d","vibe_dark: +"+darkPixels);
        if (darkPixels >= darkThreshold) {
            dark = true;
        }
        return dark;*/
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity;
        if(context instanceof  Activity)
        {
            activity=(Activity) context;
        }
        else
        {
            throw new  RuntimeException(context.toString()+"must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private class Constant {

        static final String KYC_FACE_CAPTURE="kyc_face_capture";
        static final String KYC_ID_CAPTURE="kyc_id_capture";

    }

    public class HttpHandler {
        private Context context;
        private RequestDoneInterface callBackSuccess;
        private static final String BASE_URL="https://jostkyc.com/api/";
        private static final String API_NAME="KYC_46";
        private static final String API_KEY="KYC_006star23aqpq5";


        public HttpHandler(Context context)
        {
            this.context=context;

        }



        /*---make http request to the server--*/
        public  String  makePost(String url, final Uri.Builder params, final RequestDoneInterface callback) {


            String response="";
            try
            {
                ApplicationInfo ai = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
                Bundle bundle = ai.metaData;
                String conn_ID = bundle.getString("com.jostkyc.sdk.connection_id");

                url = BASE_URL + url;
                URL uri = new URL(url);
                HttpsURLConnection conn = (HttpsURLConnection) uri.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(30000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                params.appendQueryParameter("api_name", API_NAME);
                params.appendQueryParameter("api_key", API_KEY);
                params.appendQueryParameter("conn_id", conn_ID);
                String query = params.build().getEncodedQuery();
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                InputStream in = new BufferedInputStream(conn.getInputStream());
                response = convertStreamToString(in);
                //Log.d("d","vibe_result: "+response);

            }catch (Exception e)
            {

                e.printStackTrace();
            }

            return response;
        }


        private String convertStreamToString(InputStream is) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();

            String line;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line).append('\n');
                }
            } catch (IOException e) {
                e.printStackTrace();

            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();

                }
            }

            return sb.toString();
        }



    }




}
