package com.jostkyc.sdk.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jostkyc.sdk.R;
import com.jostkyc.sdk.activities.JkycActivity;
import com.jostkyc.sdk.others.AsyncHandler;
import com.jostkyc.sdk.others.JkycSdk;
import com.jostkyc.sdk.others.KycUser;
import com.jostkyc.sdk.others.RequestDoneInterface;
import com.jostkyc.sdk.others.VerifiedUser;
import com.lamudi.phonefield.PhoneEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EmailFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EmailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EmailFragment extends Fragment
{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private  KycUser kycUser;
    private LinearLayout REGSection;
    private RelativeLayout KYCSection;
    private LinearLayout verifySection;
    private RelativeLayout emailInfo;
    private EditText nameView;
    private EditText emailView;
    private EditText ed1, ed2, ed3, ed4, ed5, ed6;
    private PhoneEditText phoneView;
    private String actualToken;
    private ProgressBar progressBar;
    private Button btnSendToken,btnVerifyToken,btnRegToken;
    private  Boolean resendToken=false;
    TextView errorView,resendTokenView;
    TextView tokenLabel,enterTokenLabel;
    JkycActivity jkycActivity;
    TextInputLayout emailLayout;
    TextInputLayout nameLayout;
    TextInputLayout phoneLayout;
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;
    String v_status;
    VerifiedUser verifiedUser;

    private OnFragmentInteractionListener mListener;

    public EmailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EmailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EmailFragment newInstance(String param1, String param2) {
        EmailFragment fragment = new EmailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView=inflater.inflate(R.layout.fragment_email, container, false);
        Bundle bundle=getArguments();
        kycUser=bundle.getParcelable("kycUser");
        KYCSection=(RelativeLayout) fragmentView.findViewById(R.id.KYC);
        REGSection=(LinearLayout) fragmentView.findViewById(R.id.REG);
        emailInfo=(RelativeLayout) fragmentView.findViewById(R.id.email_info);
        nameView=(EditText) fragmentView.findViewById(R.id.name);
        emailView=(EditText) fragmentView.findViewById(R.id.email);
        emailLayout=(TextInputLayout) fragmentView.findViewById(R.id.input_layout_email);
        nameLayout=(TextInputLayout) fragmentView.findViewById(R.id.input_layout_name);
        phoneLayout=(TextInputLayout) fragmentView.findViewById(R.id.input_layout_phone);
        phoneView=(PhoneEditText) fragmentView.findViewById(R.id.phone);
        progressBar=(ProgressBar) fragmentView.findViewById(R.id.progressBar);
        errorView=(TextView) fragmentView.findViewById(R.id.error);
        resendTokenView=(TextView) fragmentView.findViewById(R.id.resend_token);
        verifySection=(LinearLayout) fragmentView.findViewById(R.id.v_email);
        phoneView.setDefaultCountry("NG");
        sharedPreferences=getContext().getSharedPreferences("My_preference",0);
        editor= sharedPreferences.edit();
        phoneView.setTextColor(Color.BLACK);
        phoneView.setHint(R.string.kyc_phone_number);
        enterTokenLabel=(TextView) fragmentView.findViewById(R.id.enter_token);
        tokenLabel=(TextView) fragmentView.findViewById(R.id.emailtoken_label);
        btnRegToken=(Button) fragmentView.findViewById(R.id.btn_reg);
        btnSendToken=(Button) fragmentView.findViewById(R.id.btn_send_token);
        btnVerifyToken=(Button) fragmentView.findViewById(R.id.btn_verify_token);

        if(!kycUser.getEmail().equalsIgnoreCase(""))
        {
            emailView.setVisibility(View.GONE);
            emailLayout.setVisibility(View.GONE);
        }

        if(!kycUser.getPhone().equalsIgnoreCase(""))
        {
            phoneView.setVisibility(View.GONE);
            phoneLayout.setVisibility(View.GONE);
        }

        if(!kycUser.getFirstname().equalsIgnoreCase("") && !kycUser.getLastname().equalsIgnoreCase(""))
        {
            nameView.setVisibility(View.GONE);
            nameLayout.setVisibility(View.GONE);
        }


        tokenLabel.setText("A verification code will be send to "+kycUser.getEmail() + " in order to confirm that you have access to it");
        if(kycUser.getType().equalsIgnoreCase("REG"))
        {
            REGSection.setVisibility(View.VISIBLE);
            KYCSection.setVisibility(View.INVISIBLE);
        }
        else
        {
            REGSection.setVisibility(View.INVISIBLE);
            KYCSection.setVisibility(View.VISIBLE);
        }
        jkycActivity=(JkycActivity) getActivity();

        ed1 = (EditText) fragmentView.findViewById(R.id.ed1);
        ed2 = (EditText)fragmentView.findViewById(R.id.ed2);
        ed3 = (EditText) fragmentView.findViewById(R.id.ed3);
        ed4 = (EditText) fragmentView.findViewById(R.id.ed4);
        ed5 = (EditText) fragmentView.findViewById(R.id.ed5);
        ed6 = (EditText) fragmentView.findViewById(R.id.ed6);
        ed1.requestFocus();
        ed1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    ed2.setEnabled(true);
                    ed2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        ed2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    ed3.setEnabled(true);
                    ed3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        ed3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    ed4.setEnabled(true);
                    ed4.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        ed4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(ed6.getWindowToken(), 0);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        ed5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    ed6.setEnabled(true);
                    ed6.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        ed6.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ed6.getWindowToken(), 0);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btnSendToken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendToken();
            }
        });

        btnRegToken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                Boolean valid=true;
                View focusView=null;
                String email=kycUser.getEmail();
                String name=kycUser.getName();
                String phone=kycUser.getPhone();
                String country=kycUser.getCountry();

                if(email.equalsIgnoreCase(""))
                {
                    email=emailView.getText().toString();
                    if(TextUtils.isEmpty(email))
                    {
                        emailView.setError("Email is required");
                        valid=false;
                        focusView=emailView;
                    }
                    else if(!JkycSdk.isValidEmail(email))
                    {
                        emailView.setError("Invalid Email");
                        valid=false;
                        focusView=emailView;
                    }
                }

                if(kycUser.getFirstname().equalsIgnoreCase("") ||kycUser.getLastname().equalsIgnoreCase("") )
                {
                    name=nameView.getText().toString().trim();

                    if(TextUtils.isEmpty(name))
                    {
                        nameView.setError("Name is required");
                        valid=false;
                        focusView=nameView;
                    }
                    else if(!JkycSdk.isValidName(name))
                    {
                        nameView.setError("Enter full name");
                        valid=false;
                        focusView=nameView;
                    }

                }

                if(phone.equalsIgnoreCase(""))
                {
                    phone=phoneView.getPhoneNumber();

                     if(!phoneView.isValid())
                     {

                        phoneView.setError("Invalid phone number");
                        valid=false;
                        focusView=phoneView;

                     }
                     else
                     {
                         phone = phone.replace("+", "");
                     }

                }
                if(country.equalsIgnoreCase(""))
                {
                    country = "Nigeria";
                    String code = phone.substring(0, 3);

                    if (!code.equalsIgnoreCase("234")) {
                        country = "Others";
                    }

                }



                if(!valid)
                {

                    progressBar.setVisibility(View.INVISIBLE);
                    focusView.requestFocus();
                }
                else {

                    String[] names = name.split(" ");
                    kycUser.setFirstname(names[0]);
                    kycUser.setLastname(names[1]);
                    kycUser.setCountry(country);
                    kycUser.setEmail(email);
                    kycUser.setPhone(phone);
                    sendToken();
                }
            }
        });

        btnVerifyToken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verifyToken();
            }
        });

        resendTokenView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resendToken=true;
                sendToken();
            }
        });

        return fragmentView;

    }

    /*--send email token to user for verification--*/

    private  void sendToken()
    {

        progressBar.setVisibility(View.VISIBLE);
        Uri.Builder params=new Uri.Builder();
        params.appendQueryParameter("email",kycUser.getEmail());
        params.appendQueryParameter("type","email");
       AsyncHandler asyncHandler= new AsyncHandler(getContext(), Constant.SEND_EMAIL_TOKEN, params, new RequestDoneInterface() {
            @Override
            public void RequestDone(String result) {
                tokenSendDone(result);
            }
        });
       asyncHandler.execute();

    }


    /*--after token has been sent succesfully--*/

    private void tokenSendDone(String result){

        try{
            enterTokenLabel.setText("Enter the token sent to "+kycUser.getEmail()+" in the box below");
            JSONObject resp = new JSONObject(result);
            int status=resp.getInt("status");
            if(status==0)
            {
                String error=resp.getString("error");
                errorView.setText(error);
            }
            else if(status==1)
            {
                JSONObject response = resp.getJSONObject("results");
                actualToken=response.getString("email_token");
                v_status=response.getString("v_status");
                if(v_status.equalsIgnoreCase("Suspended")|| v_status.equalsIgnoreCase("Completed"))
                {
                  String v_details=response.getString("v_details");
                    Gson gson=new Gson();
                    verifiedUser=gson.fromJson(v_details, VerifiedUser.class);
                    if(v_status.equalsIgnoreCase("Suspended"))
                    {
                        jkycActivity.goSuspendedPreview(verifiedUser);
                    }
                }
                if(!resendToken){
                    if(kycUser.getType().equalsIgnoreCase("KYC"))
                    {
                        KYCSection.setVisibility(View.INVISIBLE);
                        verifySection.setVisibility(View.VISIBLE);
                    }
                    else if(kycUser.getType().equalsIgnoreCase("REG"))
                    {
                        REGSection.setVisibility(View.INVISIBLE);
                        verifySection.setVisibility(View.VISIBLE);
                    }

                    resendThread();
                    if(TextUtils.isEmpty(kycUser.getRequest_id()));
                    {
                        String req_id=response.getString("request_id");
                        kycUser.setRequest_id(req_id);
                    }
                }
                else
                {
                    resendToken=false;
                    resendTokenView.setVisibility(View.INVISIBLE);
                    Toast.makeText(getContext(),"Token sent",Toast.LENGTH_LONG).show();
                    resendThread();
                }

            }

            progressBar.setVisibility(View.INVISIBLE);
        }catch (JSONException e)
        {
            progressBar.setVisibility(View.INVISIBLE);
            e.printStackTrace();
        }
    }


   private void resendThread()
   {
       resendTokenView.postDelayed(new Runnable() {
           public void run() {
               resendTokenView.setVisibility(View.VISIBLE);
           }
       }, 11000);
   }

    /*--verify token--*/
    private void verifyToken()
    {

        if (ed1.getText().toString().trim().isEmpty()) {
            Toast.makeText(getContext(), "Please enter verification OTP", Toast.LENGTH_SHORT).show();
        } else if (ed2.getText().toString().trim().isEmpty()) {
            Toast.makeText(getContext(), "Please enter verification OTP", Toast.LENGTH_SHORT).show();
        } else if (ed3.getText().toString().trim().isEmpty()) {
            Toast.makeText(getContext(), "Please enter verification OTP", Toast.LENGTH_SHORT).show();
        } else if (ed4.getText().toString().trim().isEmpty()) {
            Toast.makeText(getContext(), "Please enter verification OTP", Toast.LENGTH_SHORT).show();
        } else {
            String token=ed1.getText().toString().trim() + ed2.getText().toString().trim() + ed3.getText().toString().trim() + ed4.getText().toString().trim();
            if(!token.equalsIgnoreCase(actualToken))
            {
                ed1.setError("Wrong Token");
                ed1.requestFocus();
            }
            else
            {
                if(v_status.equalsIgnoreCase("Completed"))
                {
                    try
                    {
                        jkycActivity.goRetrieve(verifiedUser);

                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                }
                else {
                        if(kycUser.getCountry().equalsIgnoreCase("Nigeria"))
                        {
                            jkycActivity.goNext(1,kycUser);
                        }
                        else
                        {
                            jkycActivity.goNext(2,kycUser);
                        }

                }

            }


        }



    }



   /* public void hideKeyboard() {

        ((InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE)).
                hideSoftInputFromWindow(tokenView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }*/

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity;
        if(context instanceof  Activity)
        {
            activity=(Activity) context;
        }
        else
        {
            throw new  RuntimeException(context.toString()+"must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private class Constant {

        static final String SEND_EMAIL_TOKEN="send_email_token";
        static final String KYC_PREVIEW_KEEP="kyc_preview_keep";


    }

    private class HttpHandler {
        private Context context;
        private RequestDoneInterface callBackSuccess;
        private static final String BASE_URL="https://jostkyc.com/api/";
        private static final String API_NAME="KYC_46";
        private static final String API_KEY="KYC_006star23aqpq5";


        public HttpHandler(Context context)
        {
            this.context=context;

        }



        /*---make http request to the server--*/
        public  String  makePost(String url, final Uri.Builder params, final RequestDoneInterface callback) {


            String response="";
            try
            {

                ApplicationInfo ai = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
                Bundle bundle = ai.metaData;
                String conn_ID = bundle.getString("com.jostkyc.sdk.connection_id");

                url = BASE_URL + url;
                URL uri = new URL(url);
                HttpsURLConnection conn = (HttpsURLConnection) uri.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(30000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                params.appendQueryParameter("api_name", API_NAME);
                params.appendQueryParameter("api_key", API_KEY);
                params.appendQueryParameter("conn_id", conn_ID);
                String query = params.build().getEncodedQuery();
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                InputStream in = new BufferedInputStream(conn.getInputStream());
                response = convertStreamToString(in);
                //Log.d("d","vibe_result: "+response);

            }catch (Exception e)
            {

                e.printStackTrace();
            }

            return response;
        }


        private String convertStreamToString(InputStream is) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();

            String line;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line).append('\n');
                }
            } catch (IOException e) {
                e.printStackTrace();

            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();

                }
            }

            return sb.toString();
        }



    }


}
