package com.jostkyc.sdk.others;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;


import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by JUMIA-1888 on 3/5/2018.
 */

public class KycUser implements Parcelable  {

	private String email;
	private String type;
	private String firstname;
	private String lastname;
	private String country;
	private String phone;
	private String request_id;
    private String conn_ID;
    private String website;
    private KycListener kycListener;
    Boolean valid=true;
    String error="";
    private  String name;

	public KycUser(String email,String phone,String country,String type,String firstname, String lastname,String request_id)
	{
		this.email=email;
		this.phone=phone;
		this.country=country;
		this.type=type;
		this.firstname=firstname;
		this.lastname=lastname;
		this.request_id=request_id;

	}




    public KycUser(Parcel in)
    {
        email=in.readString();
        phone=in.readString();
        country=in.readString();
        type=in.readString();
        firstname=in.readString();
        lastname=in.readString();
        request_id=in.readString();


    }

    public String getName() {
        return this.firstname+" "+this.lastname;
    }

    public String getCountry() {
		return country;
	}

	public String getEmail() {
		return email;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

    public String getPhone() {
        return phone;
    }

    public String getRequest_id() {
        return request_id;
    }


    public String getType() {
        return type;
    }


    public void setCountry(String country) {
        this.country = country;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setConn_ID(String conn_ID) {
        this.conn_ID = conn_ID;
    }


    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setName(String name) {
	    try {

            String [] names=name.split(" ");
            this.firstname=names[0];
            this.lastname=names[1];
            }catch (Exception e)
        {
            e.printStackTrace();
        }


    }

    @Override
    public void writeToParcel(Parcel out, int flags)
    {
        out.writeString(email);;
        out.writeString(phone);
        out.writeString(country);
        out.writeString(type);
        out.writeString(firstname);
        out.writeString(lastname);
        out.writeString(request_id);



    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<KycUser> CREATOR
            = new Parcelable.Creator<KycUser>() {
        // @Override
        public KycUser createFromParcel(Parcel in) {
            return new KycUser(in);
        }

        // @Override
        public KycUser[] newArray(int size) {
            return new KycUser[size];
        }
    };



	/*--start kyc process here--*/
	public void startKyc(Context context)
	{

	    if(!TextUtils.isEmpty(JkycSdk.errorMsg))
        {
            kycListener.onKError(JkycSdk.errorMsg);
        }
		else if(validateParameters(context))
        {
            KycUser toverify =new KycUser(email,phone,country,type,firstname,lastname,request_id);
            JkycSdk.verify(context,toverify,kycListener);
        }
        else
        {
            kycListener.onKError(error);
        }

	}


	public  void setJkycListener(KycListener kycListener)
    {
        this.kycListener=kycListener;
    }

	private Boolean  validateParameters(Context context)
	{


            try {
                ApplicationInfo ai = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
                Bundle bundle = ai.metaData;
                String conn_ID = bundle.getString("com.jostkyc.sdk.connection_id");
                setConn_ID(conn_ID);
            }catch (Exception e)
            {
                error="com.jostkyc.sdk.connection_id not set in manifest";
                valid= false;
            }
            if(JkycSdk.isEmpty(this.type) )		{
                error="type can not empty";
                valid= false;
            }

            else if(!JkycSdk.isValidEmail(this.email) && !type.equalsIgnoreCase("REG"))
            {
                error="Invalid email";
                valid= false;
            }

            else if(! this.type.equalsIgnoreCase("KYC") &&  ! this.type.equalsIgnoreCase("REG"))
            {
                error="type can only be REG or KYC";
                valid=false;
            }

            else if(this.type.equalsIgnoreCase("KYC") && (JkycSdk.isEmpty(this.firstname)|| JkycSdk.isEmpty(this.lastname))&& this.country.equalsIgnoreCase("Others"))
            {
                error=" firstname and lastname are required for type 'KYC' when country is not Nigeria ";
                valid= false;
            }

            if(this.type.equalsIgnoreCase("KYC") && JkycSdk.isEmpty(this.phone))
            {
                error="phone is required for type 'KYC' ";
                valid= false;
            }


            if(this.type.equalsIgnoreCase("KYC") && JkycSdk.isEmpty(this.email))
            {
                error="email is required for type 'KYC' ";
                valid= false;
            }

            if(this.type.equalsIgnoreCase("KYC") && JkycSdk.isEmpty(this.country))
            {
                error="country is required for type 'KYC' ";
                valid= false;
            }

            return valid;

        //}

        /*else
        {
            return  valid;
        }*/
	}
	

}


