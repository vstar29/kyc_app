package com.jostkyc.sdk.others;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.jostkyc.sdk.R;

import java.util.ArrayList;

/**
 * Created by VICTOR on 27-Feb-18.
 */

public class VerifyMethodAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private LayoutInflater layoutinflater;
    private ItemClickListener clickListener;



    public VerifyMethodAdapter(Context context) {
        this.context = context;
        layoutinflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView title;
        LinearLayout image;
        LinearLayout parent;

        Holder(View view) {
            super(view);

            image=(LinearLayout) view.findViewById(R.id.image);
            title=(TextView) view.findViewById(R.id.title);
            parent=(LinearLayout) view.findViewById(R.id.parent);

            view.setOnClickListener(this);
        };

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }

    }


    @Override
    public int getItemCount() {
        return 2;
    }




    @Override
    public long getItemId(int position) {
        return position;
    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View relatedModel = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.verify_method_select_model, viewGroup, false);
        return new Holder (relatedModel);


    }

    public void setClickListener(ItemClickListener itemClickListener) {

        this.clickListener = itemClickListener;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        final int position_final=position;
        Holder itemHolder=(Holder) holder;
        switch (position)
        {
            case 0:
            itemHolder.title.setText("BANK ACCOUNT");
            itemHolder.image.setBackgroundDrawable(ContextCompat.getDrawable(context,R.drawable.bank));
            setAnimation(((Holder) holder).parent,position);
            break;
            case 1:
                itemHolder.title.setText("DRIVER LICENSE");
                itemHolder.image.setBackgroundDrawable(ContextCompat.getDrawable(context,R.drawable.license));
                setAnimation(((Holder) holder).parent,position);
        }



    }

    private  void setAnimation(LinearLayout view, int position)
    {

        Animation animation= AnimationUtils.loadAnimation(context,android.R.anim.slide_in_left);
        view.startAnimation(animation);

    }
}
