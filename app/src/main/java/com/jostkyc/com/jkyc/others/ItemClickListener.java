package com.jostkyc.com.jkyc.others;


import android.view.View;

/**
 * Created by VICTOR on 05-Aug-17.
 */
public interface ItemClickListener {
    public void onClick(View view, int position);
}
