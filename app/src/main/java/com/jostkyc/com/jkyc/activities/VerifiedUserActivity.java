package com.jostkyc.com.jkyc.activities;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.jostkyc.com.jkyc.R;
import com.jostkyc.com.jkyc.others.VerifiedUser;
import com.jostkyc.com.jkyc.others.VerifiedUsersFragmentAdapter;

import java.util.ArrayList;

public class VerifiedUserActivity extends AppCompatActivity  {

	private ViewPager mViewPager;
	private VerifiedUsersFragmentAdapter mAdapter;
	private ArrayList<VerifiedUser> verifiedUsers;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verified_user);
    	mViewPager=(ViewPager) findViewById(R.id.viewpager);
        verifiedUsers=getIntent().getParcelableArrayListExtra("verifiedUsers");
   		mAdapter=new VerifiedUsersFragmentAdapter(this,  getSupportFragmentManager(),verifiedUsers);
   		mViewPager.setAdapter(mAdapter);
    }


}
