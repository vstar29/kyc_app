package com.jostkyc.com.jkyc.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jostkyc.com.jkyc.R;
import com.jostkyc.com.jkyc.others.VerifiedUser;

import java.util.ArrayList;

public class NotifyCallBackActivity extends AppCompatActivity {
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notify_call_back);
        progressBar=(ProgressBar) findViewById(R.id.progressBar);

        try
        {
            String title = getIntent().getStringExtra("message");
            String type = getIntent().getStringExtra("type");

            String data="";
            if(type.equalsIgnoreCase("DONE"))
            {
                data = getIntent().getStringExtra("v_details");

            }
            else if(type.equalsIgnoreCase("RECEIVED"))
            {
                data = getIntent().getStringExtra("id");
            }

            if(type.equalsIgnoreCase("DONE"))
            {
                Gson gson=new Gson();
                VerifiedUser user=gson.fromJson(data, VerifiedUser.class);
                ArrayList<VerifiedUser> verifiedUsers=new ArrayList<>();
                verifiedUsers.add(0,user);
                Intent intent=new Intent(this,VerifiedUserActivity.class);
                intent.putParcelableArrayListExtra("verifiedUsers",verifiedUsers);
                startActivity(intent);
            }
            else if(type.equalsIgnoreCase("RECEIVED"))
            {
                Intent intent = new Intent(this, DeepLinkActivity.class);
                intent.putExtra("id", data);
                startActivity(intent);

            }
        }catch (Exception e)
        {
            e.printStackTrace();
            progressBar.setVisibility(View.INVISIBLE);
            Intent intent =new Intent(this,RegisterActivity.class);
            startActivity(intent);
        }


    }
}
