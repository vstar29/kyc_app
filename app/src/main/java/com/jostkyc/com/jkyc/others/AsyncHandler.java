package com.jostkyc.com.jkyc.others;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import java.util.Map;


/**
 * Created by VICTOR on 23-Feb-18.
 */

public class AsyncHandler extends AsyncTask<Void, Void, String> {
    private Context context;
    private String url;
    private Map<String, String> data;
    private RequestDoneInterface callback;

    public  AsyncHandler(Context context, String url, Map<String,String> data,RequestDoneInterface callback)
    {
        this.context=context;
        this.url=url;
        this.data=data;
        this.callback=callback;
    }



    @Override
    protected void onPreExecute() {
    }

    @Override
    protected String  doInBackground(Void... params) {

        /*HttpHandler httpHandler =new HttpHandler(context);
        httpHandler.makePost(url,data,callback);*/
        return  "";
    }

    @Override
    protected void onPostExecute(String result) {
        Toast.makeText(context,result,Toast.LENGTH_LONG).show();

    }
}
