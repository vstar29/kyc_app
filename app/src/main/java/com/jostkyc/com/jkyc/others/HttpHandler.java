package com.jostkyc.com.jkyc.others;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

/**
 * Created by JUMIA-1888 on 2/20/2018.
 */

public class HttpHandler {
    private Context context;
    private  final String baseUrl="https://jostkyc.com/api/";
    private final String api_name="KYC_46";
    private final String api_key="KYC_006star23aqpq5";

    public HttpHandler(Context context)
    {
        this.context=context;
    }


    /*---make http request to the server--*/
    public  void  makePost(String url, final Map<String, String> params, final RequestDoneInterface callback) {

        url = baseUrl + url;
        url = url.trim();
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("dd","vibe:"+response);
                        callback.RequestDone(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.RequestDone(error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {

                params.put("api_name", api_name);
                params.put("api_key", api_key);
                return params;
            }
        };
        requestQueue.add(postRequest);

    }




}
