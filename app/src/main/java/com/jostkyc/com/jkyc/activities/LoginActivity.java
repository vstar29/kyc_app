package com.jostkyc.com.jkyc.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jostkyc.com.jkyc.R;
import com.jostkyc.com.jkyc.others.ConnectivityReceiver;
import com.jostkyc.com.jkyc.others.FireBaseRegService;
import com.jostkyc.com.jkyc.others.Helper;
import com.jostkyc.com.jkyc.others.HttpHandler;
import com.jostkyc.com.jkyc.others.MyApplication;
import com.jostkyc.com.jkyc.others.RequestDoneInterface;
import com.jostkyc.com.jkyc.others.User;
import com.jostkyc.sdk.others.KycListener;
import com.jostkyc.sdk.others.KycUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity  implements ConnectivityReceiver.ConnectivityReceiverListener   {
    private EditText emailView;
    private EditText passwordView;
    private Button btnLogin;
    ProgressBar progressBar;
    Boolean connected=true;
    Snackbar snackbar;
    String email,password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        emailView=(EditText) findViewById(R.id.email);
        passwordView=(EditText) findViewById(R.id.password);
        btnLogin=(Button) findViewById(R.id.login_btn);
        progressBar=(ProgressBar) findViewById(R.id.progressBar);
        connected=checkConnection();

    }


    /*--login method--*/
    public void signIn(View v)
    {
        hideKeyboard();
        progressBar.setVisibility(View.VISIBLE);
        Boolean valid=true;
        View focusView=null;
        email=emailView.getText().toString();
        password=passwordView.getText().toString();
        if(TextUtils.isEmpty(email))
        {
            emailView.setError("Email is required");
            valid=false;
            focusView=emailView;
        }
       else if(!Helper.isValidEmail(email))
        {
            emailView.setError("Invalid Email");
            valid=false;
            focusView=emailView;
        }
        else if(TextUtils.isEmpty(password))
        {
            passwordView.setError("Password is required");
            valid=false;
            focusView=passwordView;
        }
        else if(!Helper.isValidPassword(password))
        {
            passwordView.setError("Minimum of 7 characters");
            valid=false;
            focusView=passwordView;
        }
        else {}
        if(!valid)
        {
            progressBar.setVisibility(View.INVISIBLE);
            focusView.requestFocus();
        }
        else
        {

            Map<String, String> params = new HashMap<String, String>();
            if(connected)
            {
                params.put("email",email);
                params.put("password",password);
                HttpHandler httpHandler =new HttpHandler(this);
                httpHandler.makePost("signin", params, new RequestDoneInterface() {
                    @Override
                    public void RequestDone(String result) {
                        loginDone(result);
                    }
                });

            }
            else
            {
                progressBar.setVisibility(View.INVISIBLE);
                showSnack(false);
            }

        }

    }


    public void loginDone(String result) {

        try{
            JSONObject resp = new JSONObject(result);
            int status=resp.getInt("status");
            if(status==0)
            {
                String error=resp.getString("error");
                emailView.setError(error);
                emailView.requestFocus();
            }
            else if(status==1)
            {
                JSONObject response = resp.getJSONObject("results");
                String name=response.getString("name");
                String units=response.getString("units");
                email =response.getString("email");
                String avatar =response.getString("avatar");
                String a_status =response.getString("a_status");
                String address ="";
                String v_details =response.getString("v_details");
                int vcount=0;
                int id=response.getInt("id");
                String phone=response.getString("phone");
                phone=phone.replace("+","");
                if(v_details.equalsIgnoreCase("0"))
                {
                    String country="Nigeria";
                    String type="KYC";
                    String code=phone.substring(0,3);
                    if(!code.equalsIgnoreCase("234"))
                    {
                        country="Others";
                        type="REG";
                    }

                    KycUser kycUser=new KycUser(email,phone,country,type,"","","");
                    kycUser.setJkycListener(new KycListener() {

                        @Override
                        public void onKError(String error) {
                            //handler error
                            progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(LoginActivity.this,"kyc_error: "+error,Toast.LENGTH_LONG).show();
                        }
                    });
                    kycUser.startKyc(this);
                }
                else
                {
                    User user=new User(id,name,email,phone,a_status,units,vcount,avatar,address,v_details);
                    Helper.saveUser(this,user);
                    Intent intent=new Intent(this,MainActivity.class);
                    startActivity(intent);
                    progressBar.setVisibility(View.INVISIBLE);
                    updateFireBaseID(email);
                }

            }
            progressBar.setVisibility(View.INVISIBLE);

        }catch (JSONException e)
        {
            progressBar.setVisibility(View.INVISIBLE);

            e.printStackTrace();
        }

    }

    @Override
    protected  void onActivityResult(int requestCode,int resultCode,Intent data)
    {
        progressBar.setVisibility(View.INVISIBLE);
        // check that you are responding to JKYC request
        if(requestCode==5592)
        {

            //check may be result is ok
            if(resultCode==RESULT_OK)
            {
                String Status=data.getStringExtra("STATUS");

                if(Status.equalsIgnoreCase("COMPLETED"))
                {

                    signIn();
                    progressBar.setVisibility(View.INVISIBLE);
                }

            }


        }
    }
    /*--login method--*/
    private void signIn()
    {

        Map<String, String> params = new HashMap<String, String>();

        if(connected)
        {
            params.put("email",email);
            params.put("password",password);
            HttpHandler httpHandler =new HttpHandler(this);
            httpHandler.makePost("signin", params, new RequestDoneInterface() {
                @Override
                public void RequestDone(String result) {
                    loginDone(result);
                }
            });

        }


    }



    /*---Save user firebase id to db for push notification--*/
    public void updateFireBaseID(String email)
    {
        if(connected)
        {
            Map<String, String> params = new HashMap<String, String>();
            params.put("email",email);
            String ID = FireBaseRegService.getToken();
            params.put("firebase_id",ID);
            HttpHandler httpHandler =new HttpHandler(this);
            httpHandler.makePost("update_data", params, new RequestDoneInterface() {
                @Override
                public void RequestDone(String result) {
                    //do nothing
                }
            });




        }
    }



    /*--navigate to registration page(also serve as homepage)--*/
    public void goReg(View v )
    {
        Intent intent =new Intent(this,RegisterActivity.class);
        startActivity(intent);
    }



    /*--hide soft keyboard--*/

    public void hideKeyboard(){
    
        ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
            hideSoftInputFromWindow(passwordView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//***Change Here***
        startActivity(intent);
        finish();
        System.exit(0);

        // Otherwise defer to system default behavior.
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();

        MyApplication.getInstance().setConnectivityListener(this);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        connected=isConnected;
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {


        if (!isConnected) {
            final CoordinatorLayout coordinatorLayout=(CoordinatorLayout) findViewById(R.id.main);
            snackbar = Snackbar.make(coordinatorLayout, "No internet connection",Snackbar.LENGTH_LONG).setAction("DISMISS", new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
            snackbar.setActionTextColor(Color.WHITE);
            TextView tv = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.RED);
            snackbar.show();

        }

    }

    public boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
        return isConnected;
    }
    @Override
    public  void onDestroy()
    {
        super.onDestroy();
        try
        {
            unregisterReceiver(new ConnectivityReceiver());
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}

