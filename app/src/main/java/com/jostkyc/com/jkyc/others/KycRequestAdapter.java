package com.jostkyc.com.jkyc.others;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jostkyc.com.jkyc.R;

import java.util.ArrayList;

/**
 * Created by VICTOR on 27-Feb-18.
 */

public class KycRequestAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<KycRequest> kycRequests;
    private LayoutInflater layoutinflater;
    private String type;
    private ItemClickListener clickListener;
    
    public KycRequestAdapter(Context context, ArrayList<KycRequest> kycRequests,String type) {
        this.context = context;
        layoutinflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.kycRequests = kycRequests;
        this.type=type;

    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener  {

        
        TextView email;
        TextView date;
        TextView status;
        LinearLayout parent;

        Holder(View view) {
            super(view);
            
            email=(TextView) view.findViewById(R.id.email);
            date=(TextView) view.findViewById(R.id.date);
            status=(TextView) view.findViewById(R.id.status);
            view.setOnClickListener(this);

        };

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }

        

    }

    public void setClickListener(ItemClickListener itemClickListener) {

        this.clickListener = itemClickListener;
    }


    @Override
    public int getItemCount() {
        return kycRequests.size();
    }




    @Override
    public long getItemId(int position) {
        return position;
    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View relatedModel;

        relatedModel= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.request_model, viewGroup, false);

        if(type.equalsIgnoreCase("invites"))
        {
            relatedModel= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.invites_model, viewGroup, false);
        }

        return new Holder (relatedModel);


    }

   

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final int position_final=position;
        Holder itemHolder=(Holder) holder;
        if(type.equalsIgnoreCase("invites"))
        {
            itemHolder.email.setText(kycRequests.get(position).getSenderEmail());
        }


        if(type.equalsIgnoreCase("request"))
        {
            itemHolder.email.setText(kycRequests.get(position).getEmail());
            if(kycRequests.get(position).getStatus().equalsIgnoreCase("Completed"))
            {
                itemHolder.status.setText("VIEW DETAILS");
                itemHolder.status.setTextColor(Color.GREEN);
            }
            else
            {
                itemHolder.status.setText(kycRequests.get(position).getStatus());
                itemHolder.status.setTextColor(Color.BLACK);
            }

        }

        itemHolder.date.setText(kycRequests.get(position).getDate());
        //setAnimation(((Holder) holder).parent,position);

    }

    private  void setAnimation(LinearLayout view, int position)
    {

        Animation animation= AnimationUtils.loadAnimation(context,android.R.anim.slide_in_left);
        view.startAnimation(animation);

    }
}
