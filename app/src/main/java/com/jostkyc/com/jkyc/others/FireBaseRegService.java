package com.jostkyc.com.jkyc.others;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;


/**
 * Created by VICTOR on 10-Nov-16.
 */
public class FireBaseRegService extends FirebaseInstanceIdService {
    
    @Override
    public void onTokenRefresh() {
        

    }

    public static   String getToken()
    {
        return  FirebaseInstanceId.getInstance().getToken();
    }

    public void subscribedUser(String topic) {

        FirebaseMessaging.getInstance().subscribeToTopic(topic);
    }

    public void unSubscribedUser(String topic) {
        FirebaseMessaging.getInstance().unsubscribeFromTopic(topic);
    }




}
