package com.jostkyc.com.jkyc.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jostkyc.com.jkyc.R;
import com.jostkyc.com.jkyc.others.ConnectivityReceiver;
import com.jostkyc.com.jkyc.others.Helper;
import com.jostkyc.com.jkyc.others.HttpHandler;
import com.jostkyc.com.jkyc.others.MyApplication;
import com.jostkyc.com.jkyc.others.RequestDoneInterface;
import com.jostkyc.com.jkyc.others.User;
import com.jostkyc.sdk.others.JkycSdk;
import com.jostkyc.sdk.others.KycListener;
import com.jostkyc.sdk.others.KycUser;
import com.lamudi.phonefield.PhoneEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {
    private EditText emailView;
    private EditText passwordView;
    private PhoneEditText phoneView;
    private EditText cPasswordView;
    private TextView errorView;
    private Button regBtn;
    ProgressBar progressBar;
    Boolean connected=true;
    Snackbar snackbar;
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;
    String password,email;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        emailView=(EditText) findViewById(R.id.email);
        passwordView=(EditText) findViewById(R.id.password);
        cPasswordView=(EditText) findViewById(R.id.c_password);
        phoneView=(PhoneEditText) findViewById(R.id.phone);
        errorView=(TextView) findViewById(R.id.error);
        regBtn=(Button) findViewById(R.id.reg_btn);
        progressBar=(ProgressBar) findViewById(R.id.progressBar);
        phoneView.setDefaultCountry("NG");
        phoneView.setHint(R.string.phone_number);
        phoneView.setTextColor(Color.WHITE);
        connected=checkConnection();
        progressDialog=new ProgressDialog(this,ProgressDialog.THEME_HOLO_LIGHT);
        progressDialog.setTitle("Signing in..");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        sharedPreferences=this.getSharedPreferences("My_preference",0);
        editor= sharedPreferences.edit();
        JkycSdk.initialize(this);

    }

    public void goSignIn(View v)
    {
        Intent intent=new Intent(this,LoginActivity.class);
        startActivity(intent);
    }

    /*--register method--*/
    public void signUp(View v)
    {
        hideKeyboard();
        errorView.setText("");
        progressBar.setVisibility(View.VISIBLE);
        Boolean valid=true;
        View focusView=null;
        email=emailView.getText().toString();
        password=passwordView.getText().toString();
        String c_password=cPasswordView.getText().toString();
        String phone=phoneView.getPhoneNumber();

        if(TextUtils.isEmpty(email))
        {
            emailView.setError("Email is required");
            valid=false;
            focusView=emailView;
        }
        else if(!Helper.isValidEmail(email))
        {
            emailView.setError("Invalid Email");
            valid=false;
            focusView=emailView;
        }
        else if(TextUtils.isEmpty(password))
        {
            passwordView.setError("Password is required");
            valid=false;
            focusView=passwordView;
        }
        else if(!Helper.isValidPassword(password))
        {
            passwordView.setError("Minimum of 7 characters");
            valid=false;
            focusView=passwordView;
        }
        else if(!password.equalsIgnoreCase(c_password))
        {
            passwordView.setError("Password must match");
            valid=false;
            focusView=passwordView;
        }
       
        else if(!phoneView.isValid())
        {

            phoneView.setError("Invalid phone number");
            valid=false;
            focusView=phoneView;

        }
        if(!valid)
        {
            progressBar.setVisibility(View.INVISIBLE);
            focusView.requestFocus();
        }
        else
        {
            phone=phone.replace("+","");
            if(connected)
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("email",email);
                params.put("password",password);
                params.put("password2",c_password);
                params.put("phone",phone);
                HttpHandler httpHandler =new HttpHandler(this);


                httpHandler.makePost("signup", params, new RequestDoneInterface() {
                    @Override
                    public void RequestDone(String result) {
                        RegisterDone(result);
                    }
                });
            }else
            {
                progressBar.setVisibility(View.INVISIBLE);
                showSnack(false);
            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        MyApplication.getInstance().setConnectivityListener(this);
    }

    /*--called after succesful asyn task--*/

    public void RegisterDone(String result)
    {
        try{

                JSONObject resp = new JSONObject(result);
                int status=resp.getInt("status");
                if(status==0)
                {
                    JSONObject error=resp.getJSONObject("error");
                    if(error.has("email"))
                    {
                        emailView.setError(error.getString("email"));
                        emailView.requestFocus();
                    }
                    if(error.has("password"))
                    {
                        passwordView.setError(error.getString("password"));
                    }
                    if(error.has("phone"))
                    {

                        phoneView.setError(error.getString("password"));
                    }

                }
                else if(status==1)
                {
                    JSONObject response = resp.getJSONObject("results");
                    String email =response.getString("email");
                    String phone=response.getString("phone");
                    phone=phone.replace("+","");
                    String country="Nigeria";
                    String type="KYC";
                    String code=phone.substring(0,3);
                    if(!code.equalsIgnoreCase("234"))
                    {
                        country="Others";
                        type="REG";
                    }


                    KycUser kycUser=new KycUser(email,phone,country,type,"","","");
                    kycUser.setJkycListener(new KycListener() {

                        @Override
                        public void onKError(String error) {
                            //handler error
                            progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(RegisterActivity.this,"kyc_error: "+error,Toast.LENGTH_LONG).show();
                        }
                    });
                    kycUser.startKyc(this);

                }
            progressBar.setVisibility(View.INVISIBLE);
        }catch (JSONException e)
        {
            progressBar.setVisibility(View.INVISIBLE);
            e.printStackTrace();
        }
    }

    /*--login method--*/
    private void signIn()
    {

        Map<String, String> params = new HashMap<String, String>();

        if(connected)
        {
            params.put("email",email);
            params.put("password",password);
            HttpHandler httpHandler =new HttpHandler(this);
            httpHandler.makePost("signin", params, new RequestDoneInterface() {
                @Override
                public void RequestDone(String result) {
                    loginDone(result);
                }
            });

        }


    }

    /*--called after succesfull login--*/
    public void loginDone(String result) {

        try{
            JSONObject resp = new JSONObject(result);
            int status=resp.getInt("status");
            if(status==0)
            {
                String error=resp.getString("error");
                emailView.setError(error);
                emailView.requestFocus();
            }
            else if(status==1)
            {

                JSONObject response = resp.getJSONObject("results");
                String name=response.getString("name");
                String units=response.getString("units");
                email =response.getString("email");
                String avatar =response.getString("avatar");
                String a_status =response.getString("a_status");
                String address ="";
                String v_details =response.getString("v_details");
                int vcount=0;
                int id=response.getInt("id");
                String phone=response.getString("phone");
                User user=new User(id,name,email,phone,a_status,units,vcount,avatar,address,v_details);
                Helper.saveUser(this,user);
                Intent intent=new Intent(this,MainActivity.class);
                startActivity(intent);
            }
            progressDialog.dismiss();
        }catch (JSONException e)
        {

            progressDialog.dismiss();
            e.printStackTrace();
        }

    }

    /*--hide keyboard--*/
    public void hideKeyboard() {

    ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
        hideSoftInputFromWindow(passwordView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//***Change Here***
        startActivity(intent);
        finish();
        System.exit(0);

        // Otherwise defer to system default behavior.
        super.onBackPressed();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        connected=isConnected;
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {


        if (!isConnected) {
            final CoordinatorLayout coordinatorLayout=(CoordinatorLayout) findViewById(R.id.main);
            snackbar = Snackbar.make(coordinatorLayout, "No internet connection",Snackbar.LENGTH_LONG).setAction("DISMISS", new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
            snackbar.setActionTextColor(Color.WHITE);
            TextView tv = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.RED);
            snackbar.show();

        }

    }

    @Override
    protected  void onActivityResult(int requestCode,int resultCode,Intent data)
    {
        progressBar.setVisibility(View.INVISIBLE);
        // check that you are responding to JKYC request
        if(requestCode==5592)
        {

            //check may be result is ok
            if(resultCode==RESULT_OK)
            {
                String Status=data.getStringExtra("STATUS");

                if(Status.equalsIgnoreCase("COMPLETED"))
                {
                    progressDialog.show();
                    signIn();
                }

            }


        }
    }

    public boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
        return isConnected;
    }
    @Override
    public  void onDestroy()
    {

        super.onDestroy();
        try
        {
            unregisterReceiver(new ConnectivityReceiver());
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    
}
