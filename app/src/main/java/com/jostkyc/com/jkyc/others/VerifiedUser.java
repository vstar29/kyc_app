package com.jostkyc.com.jkyc.others;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by VICTOR on 27-Feb-18.
 */

public class VerifiedUser implements Parcelable {
    private int  id;
    private String email;
    private String request_email;
    @SerializedName("request_status")
    private String status;
    private String request_data;
    @SerializedName("request_date")
    private String date;
    private String request_id;
    private String request_websites;

    public VerifiedUser(int id, String email, String request_email, String status, String request_data, String date, String request_id, String request_websites)
    {
        this.id=id;
        this.email=email;
        this.status=status;
        this.request_email=request_email;
        this.request_data=request_data;
        this.date=date;
        this.request_id=request_id;
        this.request_websites=request_websites;

    }



    public VerifiedUser(Parcel in)
    {
        id=in.readInt();
        email=in.readString();
        status=in.readString();
        request_email=in.readString();
        request_data=in.readString();
        date=in.readString();
        request_id=in.readString();
        request_websites=in.readString();

    }

    @Override
    public void writeToParcel(Parcel out, int flags)
    {
        out.writeInt(id);
        out.writeString(email);
        out.writeString(status);
        out.writeString(request_email);
        out.writeString(request_data);
        out.writeString(date);
        out.writeString(request_id);
        out.writeString(request_websites);

    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VerifiedUser> CREATOR
            = new Creator<VerifiedUser>() {
        // @Override
        public VerifiedUser createFromParcel(Parcel in) {
            return new VerifiedUser(in);
        }

        // @Override
        public VerifiedUser[] newArray(int size) {
            return new VerifiedUser[size];
        }
    };

    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getStatus() {
        return status;
    }

    public String getRequest_email() {
        return request_email;
    }

    public String getRequest_details() {
        return request_data;
    }

    public String getDate() {
        int sec=Integer.parseInt(this.date);
        Date date =new Date(sec*1000L);
        java.text.DateFormat df=new SimpleDateFormat("MMM dd, yyyy hh:mm",Locale.ENGLISH);
        return df.format(date);

    }

    public String getRequest_id() {
        return request_id;
    }

    public String getName()
    {
        String name="";
        try{
            JSONObject details=new JSONObject(this.request_data);
            if(details.has("BANK"))
            {
                JSONObject Bank=details.getJSONObject("BANK");
                name=Bank.getString("accountName");

            }
           else if(details.has("LICENSE"))
            {
                JSONObject License=details.getJSONObject("LICENSE");
                name=License.getString("FIRSTNAME")+ " "+License.getString("SURNAME");
            }
            else
            {
                name=details.getString("FIRSTNAME")+" "+ details.getString("LASTNAME");
            }
        }catch (JSONException e)
        {
            e.printStackTrace();
        }
       if(!TextUtils.isEmpty(name))
        {
            try{
                String[] names=name.split(" ");
                String real_name="";
                for (int i=0; i<=names.length;i++){

                    String n=names[i];
                    real_name=real_name+" "+n.substring(0,1).toUpperCase()+n.substring(1).toLowerCase();
                }
                return real_name;
            }catch (Exception e){
                return name;
            }

        }
        return name;


    }

    public String getPhotoCapture()
    {
        String photocapture="";
        try{
            JSONObject details=new JSONObject(this.request_data);
            if(details.has("PHOTOCAPTURE"))
            {
                photocapture="https://jostkyc.com/"+details.getString("PHOTOCAPTURE");
            }
        }catch (JSONException e)
        {
            e.printStackTrace();
        }

        return photocapture;

    }


    public String getIDCapture()
    {
        String idcapture="";
        try{
            JSONObject details=new JSONObject(this.request_data);
            if(details.has("IDCAPTURE"))
            {
                idcapture="https://jostkyc.com/"+details.getString("IDCAPTURE");
            }
        }catch (JSONException e)
        {
            e.printStackTrace();
        }

        return idcapture;

    }

    public String getPhone()
    {
        String phone="";
        try{
            JSONObject details=new JSONObject(this.request_data);
            if(details.has("PHONE"))
            {
                phone= details.getString("PHONE");
            }
        }catch (JSONException e)
        {
            e.printStackTrace();
        }

        return phone;
    }

    public String getMethod()
    {
        String method="";
        try{
            JSONObject details=new JSONObject(this.request_data);
            if(details.has("BANK"))
            {
                method="BANK";
            }
            else if(details.has("LICENSE"))
            {
                method="LICENSE";
            }
            
        }catch (JSONException e)
        {
            e.printStackTrace();
        }
       
        return method;


    }

    public String getMethodLabel(String method)
    {

        if(method.equalsIgnoreCase("BANK"))
        {
            return "Account No";
        }
        else if(method.equalsIgnoreCase("LICENSE"))
        {
            return "License No";
        }
        return "";
    }

    public String getMethodValue(String method)
    {
        String value="";
        try {
            JSONObject details = new JSONObject(this.request_data);

            if (method.equalsIgnoreCase("BANK")) {
                JSONObject Bank = details.getJSONObject("BANK");
                value = Bank.getString("accountNumber")+" "+"("+Bank.getString("bankName")+")";
            } else if (method.equalsIgnoreCase("LICENSE")) {
                JSONObject License = details.getJSONObject("LICENSE");
                value = License.getString("LICENSENUMBER");
            }
        }
        catch(JSONException e)
        {
            e.printStackTrace();
        }

        return value;
    }


}

