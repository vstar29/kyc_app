package com.jostkyc.com.jkyc.others;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jostkyc.com.jkyc.R;

/**
 * Created by VICTOR on 25-Feb-18.
 */

public class DashInfoAdapter extends BaseAdapter {

    private Context context;
    LayoutInflater layoutInflater;
    private String[] names;
    private User user;

    public  DashInfoAdapter(Context context, User user)
    {
        this.context=context;
        layoutInflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.user=user;
        Resources resource= context.getResources();
        this.names=resource.getStringArray(R.array.dash_info);
    }

    @Override
    public int getCount() {
        return names.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        DashInfoAdapter.ViewHolder listViewHolder;
        if(convertView == null){

            convertView = layoutInflater.inflate(R.layout.dash_info_model,parent,false);
            listViewHolder = new DashInfoAdapter.ViewHolder();
            listViewHolder.title = (TextView)convertView.findViewById(R.id.title);
            listViewHolder.value=(TextView) convertView.findViewById(R.id.value);

            convertView.setTag(listViewHolder);
        }else
        {
            listViewHolder = (DashInfoAdapter.ViewHolder)convertView.getTag();
        }
        String name=names[position];
        listViewHolder.title.setText(name);
        switch (position)
        {
            case 0:
                listViewHolder.value.setText(user.status);
                break;
            case 1:
                listViewHolder.value.setText(user.units);
                break;
            case 2:
                listViewHolder.value.setText(user.getV_count());
                break;

        }

        return convertView;
    }

    static class ViewHolder{

        TextView title;
        TextView value;

    }

}
