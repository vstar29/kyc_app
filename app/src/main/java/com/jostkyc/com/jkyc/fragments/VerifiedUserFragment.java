package com.jostkyc.com.jkyc.fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.jostkyc.com.jkyc.R;
import com.jostkyc.com.jkyc.activities.VerifiedUsersActivity;
import com.jostkyc.com.jkyc.others.VerifiedUser;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link VerifiedUserFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link VerifiedUserFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VerifiedUserFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private VerifiedUser verifiedUser;
    private TextView name;
    private TextView email;
    private TextView status;
    private TextView phone;
    private TextView date;
    private TextView method;
    private TextView method_value_label,methodTitle;
    private TextView method_value;
    private ImageButton photocapture;
    private ImageButton idcapture;
    private LinearLayout toHide1;
    private LinearLayout toHide2;
    private Toolbar toolbar;
    private Animator mCurrentAnimator;
    private int mShortAnimationDuration;
    private OnFragmentInteractionListener mListener;
    private  View fragmentView;

    public VerifiedUserFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment VerifiedUserFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static VerifiedUserFragment newInstance(String param1, String param2) {
        VerifiedUserFragment fragment = new VerifiedUserFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         fragmentView= inflater.inflate(R.layout.fragment_verified_user, container, false);
        toolbar = (Toolbar) fragmentView.findViewById(R.id.my_toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        //toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
        final android.support.v7.app.ActionBar actionBar =    ((AppCompatActivity) getActivity()).getSupportActionBar();
        toolbar.setTitleTextColor(Color.WHITE);
        //actionBar.setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), VerifiedUsersActivity.class));
            }
        });

        Bundle bundle=getArguments();
        verifiedUser=bundle.getParcelable("verifiedUser");
        name=(TextView) fragmentView.findViewById(R.id.name);
        email=(TextView) fragmentView.findViewById(R.id.email);
        methodTitle=(TextView) fragmentView.findViewById(R.id.method_title);
        status=(TextView) fragmentView.findViewById(R.id.status);
        phone=(TextView) fragmentView.findViewById(R.id.phone);
        date=(TextView) fragmentView.findViewById(R.id.date);
        method=(TextView) fragmentView.findViewById(R.id.method);
        method_value=(TextView) fragmentView.findViewById(R.id.method_value);
        method_value_label=(TextView) fragmentView.findViewById(R.id.method_value_label);
        photocapture=(ImageButton) fragmentView.findViewById(R.id.photocapture);
        idcapture=(ImageButton) fragmentView.findViewById(R.id.idcapture);

        toHide1=(LinearLayout) fragmentView.findViewById(R.id.hide1);
        toHide2=(LinearLayout) fragmentView.findViewById(R.id.hide2);
        name.setText(verifiedUser.getName());
        email.setText(verifiedUser.getRequest_email());
        status.setText(verifiedUser.getStatus());
        phone.setText(verifiedUser.getPhone());
        date.setText(verifiedUser.getDate());
        toolbar.setTitle(verifiedUser.getName());
        //actionBar.setTitle(verifiedUser.getName());
        final String photo_capture_url=verifiedUser.getPhotoCapture();
        final String id_capture_url=verifiedUser.getIDCapture();
        Glide.with(getContext()).load(photo_capture_url) .into(photocapture);
        Glide.with(getContext()).load(id_capture_url).into(idcapture);
        String method_s=verifiedUser.getMethod();
        method.setText(method_s);
        if(TextUtils.isEmpty(method_s))
        {
            toHide1.setVisibility(View.INVISIBLE);
            toHide2.setVisibility(View.INVISIBLE);
            methodTitle.setVisibility(View.INVISIBLE);
        }
        method_value.setText(verifiedUser.getMethodValue(method_s));
        method_value_label.setText(verifiedUser.getMethodLabel(method_s));
        photocapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                zoomImageFromThumb(photocapture, photo_capture_url);
            }
        });
        idcapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                zoomImageFromThumb(idcapture, id_capture_url);
            }
        });
        return fragmentView;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity;
        if(context instanceof  Activity)
        {
            activity=(Activity) context;
        }
        else
        {
            throw new  RuntimeException(context.toString()+"must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void zoomImageFromThumb(final View thumbView, String url) {

        if (mCurrentAnimator != null) {
            mCurrentAnimator.cancel();
        }

        // Load the high-resolution "zoomed-in" image.
        final ImageView expandedImageView = (ImageView) fragmentView.findViewById(
                R.id.expanded_image);
        Glide.with(getContext()).load(url) .into(expandedImageView);

        // Calculate the starting and ending bounds for the zoomed-in image.
        // This step involves lots of math. Yay, math.
        final Rect startBounds = new Rect();
        final Rect finalBounds = new Rect();
        final Point globalOffset = new Point();

        // The start bounds are the global visible rectangle of the thumbnail,
        // and the final bounds are the global visible rectangle of the container
        // view. Also set the container view's offset as the origin for the
        // bounds, since that's the origin for the positioning animation
        // properties (X, Y).
        thumbView.getGlobalVisibleRect(startBounds);
        fragmentView.findViewById(R.id.container)
                .getGlobalVisibleRect(finalBounds, globalOffset);
        startBounds.offset(-globalOffset.x, -globalOffset.y);
        finalBounds.offset(-globalOffset.x, -globalOffset.y);

        // Adjust the start bounds to be the same aspect ratio as the final
        // bounds using the "center crop" technique. This prevents undesirable
        // stretching during the animation. Also calculate the start scaling
        // factor (the end scaling factor is always 1.0).
        float startScale;
        if ((float) finalBounds.width() / finalBounds.height()
                > (float) startBounds.width() / startBounds.height()) {
            // Extend start bounds horizontally
            startScale = (float) startBounds.height() / finalBounds.height();
            float startWidth = startScale * finalBounds.width();
            float deltaWidth = (startWidth - startBounds.width()) / 2;
            startBounds.left -= deltaWidth;
            startBounds.right += deltaWidth;
        } else {
            // Extend start bounds vertically
            startScale = (float) startBounds.width() / finalBounds.width();
            float startHeight = startScale * finalBounds.height();
            float deltaHeight = (startHeight - startBounds.height()) / 2;
            startBounds.top -= deltaHeight;
            startBounds.bottom += deltaHeight;
        }

        // Hide the thumbnail and show the zoomed-in view. When the animation
        // begins, it will position the zoomed-in view in the place of the
        // thumbnail.
        thumbView.setAlpha(0f);
        expandedImageView.setVisibility(View.VISIBLE);

        // Set the pivot point for SCALE_X and SCALE_Y transformations
        // to the top-left corner of the zoomed-in view (the default
        // is the center of the view).
        expandedImageView.setPivotX(0f);
        expandedImageView.setPivotY(0f);

        // Construct and run the parallel animation of the four translation and
        // scale properties (X, Y, SCALE_X, and SCALE_Y).
        AnimatorSet set = new AnimatorSet();
        set
                .play(ObjectAnimator.ofFloat(expandedImageView, View.X,
                        startBounds.left, finalBounds.left))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
                        startBounds.top, finalBounds.top))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
                        startScale, 1f))
                .with(ObjectAnimator.ofFloat(expandedImageView,
                        View.SCALE_Y, startScale, 1f));
        set.setDuration(mShortAnimationDuration);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mCurrentAnimator = null;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                mCurrentAnimator = null;
            }
        });
        set.start();
        mCurrentAnimator = set;

        // Upon clicking the zoomed-in image, it should zoom back down
        // to the original bounds and show the thumbnail instead of
        // the expanded image.
        final float startScaleFinal = startScale;
        expandedImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCurrentAnimator != null) {
                    mCurrentAnimator.cancel();
                }

                // Animate the four positioning/sizing properties in parallel,
                // back to their original values.
                AnimatorSet set = new AnimatorSet();
                set.play(ObjectAnimator
                        .ofFloat(expandedImageView, View.X, startBounds.left))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.Y,startBounds.top))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.SCALE_X, startScaleFinal))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.SCALE_Y, startScaleFinal));
                set.setDuration(mShortAnimationDuration);
                set.setInterpolator(new DecelerateInterpolator());
                set.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        thumbView.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        mCurrentAnimator = null;
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        thumbView.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        mCurrentAnimator = null;
                    }
                });
                set.start();
                mCurrentAnimator = set;
            }
        });
    }




}
