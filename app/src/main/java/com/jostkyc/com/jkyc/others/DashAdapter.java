package com.jostkyc.com.jkyc.others;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.jostkyc.com.jkyc.R;

/**
 * Created by JUMIA-1888 on 2/19/2018.
 */

public class DashAdapter extends BaseAdapter {

   private TypedArray icons;
   private Context context;
   LayoutInflater layoutInflater;
   private String[] name;

   public  DashAdapter(Context context)
   {
       this.context=context;
       layoutInflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
       this.icons=context.getResources().obtainTypedArray(R.array.dash_icon);
       Resources resource= context.getResources();
       this.name=resource.getStringArray(R.array.dash_item);
   }

    @Override
    public int getCount() {
        return name.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder listViewHolder;
        if(convertView == null){

            convertView = layoutInflater.inflate(R.layout.dashboard_model,parent,false);
            listViewHolder = new ViewHolder();
            listViewHolder.title = (TextView)convertView.findViewById(R.id.title);
            listViewHolder.icon=(ImageView) convertView.findViewById(R.id.icon);

            convertView.setTag(listViewHolder);
        }else
        {
            listViewHolder = (ViewHolder)convertView.getTag();
        }

        listViewHolder.title.setText(name[position]);
        listViewHolder.icon.setImageResource(icons.getResourceId(position,R.drawable.ic_request));
        return convertView;
    }

    static class ViewHolder{

        ImageView icon;
        TextView title;

    }

}
