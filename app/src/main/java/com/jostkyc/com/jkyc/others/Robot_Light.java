package com.jostkyc.com.jkyc.others;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by PC-i3 on 6/16/2017.
 */

public class Robot_Light extends TextView {
    public Robot_Light(Context context) {
        super(context);
        init();
    }

    public Robot_Light(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Robot_Light(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Light.ttf");
            setTypeface(tf);
        }
    }
}
