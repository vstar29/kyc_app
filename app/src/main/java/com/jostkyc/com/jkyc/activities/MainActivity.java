package com.jostkyc.com.jkyc.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.jostkyc.com.jkyc.R;
import com.jostkyc.com.jkyc.others.ConnectivityReceiver;
import com.jostkyc.com.jkyc.others.DashAdapter;
import com.jostkyc.com.jkyc.others.DashInfoAdapter;
import com.jostkyc.com.jkyc.others.HeaderGridView;
import com.jostkyc.com.jkyc.others.Helper;
import com.jostkyc.com.jkyc.others.MyApplication;
import com.jostkyc.com.jkyc.others.User;
import com.jostkyc.com.jkyc.others.VerifiedUser;
import com.jostkyc.sdk.fragments.EmailFragment;
import com.jostkyc.sdk.fragments.NameFragment;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {
Toolbar toolbar;
GridView gridView;
DashAdapter dashAdapter;
TextView name,address;
ImageView avatar;
TextView statusView;
User user;
Snackbar snackbar;
Boolean connected=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        gridView=(GridView) findViewById(R.id.gridView);
        statusView=(TextView) findViewById(R.id.status);
        setSupportActionBar(toolbar);
        dashAdapter=new DashAdapter(this);
        avatar=(ImageView) findViewById(R.id.avatar);
        toolbar.setTitle(" ");
        toolbar.setTitleTextColor(Color.WHITE);
        user= Helper.getUser(this);
        if(user==null)
        {
            logOut();
        }

        Glide.with(this).load(user.getAvatar()) .diskCacheStrategy(DiskCacheStrategy.ALL).into(avatar);
        VerifiedUser verifiedUser=user.getV_details();
        if(!verifiedUser.getStatus().equalsIgnoreCase("Completed"))
        {
            statusView.setText(verifiedUser.getStatus());
            statusView.setTextColor(Color.RED);
        }
        gridView.setAdapter(dashAdapter);
        connected=checkConnection();
        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(" ");
        //toolbar.setLogo(R.drawable.jostkyc_white_35);


        EmailFragment emailFragment=new EmailFragment();

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                switch (i)
                {
                    case 0:
                        Intent intent= new Intent(MainActivity.this,KycRequestActivity.class);
                        startActivity(intent);
                        break;
                    case 1:
                        Intent intent1= new Intent(MainActivity.this,KycInvitesActivity.class);
                        startActivity(intent1);
                        break;
                    case 2:
                        Intent intent3= new Intent(MainActivity.this,RequestsListActivity.class);
                        startActivity(intent3);
                        break;

                    case 3:
                        logOut();
                        break;

                }
            }
        });


        if (getIntent().hasExtra("notifyCallBack") && getIntent().hasExtra("type")) {
            String type=getIntent().getStringExtra("type");
            String data=getIntent().getStringExtra("notifyCallBack");
            if(type.equalsIgnoreCase("DONE"))
            {
                Gson gson=new Gson();
                VerifiedUser user=gson.fromJson(data, VerifiedUser.class);
                ArrayList<VerifiedUser> verifiedUsers=new ArrayList<>();
                verifiedUsers.add(0,user);
                Intent intent=new Intent(this,VerifiedUserActivity.class);
                intent.putParcelableArrayListExtra("verifiedUsers",verifiedUsers);
                startActivity(intent);
            }
            else if(type.equalsIgnoreCase("RECEIVED"))
            {
                Intent intent = new Intent(this, DeepLinkActivity.class);
                intent.putExtra("id", data);
                startActivity(intent);

            }

        }

    }

    public void goProfile(View v)
    {
        VerifiedUser verifiedUser=user.getV_details();
        if(verifiedUser.getStatus().equalsIgnoreCase("Completed"))
        {
            ArrayList<VerifiedUser> verifiedUsers=new ArrayList<>();
            verifiedUsers.add(0,verifiedUser);
            Intent intent=new Intent(this,VerifiedUserActivity.class);
            intent.putParcelableArrayListExtra("verifiedUsers",verifiedUsers);
            startActivity(intent);
        }
        else
        {
            Intent intent=new Intent(this,SuspendedActivity.class);
            intent.putExtra("verifiedUsers",verifiedUser);
            startActivity(intent);
        }
    }
    public void logOut()
    {

        Helper.logoutUser(this);
        Intent intent =new Intent(this,LoginActivity.class);
        startActivity(intent);
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//***Change Here***
        startActivity(intent);
        finish();
        System.exit(0);

        // Otherwise defer to system default behavior.
        super.onBackPressed();
    }



    @Override
    protected void onResume() {
        super.onResume();

        MyApplication.getInstance().setConnectivityListener(this);
    }

    public boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
        return isConnected;
    }
    @Override
    public  void onDestroy()
    {
        super.onDestroy();
        try
        {
            unregisterReceiver(new ConnectivityReceiver());
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        connected=isConnected;
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {


        if (!isConnected) {
            final CoordinatorLayout coordinatorLayout=(CoordinatorLayout) findViewById(R.id.main);
            snackbar = Snackbar.make(coordinatorLayout, "No internet connection",Snackbar.LENGTH_LONG).setAction("DISMISS", new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
            snackbar.setActionTextColor(Color.WHITE);
            TextView tv = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.RED);
            snackbar.show();

        }

    }

}
