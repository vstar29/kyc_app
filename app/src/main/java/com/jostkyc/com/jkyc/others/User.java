package com.jostkyc.com.jkyc.others;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by JUMIA-1888 on 2/20/2018.
 */

public class User {

    public int id;
    public String name;
    public String email;
    public String phone;
    @SerializedName("a_status")
    public String status;
    public String units;
    public int v_count;
    public String avatar;
    public String address;
    public String v_details;
    public User(int id, String name, String email, String phone,String status,String units,int v_count,String avatar,String address,String v_details)
    {
        this.id=id;
        this.name=name;
        this.email=email;
        this.phone=phone;
        this.status=status;
        this.units=units;
        this.v_count=v_count;
        this.avatar=avatar;
        this.address=address;
        this.v_details=v_details;
    }

    public int getId()
    {
        return  this.id;
    }

    public String getName()
    {
        VerifiedUser user=this.getV_details();
        return  user.getName();


    }
    public  String getEmail()
    {
        return  this.email;
    }
    public String getPhone()
    {
        return this.phone;
    }
    public String getStatus()
    {
        return this.status;
    }
    public String getUnits() {
        return units;
    }
    public String getV_count() {
        return Integer.toString(v_count);
    }

    public String getAvatar()
    {
        VerifiedUser user=this.getV_details();
        return  user.getPhotoCapture();


    }
    public String getAddress() {
        return address;
    }
    public VerifiedUser getV_details() {

        Gson gson=new Gson();
        VerifiedUser user=gson.fromJson(v_details, VerifiedUser.class);
        return user;
    }
}
