package com.jostkyc.com.jkyc.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.jostkyc.com.jkyc.R;
import com.jostkyc.com.jkyc.others.Helper;
import com.jostkyc.com.jkyc.others.User;

public class SplashActivity extends AppCompatActivity {

    SplashActivity splashActivity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_spalsh);
        Thread thread=new Thread()
        {
            public void run()
            {
                try
                {
                    sleep(900);
                }catch (Exception e)
                {

                }
                finally {
                    User user= Helper.getUser(SplashActivity.this);
                    if(user!=null)
                    {
                        Intent intent=new Intent(SplashActivity.this,MainActivity.class);
                        startActivity(intent);
                    }
                    else {
                        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }

                }
            }
        };
        thread.start();


    }


}