package com.jostkyc.com.jkyc.activities;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.jostkyc.com.jkyc.R;
import com.jostkyc.com.jkyc.others.VerifiedUser;

public class SuspendedActivity extends AppCompatActivity {
    Toolbar toolbar;
    TextView email;
    TextView phone;
    VerifiedUser user;
    android.support.v7.app.ActionBar actionBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suspended);
        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setTitle("ACCOUNT SUSPENDED");
        user=getIntent().getParcelableExtra("verifiedUsers");
        toolbar.setTitleTextColor(Color.WHITE);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_left);
        email=(TextView) findViewById(R.id.email);
        phone=(TextView) findViewById(R.id.phone);
        email.setText("You MUST send your ID with your registered email which is "+user.getRequest_email());
        phone.setText("After sending your ID, You will send an sms with your registered phone number which is "+user.getPhone());
    }
}
