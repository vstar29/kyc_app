package com.jostkyc.com.jkyc.others;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;

import com.jostkyc.com.jkyc.fragments.VerifiedUserFragment;

import java.util.ArrayList;

public class VerifiedUsersFragmentAdapter extends FragmentPagerAdapter {


    private Context context;
    private LayoutInflater layoutinflater;
    private ArrayList<VerifiedUser> verifiedUsers;


    public VerifiedUsersFragmentAdapter(Context context, FragmentManager fm, ArrayList<VerifiedUser> verifiedUsers) {
        super(fm);
        this.verifiedUsers = verifiedUsers;
        this.context=context;

    }


    @Override
    public int getCount() {
        return verifiedUsers.size();
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = new VerifiedUserFragment();
        Bundle bundle = new Bundle();
        VerifiedUser  user=verifiedUsers.get(position);
        bundle.putParcelable("verifiedUser", user);
        fragment.setArguments(bundle);
        return fragment;

    }

    @Override
    public long getItemId(int position) {
        return position;
    }


}