package com.jostkyc.com.jkyc.others;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by JUMIA-1888 on 3/2/2018.
 */

public class KycRequest {

    private int  id;
    private String email;
    @SerializedName("time")
    private String date;
    private String status;
    private String sender_email;

    public KycRequest(int id,String email, String date, String sender_email,String status)
    {
        this.id=id;
        this.email=email;
        this.date=date;
        this.status=status;
        this.sender_email=sender_email;
    }


    public String getEmail()
    {
        return email;
    }

    public int getId() {
        return id;
    }

    public String getStatus() {
       if(status.equalsIgnoreCase("1"))
       {
           return "Completed";
       }
        return "Pending";
    }
    public String getSenderEmail()
    {
        return sender_email;
    }



    public String getDate() {
        int sec=Integer.parseInt(this.date);
        Date date =new Date(sec*1000L);
        java.text.DateFormat df=new SimpleDateFormat("MMM dd, yyyy ",Locale.ENGLISH);
        return df.format(date);

    }


}


