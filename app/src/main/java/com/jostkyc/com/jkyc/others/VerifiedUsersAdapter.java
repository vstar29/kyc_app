package com.jostkyc.com.jkyc.others;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.jostkyc.com.jkyc.R;

import java.util.ArrayList;

/**
 * Created by VICTOR on 27-Feb-18.
 */

public class VerifiedUsersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<VerifiedUser> verifiedUserses;
    private LayoutInflater layoutinflater;
    private ItemClickListener clickListener;



    public VerifiedUsersAdapter(Context context, ArrayList<VerifiedUser> verifiedUserses) {
        this.context = context;
        layoutinflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.verifiedUserses = verifiedUserses;

    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView photocapture;
        TextView email;
        TextView phone;
        TextView name;
        TextView status;
        LinearLayout parent;

        Holder(View view) {
            super(view);
            name=(TextView) view.findViewById(R.id.name);
            email=(TextView) view.findViewById(R.id.email);
            phone=(TextView) view.findViewById(R.id.phone);
            status=(TextView) view.findViewById(R.id.status);
            parent=(LinearLayout) view.findViewById(R.id.parent);
            photocapture=(ImageView) view.findViewById(R.id.photocapture);
            view.setOnClickListener(this);
        };

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }

    }


    @Override
    public int getItemCount() {
        return verifiedUserses.size();
    }




    @Override
    public long getItemId(int position) {
        return position;
    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View relatedModel = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.verified_user_model, viewGroup, false);
        return new Holder (relatedModel);


    }

    public void setClickListener(ItemClickListener itemClickListener) {

        this.clickListener = itemClickListener;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        final int position_final=position;
        Holder itemHolder=(Holder) holder;
        itemHolder.name.setText(verifiedUserses.get(position).getName());
        itemHolder.email.setText(verifiedUserses.get(position).getRequest_email());
        itemHolder.status.setText(verifiedUserses.get(position).getStatus());
        itemHolder.phone.setText(verifiedUserses.get(position).getPhone());
        Glide.with(context).load(verifiedUserses.get(position).getPhotoCapture()).diskCacheStrategy(DiskCacheStrategy.ALL ).into(itemHolder.photocapture);



        setAnimation(((Holder) holder).parent,position);



    }

    private  void setAnimation(LinearLayout view, int position)
    {

        Animation animation= AnimationUtils.loadAnimation(context,android.R.anim.slide_in_left);
        view.startAnimation(animation);

    }
}
