package com.jostkyc.com.jkyc.activities;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.widget.CompoundButton;

import com.jostkyc.com.jkyc.R;

public class SettingsActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private SwitchCompat switchCompat;
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        switchCompat= (SwitchCompat) findViewById(R.id.notificationSwitch);
        sharedPreferences=this.getSharedPreferences("My_preference",0);
        editor= sharedPreferences.edit();
        setSupportActionBar(toolbar);
        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Settings");
        toolbar.setTitleTextColor(Color.WHITE);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_left);
        if(sharedPreferences.getBoolean("pushNotification",true))
        {
            switchCompat.setChecked(true);
        }
        else
        {
            switchCompat.setChecked(false);
        }

        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {

                    editor.putBoolean("pushNotification",true);


                }else
                {

                    editor.putBoolean("pushNotification",false);
                }
                editor.commit();
            }
        });
    }
}
