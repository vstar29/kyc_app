package com.jostkyc.com.jkyc.others;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;

/**
 * Created by JUMIA-1888 on 2/20/2018.
 */

public  class Helper {



    /*-@param email{String}
     "response {boolean}--*/
    public static boolean isValidEmail(String email)
    {
        if(TextUtils.isEmpty(email))
        {
            return  false;
        }
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();

    }

    /*-@param phone{String}
     "response {boolean}--*/
    public static boolean isValidPhoneNumber(String phone)
    {

        if (phone.matches("[0-9]+") && phone.length() > 11 && phone.length()<16) {
            return true;
        }
        return  false;

    }
    /*-@param password{String}
     "response {boolean}--*/
    public static boolean isValidPassword(String password)
    {
        if(TextUtils.isEmpty(password))
        {
            return  false;
        }
        if(password.length()<7)
        {
            return  false;
        }
        return  true;

    }


    /*--save user data to preference--*/
    public  static void saveUser(Context context, User user)
    {
        Gson gson=new Gson();
        SharedPreferences  appSharedPref= context.getSharedPreferences("My_Preference", 0);
        SharedPreferences.Editor editor=appSharedPref.edit();
        String userDetails=gson.toJson(user,User.class);
        editor.putString("user", userDetails);
        editor.commit();

    }

    public static void logoutUser(Context context)
    {
        SharedPreferences appSharedPref= context.getSharedPreferences("My_Preference", 0);
        SharedPreferences.Editor editor=appSharedPref.edit();
        editor.remove("user");
        editor.commit();
    }


    /*--get user data from preference--*/
    public static User getUser(Context context)
    {
        Gson gson=new Gson();
        SharedPreferences appSharedPref= context.getSharedPreferences("My_Preference", 0);
        String userString=appSharedPref.getString("user", "");
        Log.d("dd","vibe: "+userString);
        if(userString!="")
        {
            User user=gson.fromJson(userString, User.class);
            return user;
        }
        else
        {
            return null;
        }


    }



}

