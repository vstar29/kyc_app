package com.jostkyc.com.jkyc.others;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by PC-i3 on 6/16/2017.
 */

public class Robot_Bold extends TextView {
    public Robot_Bold(Context context) {
        super(context);
        init();
    }

    public Robot_Bold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Robot_Bold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Bold.ttf");
            setTypeface(tf);
        }
    }
}
