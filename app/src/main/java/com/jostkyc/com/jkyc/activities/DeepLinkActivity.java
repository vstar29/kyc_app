package com.jostkyc.com.jkyc.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jostkyc.com.jkyc.R;
import com.jostkyc.com.jkyc.others.ConnectivityReceiver;
import com.jostkyc.com.jkyc.others.HttpHandler;
import com.jostkyc.com.jkyc.others.RequestDoneInterface;
import com.jostkyc.com.jkyc.others.VerifiedUser;
import com.jostkyc.sdk.others.JkycSdk;
import com.jostkyc.sdk.others.KycListener;
import com.jostkyc.sdk.others.KycUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class DeepLinkActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {

    String id;
    ProgressDialog progressDialog;
    Boolean connected=true;
    Snackbar snackbar;
    RelativeLayout resultView;
    RelativeLayout suspendedView;
    Toolbar toolbar;
    android.support.v7.app.ActionBar actionBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deep_link);
        progressDialog =new ProgressDialog(this,ProgressDialog.THEME_DEVICE_DEFAULT_LIGHT);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setTitle("Starting KYC");
        progressDialog.show();
        connected=checkConnection();
        resultView=(RelativeLayout) findViewById(R.id.result_view) ;
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setTitle("Jostkyc");
        toolbar.setTitleTextColor(Color.BLACK);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.icon_close);
        suspendedView=(RelativeLayout) findViewById(R.id.suspended);
        //PICK request id and start KYC
        onNewIntent(getIntent());

    }

    protected void onNewIntent(Intent intent) {

        String action = intent.getAction();
        String data = intent.getDataString();

        if ((Intent.ACTION_VIEW.equals(action) && data != null)) {
            String link = data;
            try
            {

                String[] splits = link.split("/");
                id = splits[splits.length - 1];
                if(!TextUtils.isEmpty(id))
                {
                    verifyRequestID(id);

                }
                else
                {
                    Intent intent1=new Intent(this,RegisterActivity.class);
                    startActivity(intent);
                }


            }catch (Exception e)
            {
                progressDialog.dismiss();
                Intent intent1=new Intent(DeepLinkActivity.this,RegisterActivity.class);
                startActivity(intent1);
            }


        }
        else
        {

            if (getIntent().hasExtra("id")) {
                id=getIntent().getStringExtra("id");
                verifyRequestID(id);

            }

        }
    }

    public  void verifyRequestID(String id)
    {
        Map<String, String> params = new HashMap<String, String>();
        if(connected)
        {
            params.put("id",id);
            HttpHandler httpHandler =new HttpHandler(this);
            httpHandler.makePost("verify_request", params, new RequestDoneInterface() {
                @Override
                public void RequestDone(String result) {
                    try
                    {
                        JSONObject resp = new JSONObject(result);
                        int status = resp.getInt("status");
                        if (status == 0) {

                            Intent intent=new Intent(DeepLinkActivity.this,RegisterActivity.class);
                            startActivity(intent);
                        } else if (status == 1) {
                            JSONObject response = resp.getJSONObject("results");
                            String vStatus=response.getString("v_status");
                            if(vStatus.equalsIgnoreCase("Completed"))
                            {
                                String vDetails=response.getString("v_details");
                                Gson gson=new Gson();
                                VerifiedUser user=gson.fromJson(vDetails, VerifiedUser.class);
                                ArrayList<VerifiedUser> verifiedUsers=new ArrayList<>();
                                verifiedUsers.add(0,user);
                                Intent intent=new Intent(DeepLinkActivity.this,VerifiedUserActivity.class);
                                intent.putParcelableArrayListExtra("verifiedUsers",verifiedUsers);
                                startActivity(intent);

                            }
                            else if(vStatus.equalsIgnoreCase("Suspended"))
                            {
                                actionBar.setTitle("Suspended");
                                suspendedView.setVisibility(View.VISIBLE);
                            }
                            else if(vStatus.equalsIgnoreCase("0"))
                            {
                                String email=response.getString("email");
                                String country=response.getString("country");
                                startKYC(email,country);
                            }
                        }
                    }catch (JSONException e)
                    {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }


                }
            });

        }
    }

    /*--start kyc--*/
    public void startKYC(String email,String country)
    {
        JkycSdk.initialize(this);
        KycUser kycUser=new KycUser(email,"",country,"REG","","","");
        kycUser.setJkycListener(new KycListener() {

            @Override
            public void onKError(String error) {

                progressDialog.dismiss();
            }
        });
        kycUser.startKyc(this);
    }

    /***update request--*/

    public void updateRequest()
    {
        Map<String, String> params = new HashMap<String, String>();
        if(connected)
        {
            params.put("id",id);
            HttpHandler httpHandler =new HttpHandler(this);
            httpHandler.makePost("update_request", params, new RequestDoneInterface() {
                @Override
                public void RequestDone(String result) {
                    updateDone(result);
                }
            });

        }

    }


    public  void  updateDone(String result) {
        try {
            JSONObject resp = new JSONObject(result);
            int status = resp.getInt("status");
            if (status == 0) {
                showSnack(false);
            } else if (status == 1) {
                actionBar.setTitle("COMPLETED");
                resultView.setVisibility(View.VISIBLE);
            }
            progressDialog.dismiss();
        }catch (JSONException e)
        {
            e.printStackTrace();
            progressDialog.dismiss();
        }
    }

    public boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
        return isConnected;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        connected=isConnected;
        showSnack(isConnected);
    }


    public void goReg(View v )
    {
        Intent intent =new Intent(this,RegisterActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {

        Intent intent =new Intent(this,RegisterActivity.class);
        startActivity(intent);
        super.onBackPressed();
    }

    private void showSnack(boolean isConnected) {


        if (!isConnected) {
            final CoordinatorLayout coordinatorLayout=(CoordinatorLayout) findViewById(R.id.main);
            snackbar = Snackbar.make(coordinatorLayout, "No internet connection",Snackbar.LENGTH_LONG).setAction("DISMISS", new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
            snackbar.setActionTextColor(Color.WHITE);
            TextView tv = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.RED);
            snackbar.show();

        }

    }

    @Override
    protected  void onActivityResult(int requestCode,int resultCode,Intent data)
    {
        progressDialog.setTitle("Loading");
        // check that you are responding to JKYC request
        if(requestCode==5592)
        {
            //check may be result is ok
            if(resultCode==RESULT_OK)
            {
                String Status=data.getStringExtra("STATUS");
                if(Status.equalsIgnoreCase("COMPLETED"))
                {
                    //send status and alert tru web api
                    updateRequest();
                }
                else
                {
                    progressDialog.dismiss();
                    Intent intent =new Intent(this,RegisterActivity.class);
                    {
                        startActivity(intent);
                    }
                }

            }
            else
            {
                progressDialog.dismiss();
            }
        }
        else
        {
            progressDialog.dismiss();
        }
    }
}
