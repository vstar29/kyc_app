package com.jostkyc.com.jkyc.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jostkyc.com.jkyc.R;
import com.jostkyc.com.jkyc.others.ConnectivityReceiver;
import com.jostkyc.com.jkyc.others.Helper;
import com.jostkyc.com.jkyc.others.HttpHandler;
import com.jostkyc.com.jkyc.others.ItemClickListener;
import com.jostkyc.com.jkyc.others.KycRequest;
import com.jostkyc.com.jkyc.others.MyApplication;
import com.jostkyc.com.jkyc.others.RequestDoneInterface;
import com.jostkyc.com.jkyc.others.User;
import com.jostkyc.com.jkyc.others.VerifiedUser;
import com.jostkyc.com.jkyc.others.VerifiedUsersAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VerifiedUsersActivity extends AppCompatActivity implements ItemClickListener, ConnectivityReceiver.ConnectivityReceiverListener {

    private CoordinatorLayout coordinatorLayout;
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private VerifiedUsersAdapter verifiedUsersAdapter;
    private ProgressBar progressBar;
    private ProgressBar loadMore;
    private int offset=0;
    private  boolean fetching=false;
    ArrayList<VerifiedUser> verifiedUsers;
    Parcelable recyclerState;
    RecyclerView.LayoutManager layoutManager;
    private static final String M_STATE = "r_state";
    TextView no_result;
    Boolean connected=true;
    Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verified_users);
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        recyclerView=(RecyclerView) findViewById(R.id.recyclerView);
        no_result=(TextView) findViewById(R.id.no_result);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
        progressBar=(ProgressBar) findViewById(R.id.progressBar);
        loadMore=(ProgressBar) findViewById(R.id.loading);
        verifiedUsers=new ArrayList<>();
        setSupportActionBar(toolbar);
        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Verified users");
        toolbar.setTitleTextColor(Color.WHITE);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_left);
        connected=checkConnection();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(dy > 0) //check for scroll down
                {
                    final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    int lastVisibleItem, totalItemCount;
                    totalItemCount = linearLayoutManager.getItemCount();
                    int visibleThreshold = 2;
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if(totalItemCount>=20) {
                        if (!fetching && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                            loadMore.setVisibility(View.VISIBLE);
                            loadVerifiedUsers();
                        }
                    }
                }

            }
        });

        loadVerifiedUsers();

    }

    public void loadVerifiedUsers()
    {
        if(!fetching) {
            fetching = true;
            Map<String, String> params = new HashMap<String, String>();
            User user = Helper.getUser(this);
            params.put("email", user.getEmail());
            params.put("offset", Integer.toString(offset));
            HttpHandler httpHandler = new HttpHandler(this);
            httpHandler.makePost("get_v_users", params, new RequestDoneInterface() {
                @Override
                public void RequestDone(String result) {
                    VerifiedUsersGetDone(result);
                }
            });
        }
    }

    public void VerifiedUsersGetDone(String result)
    {

        fetching=false;
        try {
            JSONObject resp = new JSONObject(result);
            int status = resp.getInt("status");
            if (status == 0) {
            } else if (status == 1) {
                String response = resp.getString("results");
                if(!TextUtils.isEmpty(response)){

                    Gson Converter = new Gson();
                    Type type = new TypeToken<List<VerifiedUser>>() {
                    }.getType();
                    try {

                        if (offset == 0) {

                            verifiedUsers = Converter.fromJson(response, type);
                            if (verifiedUsers.isEmpty()) {

                                no_result.setVisibility(View.VISIBLE);
                            }
                            verifiedUsersAdapter = new VerifiedUsersAdapter(this, verifiedUsers);
                            recyclerView.setAdapter(verifiedUsersAdapter);

                        } else {

                            loadMore.setVisibility(View.INVISIBLE);
                            ArrayList<VerifiedUser> holder = Converter.fromJson(response, type);
                            verifiedUsers.addAll(holder);
                            verifiedUsersAdapter.notifyDataSetChanged();
                            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                            recyclerView.smoothScrollToPosition(linearLayoutManager.getItemCount()-holder.size());

                        }
                        verifiedUsersAdapter.setClickListener(this);
                        offset = offset + 25;
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }


            }
            progressBar.setVisibility(View.INVISIBLE);
        }catch (JSONException e)
        {
            progressBar.setVisibility(View.INVISIBLE);
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view, int position) {

        ArrayList<VerifiedUser> toForward=new ArrayList<>();

        for (int i = position; i < position+7; i++) {
            if(i>=verifiedUsers.size())
            {
                break;
            }
            VerifiedUser each = verifiedUsers.get(i);
            toForward.add(i-position, each);
        }

        Intent intent = new Intent(VerifiedUsersActivity.this,VerifiedUserActivity.class);
        intent.putParcelableArrayListExtra("verifiedUsers", toForward);
        startActivity(intent);
    }
    @Override
    protected void onSaveInstanceState(Bundle state) 
    {
     super.onSaveInstanceState(state);

     recyclerState = layoutManager.onSaveInstanceState();
     state.putParcelable(M_STATE, recyclerState);
    }
    @Override
    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);
        if(state != null)
            recyclerState = state.getParcelable(M_STATE);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (recyclerState != null) {
            layoutManager.onRestoreInstanceState(recyclerState);
        }
        MyApplication.getInstance().setConnectivityListener(this);
    }



    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        connected=isConnected;
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {


        if (!isConnected) {
            final CoordinatorLayout coordinatorLayout=(CoordinatorLayout) findViewById(R.id.main);
            snackbar = Snackbar.make(coordinatorLayout, "No internet connection",Snackbar.LENGTH_LONG).setAction("DISMISS", new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
            snackbar.setActionTextColor(Color.WHITE);
            TextView tv = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.RED);
            snackbar.show();

        }

    }

    public boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
        return isConnected;
    }
    @Override
    public  void onDestroy()
    {
        super.onDestroy();
        try
        {
            unregisterReceiver(new ConnectivityReceiver());
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
