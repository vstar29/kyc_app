package com.jostkyc.com.jkyc.others;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.jostkyc.com.jkyc.R;
import com.jostkyc.com.jkyc.activities.MainActivity;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by VICTOR on 15-Nov-16.
 */
public class MyFireBaseMessageService extends FirebaseMessagingService {
    String TAG= FireBaseRegService.class.getSimpleName();
    Bitmap bitmap;
    SharedPreferences sharedPreferences;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try
        {
            String title = remoteMessage.getData().get("message");
            String type = remoteMessage.getData().get("type");
            String data="";
            String action_title="";
            if(type.equalsIgnoreCase("DONE"))
            {
                data = remoteMessage.getData().get("v_details");
                title="Tap for details";

            }
            else if(type.equalsIgnoreCase("RECEIVED"))
            {
                data = remoteMessage.getData().get("id");
                title="Tap to verify";

            }

            sharedPreferences=this.getSharedPreferences("My_preference",0);
            if(sharedPreferences.getBoolean("pushNotification",true))
            {
                sendNotification(title,data,type,action_title);
            }
        }catch (Exception e)
        {

        }

    }

    private void sendNotification(String title,String data,String type,String action_title) {

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("notifyCallBack",data);
        intent.putExtra("type",type);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)

                .setSmallIcon(R.drawable.jkyc_logo)
                .setContentTitle(title)
                .setContentText(action_title)
                .setAutoCancel(false)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 , notificationBuilder.build());

    }


    /*
   *To get a Bitmap image from the URL received
   * */
    public Bitmap getBitmapFromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }
    }

    private String formatDate(String date) {

        Date today = new Date();
        String current = today.toString();
        Date postdate = null;

        Long mSeconds;
        long day, hours, minutes, seconds;
        Boolean sameYear=false;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat yearFormat=new SimpleDateFormat("yyyy");
        SimpleDateFormat defaultFormat=new SimpleDateFormat("MMM dd");
        SimpleDateFormat format=new SimpleDateFormat("MMM dd, yyyy");

        try {
            postdate = dateFormat.parse(date);
            today= dateFormat.parse(current);

        } catch (Exception e) {
        }

        mSeconds = today.getTime() - postdate.getTime();
        //seconds= TimeUnit.MILLISECONDS.toSeconds(mSeconds);
        minutes= TimeUnit.MILLISECONDS.toMinutes(mSeconds);
        hours=TimeUnit.MILLISECONDS.toHours(mSeconds);
        day=TimeUnit.MILLISECONDS.toDays(mSeconds);
        String originalYear=yearFormat.format(postdate);
        String yearNow=yearFormat.format(today);
        if(originalYear.equals(yearNow))
        {
            sameYear=true;
        }

        if(day>7 &&sameYear )
        {
            return defaultFormat.format(postdate);
        }

        else if(day==1 &&sameYear )
        {
            return Long.toString(day) +  " day ago";
        }

        else if(day>1 &&day<=7 &&sameYear )
        {
            return Long.toString(day) +  " days ago";
        }
        else if(hours>1 &&hours <=23 && sameYear)
        {
            return Long.toString(hours) +  " hours ago";
        }
        else if(hours==1)
        {
            return  Long.toString(hours) + " hour ago";
        }

        else if(hours< 1 && minutes>1)
        {
            return Long.toString(minutes) +" minutes ago";
        }

        else if(minutes==1)
        {

            return Long.toString(minutes) +" minute ago";
        }

        else if(minutes<1 && sameYear)
        {

            return " just now";
        }

        else
        {
            return format.format(postdate);
        }



    }


}
