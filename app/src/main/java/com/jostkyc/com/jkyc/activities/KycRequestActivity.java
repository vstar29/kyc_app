package com.jostkyc.com.jkyc.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.jostkyc.com.jkyc.R;
import com.jostkyc.com.jkyc.others.ConnectivityReceiver;
import com.jostkyc.com.jkyc.others.Helper;
import com.jostkyc.com.jkyc.others.HttpHandler;
import com.jostkyc.com.jkyc.others.MyApplication;
import com.jostkyc.com.jkyc.others.RequestDoneInterface;
import com.jostkyc.com.jkyc.others.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class KycRequestActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener, AdapterView.OnItemSelectedListener {
    private EditText emailView;
    private ProgressBar progressBar;
    private CoordinatorLayout coordinatorLayout;
    private Toolbar toolbar;
    Boolean connected=true;
    Snackbar snackbar;
    private User user;
    String country;
    Spinner spinner;
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kyc_request);
        emailView=(EditText) findViewById(R.id.email);
        coordinatorLayout=(CoordinatorLayout) findViewById(R.id.main);
        spinner=(Spinner) findViewById(R.id.country);
        spinner.setOnItemSelectedListener(this);
        progressBar=(ProgressBar) findViewById(R.id.progressBar);
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("IDENTITY REQUEST");
        toolbar.setTitleTextColor(Color.WHITE);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_left);
        connected=checkConnection();
        user= Helper.getUser(this);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.country, R.layout.spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
    }


    public void sendRequest(View v)
    {
        hideKeyboard();
        progressBar.setVisibility(View.VISIBLE);
        Boolean valid=true;
        View focusView=null;
        String email=emailView.getText().toString();
        if(TextUtils.isEmpty(email))
        {
            emailView.setError("Email is required");
            valid=false;
            focusView=emailView;
        }
       else if(!Helper.isValidEmail(email))
        {
            emailView.setError("Invalid Email");
            valid=false;
            focusView=emailView;
        }
        if(email.equalsIgnoreCase(user.email))
        {
            emailView.setError("Use another email");
            valid=false;
            focusView=emailView;

        }

        if(TextUtils.isEmpty(country))
        {
            emailView.setError("Country is required");
            valid=false;
            focusView=emailView;
        }

        if(!valid)
        {
            progressBar.setVisibility(View.INVISIBLE);
            focusView.requestFocus();
        }
        else
        {
            if(connected)
            {
                Map<String, String> params = new HashMap<String, String>();
                User user= Helper.getUser(this);
                params.put("email",email);
                params.put("sender_email",user.getEmail());
                params.put("sender_name",user.getName());
                params.put("country",country);
                HttpHandler httpHandler =new HttpHandler(this);
                httpHandler.makePost("send_v_request", params, new RequestDoneInterface() {
                    @Override
                    public void RequestDone(String result) {
                        requestSentDone(result);;
                    }
                });
            }
            else
            {
                showSnack(false);
                progressBar.setVisibility(View.INVISIBLE);

            }

        }
    }

    /*-called after request has been succesfully sent--*/
    public void requestSentDone(String result)
    {
        try{
            JSONObject response = new JSONObject(result);
            int status=response.getInt("status");
            if(status==0)
            {
                String error=response.getString("error");
                emailView.setError(error);
            }
            else if(status==1)
            {
                Toast.makeText(this,"Request sent",Toast.LENGTH_LONG).show();
                Intent intent=new Intent(this,MainActivity.class);
                startActivity(intent);
            }

            progressBar.setVisibility(View.INVISIBLE);
        }catch (JSONException e)
        {
            progressBar.setVisibility(View.INVISIBLE);
            e.printStackTrace();
        }

    }


    /*--hide soft keyboard input---*/
    public void hideKeyboard() {
    
        ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
            hideSoftInputFromWindow(emailView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }
    @Override
    protected void onResume() {
        super.onResume();

        MyApplication.getInstance().setConnectivityListener(this);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        connected=isConnected;
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {


        if (!isConnected) {
            final CoordinatorLayout coordinatorLayout=(CoordinatorLayout) findViewById(R.id.main);
            snackbar = Snackbar.make(coordinatorLayout, "No internet connection",Snackbar.LENGTH_LONG).setAction("DISMISS", new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
            snackbar.setActionTextColor(Color.WHITE);
            TextView tv = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.RED);
            snackbar.show();

        }

    }

    public boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
        return isConnected;
    }
    @Override
    public  void onDestroy()
    {
        super.onDestroy();
        try
        {
            unregisterReceiver(new ConnectivityReceiver());
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        // An item was selected. You can retrieve the selected item using
        // parent.getItemAtPosition(pos)
        country=spinner.getSelectedItem().toString();

    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }
}
